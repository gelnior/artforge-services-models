package fr.hd3d.model.lightweight;

public interface ILMountPoint extends ILBase
{
    String getStorageName();

    String getOperatingSystem();

    String getMountPointPath();

    void setStorageName(String storageName);

    void setOperatingSystem(String operatingSystem);

    void setMountPointPath(String mountPointPath);
}
