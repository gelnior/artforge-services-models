package fr.hd3d.model.lightweight;

/**
 * Interface for object with name lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILSimple extends ILBase
{
    public abstract String getName();

    public abstract void setName(String name);
}
