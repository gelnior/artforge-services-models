package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILShot extends ILBase, Hook
{

    public Long getSequence();

    public String getLabel();

    public String getDescription();

    public Integer getDifficulty();

    public Integer getTrust();

    public Boolean getMattePainting();

    public Boolean getLayout();

    public Boolean getAnimation();

    public Boolean getExport();

    public Boolean getTracking();

    public Boolean getDressing();

    public Boolean getLighting();

    public Boolean getRendering();

    public Boolean getCompositing();

    public Integer getCompletion();

    public Integer getNbFrame();

    public String getSequencesNames();

    // setters
    public void setSequence(Long sequence);

    public void setLabel(String label);

    public void setDescription(String desc);

    public void setDifficulty(Integer difficulty);

    public void setTrust(Integer trust);

    public void setMattePainting(Boolean mattePainting);

    public void setLayout(Boolean layout);

    public void setAnimation(Boolean animation);

    public void setExport(Boolean export);

    public void setTracking(Boolean tracking);

    public void setDressing(Boolean dressing);

    public void setLighting(Boolean lighting);

    public void setRendering(Boolean rendering);

    public void setCompositing(Boolean compositing);

    public void setCompletion(Integer completion);

    public void setNbFrame(Integer nBFrame);

    public void setSequencesNames(String sequencesNames);

    public Integer getStartFrame();

    public Integer getEndFrame();

    public void setStartFrame(Integer startFrame);

    public void setEndFrame(Integer endFrame);
}
