package fr.hd3d.model.lightweight;

/**
 * a category has one and only one parent.
 * 
 * @author Try LAM
 * 
 */
public interface ILCategory extends ILBase
{

    // getters
    public String getName();

    public Long getParent();

    public Long getProject();

    // setters
    public void setName(String name);

    public void setParent(Long parent);

    public void setProject(Long project);
}
