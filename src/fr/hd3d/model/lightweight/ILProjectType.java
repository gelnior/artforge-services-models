package fr.hd3d.model.lightweight;

public interface ILProjectType extends ILBase
{
    public String getName();

    public String getColor();

    public String getDescription();

    public void setName(String name);

    public void setColor(String color);

    public void setDescription(String description);
}
