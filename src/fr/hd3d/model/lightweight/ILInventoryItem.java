package fr.hd3d.model.lightweight;

import java.util.Date;


public interface ILInventoryItem extends ILResource
{
    public abstract String getName();

    public abstract String getSerial();

    public abstract String getBillingReference();

    public abstract Date getPurchaseDate();

    public abstract Date getWarrantyEnd();

    public abstract String getInventoryStatus();

    public abstract void setName(String name);

    public abstract void setSerial(String serial);

    public abstract void setBillingReference(String billingReference);

    public abstract void setPurchaseDate(Date purchaseDate);

    public abstract void setWarrantyEnd(Date purchaseDate);

    public abstract void setInventoryStatus(String inventoryStatus);
}
