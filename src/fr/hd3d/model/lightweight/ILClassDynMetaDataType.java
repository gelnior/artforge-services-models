package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILClassDynMetaDataType extends ILBase
{
    public String getName();

    public Long getDynMetaDataType();

    public String getPredicate();

    public String getClassName();

    public String getExtra();

    public void setDynMetaDataType(Long dynmetadatetype);

    public void setPredicate(String predicate);

    public void setClassName(String className);

    public void setExtra(String extra);

    public void setName(String name);

    public String getDynMetaDataTypeName();

    public void setDynMetaDataTypeName(String name);

    public void setDynMetaDataTypeNature(String nature);

    public String getDynMetaDataTypeNature();
}
