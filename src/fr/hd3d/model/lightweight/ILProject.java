package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public interface ILProject extends ILSimple, Hook
{
    public abstract String getStatus();

    public abstract Date getStartDate();

    public abstract Date getEndDate();

    public abstract String getColor();

    public abstract LinkedHashSet<Long> getResourceGroupIDs();

    public void addResourceGroupID(Long id);

    @ClientField
    public abstract List<ILResourceGroup> getResourceGroups();

    @ServerImmutable
    public abstract List<String> getResourceGroupNames();

    public void addResourceGroupName(String name);

    public abstract void setResourceGroupIDs(LinkedHashSet<Long> resourceGroupIDs);

    public abstract void setResourceGroupNames(List<String> resourceGroupNames);

    @ClientField
    public abstract void setResourceGroup(List<ILResourceGroup> resourceGroup);

    public abstract void setStatus(String status);

    public abstract void setStartDate(Date startDate);

    public abstract void setEndDate(Date endDate);

    public abstract void setColor(String color);

    public Long getProjectTypeId();

    public void setProjectTypeId(Long projectTypeId);

    public Long getRootCategoryId();

    public void setRootCategoryId(Long rootCategoryId);

    public Long getRootSequenceId();

    public void setRootSequenceId(Long rootSequenceId);

    public abstract Set<Long> getTaskTypeIDs();

    public abstract void addTaskTypeID(Long id);

    public abstract void addTaskTypeName(String name);

    public abstract List<String> getTaskTypeColors();

    public abstract void addTaskTypeColor(String color);
}
