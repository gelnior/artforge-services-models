package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for qualification lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILQualification extends ILSimple
{
    public List<Long> getSkillIDs();

    public List<String> getSkillNames();

    public void setSkillIDs(List<Long> skillIDs);

    public void setSkillNames(List<String> skillNames);

    public void addSkillID(Long id);

    public void addSkillName(String name);
}
