package fr.hd3d.model.lightweight;

import java.util.Date;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public interface ILActivity extends ILBase
{
    public abstract ILPersonDay getDay();

    public abstract Long getDayID();

    public abstract Date getDayDate();

    public abstract Long getDuration();

    public abstract String getComment();

    @ClientField
    public abstract ILPerson getWorker();

    @ServerImmutable
    public abstract String getWorkerName();

    @ServerImmutable
    public abstract Long getWorkerID();

    @ServerImmutable
    public abstract String getWorkerLogin();

    @ServerImmutable
    public abstract String getTaskName();

    @ClientField
    public abstract ILProject getProject();

    @ServerImmutable
    public abstract Long getProjectID();

    @ServerImmutable
    public abstract String getProjectName();

    @ServerImmutable
    public abstract String getProjectColor();

    public abstract void setDay(ILPersonDay day);

    public abstract void setDayID(Long dayID);

    public abstract void setDayDate(Date dayDate);

    public abstract void setDuration(Long duration);

    public abstract void setComment(String comment);

    @ClientField
    public abstract void setProject(ILProject project);

    @ClientField
    public abstract void setWorker(ILPerson worker);

    @ClientField
    public abstract ILPerson getFilledBy();

    @ServerImmutable
    public abstract Long getFilledByID();

    @ServerImmutable
    public abstract String getFilledByName();

    @ServerImmutable
    public abstract String getFilledByLogin();

    @ClientField
    public abstract Date getFilledDate();

    public abstract void setFilledDate(Date date);

    public String getProjectTypeName();

    public void setProjectTypeName(String projectTypeName);
}
