package fr.hd3d.model.lightweight;

public interface ILAssetRevisionLink extends ILBase
{

    public Long getInputAsset();

    public Long getOutputAsset();

    public String getType();

    public void setInputAsset(Long inputAsset);

    public void setOutputAsset(Long outputAsset);

    public void setType(String type);

}
