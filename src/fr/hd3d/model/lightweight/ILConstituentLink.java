package fr.hd3d.model.lightweight;



public interface ILConstituentLink extends ILBase
{

    public Long getMaster();

    public Long getSlave();

    public String getType();

    public void setMaster(Long master);

    public void setSlave(Long slave);

    public void setType(String type);

}
