package fr.hd3d.model.lightweight;

/**
 * @author Guillaume CHATELET
 * Implementation of the A34 set specifications
 * https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public interface ILSimpleActivity extends ILActivity {
    public void setProjectID(Long projectID);

    public void setWorkerID(Long workerID);

    public String getType();

    public void setType(String type);
}
