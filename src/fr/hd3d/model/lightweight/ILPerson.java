package fr.hd3d.model.lightweight;

import java.util.List;

import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public interface ILPerson extends ILResource
{
    public String getLogin();

    public String getFirstName();

    public String getLastName();

    @ServerImmutable
    public String getName();

    public void setLogin(String login);

    public void setFirstName(String firstName);

    public void setLastName(String lastName);

    public String getEmail();

    public void setEmail(String email);

    public String getPhone();

    public void setPhone(String phone);

    public String getPhotoPath();

    public void setPhotoPath(String photoPath);

    public List<Long> getSkillLevelIDs();

    public List<String> getSkillLevelNames();

    public void setSkillLevelIDs(List<Long> skillLevelIDs);

    public void addSkillLevelID(Long id);

    public void setSkillLevelNames(List<String> skillLevelNames);

    public void addSkillLevelName(String name);

    public List<Long> getContractIDs();

    public void setContractIDs(List<Long> contractIDs);

    public void addContractID(Long id);

    public List<String> getContractNames();

    public void setContractNames(List<String> contractNames);

    public void addContractName(String name);

    @ServerImmutable
    public Long getAccountStatus();
}
