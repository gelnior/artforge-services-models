package fr.hd3d.model.lightweight;

public interface ILProxy extends ILBase
{

    Long getFileRevisionId();

    Long getProxyTypeId();

    String getLocation();

    String getDescription();

    String getMountPoint();

    String getFilename();

    String getProxyTypeOpenType();

    Integer getFramerate();

    void setProxyTypeOpenType(String opentType);

    void setFileRevisionId(final Long fileRevisionId);

    void setProxyTypeId(Long proxyTypeId);

    void setLocation(String path);

    void setProxyTypeName(String proxyTypeName);

    void setDescription(final String description);

    void setMountPoint(String mountPoint);

    void setFilename(String filename);

    void setFramerate(Integer framerate);

}
