package fr.hd3d.model.lightweight;

import java.util.Collection;

import fr.hd3d.model.lightweight.impl.LTag.LTagParentIdType;


/**
 * @author Try LAM
 */
public interface ILTag extends ILBase
{

    // getters
    public String getName();

    // setters
    public void setName(String name);

    public Collection<LTagParentIdType> getParents();

    public void setParents(Collection<LTagParentIdType> parents);

    public void addParent(Long parentId, String parentType);

    public void removeParent(Long parentId, String parentType);

}
