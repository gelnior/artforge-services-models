/**
 * 
 */
package fr.hd3d.model.lightweight;

import java.util.Date;


/**
 * @author thomas-eskenazi
 * 
 */
public interface ILMileStone extends ILBase
{
    public ILPlanning getPlanning();

    public Long getPlanningID();

    public Date getDate();

    public String getTitle();

    public String getDescription();

    public String getColor();

    public void setPlanning(ILPlanning planning);

    public void setPlanningID(Long plannongID);

    public void setDate(Date date);

    public void setTitle(String title);

    public void setDescription(String description);

    public void setColor(String color);
}
