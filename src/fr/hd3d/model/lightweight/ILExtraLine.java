/**
 * 
 */
package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * @author thomas-eskenazi
 * 
 */
public interface ILExtraLine extends ILBase
{
    public List<ILTaskBase> getTaskBases();

    public List<Long> getTaskBaseIDs();

    public ILTaskGroup getTaskGroup();

    public Long getTaskGroupID();

    public String getName();

    public ILPerson getHiddenPerson();

    public Long getHiddenPersonID();

    public void setTaskBases(List<ILTaskBase> taskBases);

    public void setTaskBaseIDs(List<Long> taskBaseIDs);

    public void setTaskGroup(ILTaskGroup taskGroup);

    public void setTaskGroupID(Long taskGroupID);

    public void setName(String name);

    public void setHiddenPerson(ILPerson person);

    public void setHiddenPersonID(Long iD);
}
