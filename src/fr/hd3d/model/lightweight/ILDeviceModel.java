package fr.hd3d.model.lightweight;

/**
 * Interface for device model lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILDeviceModel extends ILModel
{
}
