package fr.hd3d.model.lightweight;

import fr.hd3d.model.lightweight.annotation.ServerImmutable;


public interface ILApprovalNoteType extends ILBase
{
    public Long getProject();

    public String getName();

    public Long getTaskType();

    public void setName(String name);

    public void setTaskType(Long taskType);

    public void setProject(Long projectID);

    @ServerImmutable
    public String getTaskTypeName();

    public void setTaskTypeName(String name);

    @ServerImmutable
    public String getTaskTypeColor();

    public void setTaskTypeColor(String color);

}
