package fr.hd3d.model.lightweight;

public interface ILTaskType extends ILBase
{

    public Long getProjectID();

    public ILProject getProject();

    public String getName();

    public String getColor();

    public String getDescription();

    public String getEntityName();

    public void setProjectID(Long iD);

    public void setProject(ILProject project);

    public void setName(String name);

    public void setColor(String color);

    public void setDescription(String description);

    public void setEntityName(String entityName);
}
