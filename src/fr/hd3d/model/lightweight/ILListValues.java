package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILListValues extends ILBase
{
    public String getSeparator();

    public void setSeparator(String separator);

    public String getValues();

    public void setValues(String values);
}
