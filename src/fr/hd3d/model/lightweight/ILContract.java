package fr.hd3d.model.lightweight;

import java.util.Date;


public interface ILContract extends ILSimple
{

    public abstract Date getStartDate();

    public abstract void setStartDate(Date startDate);

    public abstract Date getEndDate();

    public abstract void setEndDate(Date endDate);

    public abstract Date getRealEndDate();

    public abstract void setRealEndDate(Date realEndDate);

    public abstract String getDocPath();

    public abstract void setDocPath(String docPath);

    public abstract String getType();

    public abstract void setType(String type);

    public abstract Long getPersonId();

    public abstract void setPersonId(Long personId);

    public abstract Long getQualificationId();

    public abstract void setQualificationId(Long qualificationId);

    public abstract Long getOrganizationId();

    public abstract void setOrganizationId(Long organizationId);

    public abstract String getPersonName();

    public abstract void setPersonName(String personName);

    public abstract String getQualificationName();

    public abstract void setQualificationName(String qualificationName);

    public abstract String getOrganizationName();

    public abstract void setOrganizationName(String organizationName);

    public abstract Float getSalary();

    public abstract void setSalary(Float salary);

}
