package fr.hd3d.model.lightweight;



public interface ILFileRevisionLink extends ILBase
{

    public Long getInputFile();

    public Long getOutputFile();

    public String getType();

    public void setInputFile(Long inputFile);

    public void setOutputFile(Long outputFile);

    public void setType(String type);

}
