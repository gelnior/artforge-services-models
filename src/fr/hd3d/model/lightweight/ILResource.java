package fr.hd3d.model.lightweight;

import java.util.List;

import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * Interface for resource lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILResource extends ILBase
{
    public List<Long> getResourceGroupIDs();

    public void addResourceGroupID(Long id);

    public List<String> getResourceGroupNames();

    public void addResourceGroupName(String name);

    public List<Long> getProjectIDs();

    public void addProjectID(Long id);

    public void setResourceGroupIDs(List<Long> resourceGroupIds);

    public void setResourceGroupNames(List<String> resourceGroupNames);

    public void setProjectIDs(List<Long> projectIDs);

    @ServerImmutable
    public List<String> getProjectNames();

    public void addProjectName(String name);

    @ServerImmutable
    public void setProjectNames(List<String> projectNames);
}
