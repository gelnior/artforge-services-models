package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for computer lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILComputer extends ILInventoryItem
{
    public Long getInventoryId();

    public String getDnsName();

    public String getIpAdress();

    public String getMacAdress();

    public Integer getProcFrequency();

    public Integer getNumberOfProcs();

    public Integer getNumberOfCores();

    public Integer getRamQuantity();

    public String getWorkerStatus();

    public Integer getxPosition();

    public Integer getyPosition();

    public Long getProcessorId();

    public String getProcessorName();

    public Long getComputerTypeId();

    public String getComputerTypeName();

    public Long getComputerModelId();

    public String getComputerModelName();

    public Long getStudioMapId();

    public String getStudioMapName();

    public Long getRoomId();

    public String getRoomName();

    public List<Long> getPoolIDs();

    public List<String> getPoolNames();

    public List<Long> getSoftwareIDs();

    public List<String> getSoftwareNames();

    public List<Long> getLicenseIDs();

    public List<String> getLicenseNames();

    public List<Long> getDeviceIDs();

    public List<String> getDeviceNames();

    public List<Long> getScreenIDs();

    public List<String> getScreenNames();

    public void setInventoryId(Long inventoryId);

    public void setDnsName(String dnsName);

    public void setIpAdress(String ipAdress);

    public void setMacAdress(String macAdress);

    public void setProcFrequency(Integer procFrequency);

    public void setNumberOfProcs(Integer numberOfProcs);

    public void setNumberOfCores(Integer numberOfCores);

    public void setRamQuantity(Integer ramQuantity);

    public void setWorkerStatus(String status);

    public void setxPosition(Integer xPosition);

    public void setyPosition(Integer yPosition);

    public void setStudioMapId(Long studioMapId);

    public void setStudioMapName(String studioMapName);

    public void setRoomId(Long roomId);

    public void setRoomName(String roomName);

    public void setPersonId(Long personId);

    public Long getPersonId();

    public void setPersonName(String personName);

    public String getPersonName();

    public void setProcessorId(Long processorId);

    public void setProcessorName(String processorName);

    public void setComputerModelId(Long computerModelId);

    public void setComputerModelName(String computerModelName);

    public void setComputerTypeId(Long computerModelId);

    public void setComputerTypeName(String computerModelName);

    public void setPoolIDs(List<Long> poolIDs);

    public void setPoolNames(List<String> poolNames);

    public void setSoftwareIDs(List<Long> softwareIDs);

    public void setSoftwareNames(List<String> softwareNames);

    public void setLicenseIDs(List<Long> licenseIDs);

    public void setLicenseNames(List<String> licenseNames);

    public void setDeviceIDs(List<Long> deviceIDs);

    public void setDeviceNames(List<String> deviceNames);

    public void setScreenIDs(List<Long> screenIDs);

    public void setScreenNames(List<String> screenNames);
}
