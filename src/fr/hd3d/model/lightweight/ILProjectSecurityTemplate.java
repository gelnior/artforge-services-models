package fr.hd3d.model.lightweight;

import java.util.List;


public interface ILProjectSecurityTemplate extends ILSimple
{

    public List<Long> getRoleIds();

    public void setRoleIds(List<Long> roleIds);

    public List<String> getRoleNames();

    public void setRoleNames(List<String> roleNames);

    public String getComment();

    public void setComment(String comment);

}
