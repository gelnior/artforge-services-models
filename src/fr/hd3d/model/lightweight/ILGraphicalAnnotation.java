package fr.hd3d.model.lightweight;

import java.util.Date;


public interface ILGraphicalAnnotation extends ILBase
{
    String getAnnotationSvg();

    Long getMarkIn();

    Long getMarkOut();

    String getComment();

    Long getAuthor();

    Long getProxy();

    Date getDate();

    Long getApprovalNote();

    void setAnnotationSvg(String annotationSvg);

    void setMarkIn(Long markIn);

    void setMarkOut(Long markOut);

    void setComment(String comment);

    void setAuthor(Long author);

    void setProxy(Long proxy);

    void setApprovalNote(Long approvalNote);

    void setDate(Date date);
}
