package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.Set;


public interface ILFileRevision extends ILBase
{
    public String getUriScheme();

    public Date getCreationDate();

    public String getKey();

    public String getVariation();

    public Integer getRevision();

    public Long getCreator();

    public abstract String getCreatorName();

    public String getFormat();

    public Long getSize();

    public String getChecksum();

    public String getLocation();

    public String getRelativePath();

    public String getStatus();

    public Date getLastOperationDate();

    public Long getLastUser();

    public abstract String getLastUserName();

    public String getComment();

    public String getValidity();

    public String getSequence();

    public Set<Long> getAttributes();

    public String getFullPath();

    public Long getAssetRevision();

    public String getState();

    public Set<Long> getProxies();

    public String getMountPoint();

    public void setMountPoint(String mountPoint);

    public void setUriScheme(String uriScheme);

    public void setCreationDate(Date creationDate);

    public void setKey(String key);

    public void setVariation(String variation);

    public void setRevision(Integer revision);

    public void setCreator(Long creator);

    public abstract void setCreatorName(String creatorName);

    public void setFormat(String format);

    public void setSize(Long size);

    public void setChecksum(String checksum);

    public void setLocation(String location);

    public void setRelativePath(String relativePath);

    public void setStatus(String status);

    public void setLastOperationDate(Date lastOperationDate);

    public void setLastUser(Long lastUser);

    public abstract void setLastUserName(String lastUsername);

    public void setComment(String comment);

    public void setValidity(String validity);

    public void setSequence(String sequence);

    public void setAttributes(Set<Long> attributes);

    public void setAssetRevision(Long assetRevision);

    public void setState(String state);

    public void setProxies(Set<Long> proxies);

    public void setTaskTypeName(String taskTypeName);

}
