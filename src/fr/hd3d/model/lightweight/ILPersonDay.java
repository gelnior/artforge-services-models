package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ILPersonDay extends ILBase
{

    // public abstract Long getId();
    //
    // public abstract void setId(Long id);
    //
    // public abstract java.sql.Timestamp getVersion();
    //
    // public abstract void setVersion(java.sql.Timestamp version);

    public abstract Date getDate();

    public abstract void setDate(Date day);

    public abstract ILPerson getPerson();

    public abstract void setPerson(ILPerson person);

    public abstract Long getPersonID();

    public abstract void setPersonID(Long personID);

    public abstract String getPersonName();

    public abstract void setPersonName(String name);

    public abstract List<ILActivity> getActivities();

    public abstract void setActivities(List<ILActivity> activities);

    public abstract ILPerson getActivitiesApprovedBy();

    public abstract void setActivitiesApprovedBy(ILPerson activitiesApprovedBy);

    public abstract Long getActivitiesApprovedByID();

    public abstract void setActivitiesApprovedByID(Long activitiesApprovedByID);

    public abstract String getActivitiesApprovedByLogin();

    public abstract void setActivitiesApprovedByLogin(String activitiesApprovedByName);

    public abstract Date getActivitiesApprovedDate();

    public abstract void setActivitiesApprovedDate(Date activitiesApprovedDate);

    public abstract LinkedHashSet<Long> getActivitiesIDs();

    public abstract void setActivitiesIDs(LinkedHashSet<Long> activitiesID);

    public void addActivityID(Long id);

    public void addActivity(ILActivity activity);

    public List<ILWorkingTime> getWorkingTimes();

    public List<Long> getWorkingTimesIDs();

    public void setWorkingTimes(List<ILWorkingTime> workingTimes);

    public void setWorkingTimesIDs(List<Long> workingTimesIDs);

    public void addWorkingTime(ILWorkingTime workingTime);

    public void addWorkingTimeID(Long id);

    // workaround for mcguff
    public Date getStartDate1();

    public Date getEndDate1();

    public Date getStartDate2();

    public Date getEndDate2();

    public void setStartDate1(Date startDate1);

    public void setEndDate1(Date endDate1);

    public void setStartDate2(Date startDate2);

    public void setEndDate2(Date endDate2);
}
