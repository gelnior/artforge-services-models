package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ILItem extends ILBase
{

    public String getName();

    public String getType();

    public String getQuery();

    public String getControlContent();

    public Long getItemGroup();

    public String getMetaType();

    public String getRenderer();

    public String getEditor();

    public String getTransformer();

    public String getTransformerParameters();

    public List getListValues();

    public void setName(String name);

    public void setType(String type);

    public void setQuery(String query);

    public void setControlContent(String controlcontent);

    public void setItemGroup(Long itemgroup);

    public void setMetaType(String metatype);

    public void setRenderer(String renderer);

    public void setEditor(String editor);

    public void setTransformer(String transformer);

    public void setTransformerParameters(String transformerParameters);

    public void setListValues(List listValues);

}
