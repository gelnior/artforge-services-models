package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for organization lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILOrganization extends ILSimple
{
    public Long getParent();

    public void setParent(Long parent);

    public List<Long> getResourceGroupIds();

    public List<String> getResourceGroupNames();

    public void setResourceGroupIds(List<Long> resourceGroupIds);

    public void setResourceGroupNames(List<String> resourceGroupNames);

    public void addResourceGroupID(Long id);

    public void addResourceGroupName(String name);
}
