/**
 * 
 */
package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 * 
 */
public interface ILWorkingTime extends ILDurable, ILBase
{
    public Long getPersonDayId();

    public void setPersonDayId(Long personDayId);
}
