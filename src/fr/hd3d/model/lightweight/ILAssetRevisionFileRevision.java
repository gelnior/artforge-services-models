package fr.hd3d.model.lightweight;

public interface ILAssetRevisionFileRevision extends ILBase
{
    public String getAssetKey();

    public String getFileKey();

    public String getAssetVariation();

    public String getFileVariation();

    public Integer getAssetRevision();

    public Integer getFileRevision();

    public void setAssetKey(String assetHook);

    public void setFileKey(String fileHook);

    public void setAssetRevision(Integer assetRevision);

    public void setFileRevision(Integer fileRevision);

    public void setAssetVariation(String assetVariation);

    public void setFileVariation(String fileVariation);

}
