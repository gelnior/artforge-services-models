package fr.hd3d.model.lightweight;

public interface ILFileRevisionAnnotation extends ILBase
{
    public Integer getMarkIn();

    public Integer getMarkOut();

    public String getName();

    public String getComment();

    public Long getGraphicData();

    public Long getAnnotatedFile();

    public Long getAnnotator();

    public void setMarkIn(Integer markIn);

    public void setMarkOut(Integer markOut);

    public void setName(String name);

    public void setComment(String comment);

    public void setGraphicData(Long graphicData);

    public void setAnnotatedFile(Long annotatedFile);

    public void setAnnotator(Long annotator);
}
