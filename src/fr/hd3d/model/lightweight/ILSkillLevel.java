package fr.hd3d.model.lightweight;

/**
 * Interface for skill level lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILSkillLevel extends ILBase
{
    public abstract String getLevel();

    public abstract void setLevel(String level);

    public abstract String getMotivation();

    public abstract void setMotivation(String motivation);

    public abstract Long getPersonId();

    public abstract void setPersonId(Long personId);

    public abstract String getPersonName();

    public abstract void setPersonName(String personName);

    public abstract Long getSkillId();

    public abstract void setSkillId(Long skillId);

    public abstract String getSkillName();

    public abstract void setSkillName(String skillName);
}
