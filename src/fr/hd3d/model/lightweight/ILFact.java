package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILFact extends ILBase
{

    public String getName();

    public void setName(String name);

    public String getDescription();

    public Long getComposition();

    public void setDescription(String description);

    public Long getFactType();

    public void setFactType(Long facttype);

    public void setComposition(Long composition);

}
