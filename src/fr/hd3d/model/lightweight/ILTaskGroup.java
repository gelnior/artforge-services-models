package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * @author michael.guiral
 * 
 */
public interface ILTaskGroup extends ILTaskBase
{
    public void setName(String name);

    public String getName();

    public void setColor(String color);

    public String getColor();

    public void setPlanning(Long planning);

    public Long getPlanning();

    public String getFilter();

    public void setFilter(String filter);

    public List<Long> getExtraLineIDs();

    public void setExtraLineIDs(List<Long> extraLineIDs);

    public Long getTaskTypeId();

    public void setTaskTypeId(Long taskType);

}
