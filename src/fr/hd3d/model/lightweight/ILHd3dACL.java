package fr.hd3d.model.lightweight;

import java.util.Set;


public interface ILHd3dACL extends ILBase
{

    Set<Long> getRoles();

    void setRoles(Set<Long> roles);

    String getEntityName();

    void setEntityName(String entityName);

    String getReadPermitted();

    void setReadPermitted(String read);

    String getCreatePermitted();

    void setCreatePermitted(String create);

    String getUpdatePermitted();

    void setUpdatePermitted(String update);

    String getDeletePermitted();

    void setDeletePermitted(String delete);

    String getReadBans();

    void setReadBans(String readBans);

    String getUpdateBans();

    void setUpdateBans(String updateBans);

    String getDeleteBans();

    void setDeleteBans(String deleteBans);

}
