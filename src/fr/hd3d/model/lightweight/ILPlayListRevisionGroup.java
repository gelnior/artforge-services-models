package fr.hd3d.model.lightweight;

import java.util.Collection;
import java.util.List;


public interface ILPlayListRevisionGroup extends ILBase
{
    String getName();

    Long getProject();

    void setProject(Long project);

    Long getParent();

    Collection<Long> getChildren();

    void setName(String name);

    void setParent(Long parent);

    void setChildren(Collection<Long> children);

    List<Long> getPlayListRevisions();

    void setPlayListRevisions(List<Long> playListRevisions);
}
