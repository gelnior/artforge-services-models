package fr.hd3d.model.lightweight;

public interface ILEntityTaskLink extends ILBase
{

    public Long getBoundEntity();

    public String getBoundEntityName();

    public Long getTask();

    public String getExtra();

    public void setBoundEntity(Long boundEntity);

    public void setBoundEntityName(String boundEntityName);

    public void setTask(Long task);

    public void setExtra(String extra);
}
