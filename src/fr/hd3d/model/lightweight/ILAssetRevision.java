package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;


public interface ILAssetRevision extends ILBase
{
    public abstract Set<Long> getAssetRevisionGroups();

    public abstract Long getProject();

    public abstract Date getCreationDate();

    public abstract String getKey();

    public abstract String getVariation();

    public abstract Integer getRevision();

    public abstract Long getCreator();

    public abstract String getCreatorName();

    public abstract Long getTaskType();

    public abstract String getTaskTypeName();

    public abstract String getName();

    public abstract String getStatus();

    public abstract Date getLastOperationDate();

    public abstract Long getLastUser();

    public abstract String getLastUserName();

    public abstract String getComment();

    public abstract String getValidity();

    public abstract String getWorkObjectName();

    public abstract String getWorkObjectPath();

    public abstract Long getWorkObjectId();

    public abstract String getWipPath();

    public abstract String getPublishPath();

    public abstract String getOldPath();

    public abstract String getMountPointWipPath();

    public abstract String getMountPointPublishPath();

    public abstract String getMountPointOldPath();

    public String getBoundEntityName();

    public void setBoundEntityName(String boundEntityName);

    public abstract void setAssetRevisionGroups(Set<Long> assetRevisionGroups);

    public abstract void setProject(Long project);

    public abstract void setCreationDate(Date timeStamp);

    public abstract void setKey(String key);

    public abstract void setVariation(String variation);

    public abstract void setRevision(Integer revision);

    public abstract void setCreator(Long creator);

    public abstract void setCreatorName(String creatorName);

    public abstract void setTaskType(Long type);

    public abstract void setTaskTypeName(String taskTypeName);

    public abstract void setName(String name);

    public abstract void setStatus(String status);

    public abstract void setLastOperationDate(Date since);

    public abstract void setLastUser(Long lastUser);

    public abstract void setLastUserName(String lastUsername);

    public abstract void setComment(String comment);

    public abstract void setValidity(String validity);

    public abstract String getTaskTypeColor();

    public abstract void setTaskTypeColor(String color);

    public abstract void setWorkObjectName(String name);

    public abstract void setWorkObjectPath(String path);

    public abstract void setWorkObjectId(Long workObjectId);

    public abstract void setWipPath(String wipPath);

    public abstract void setPublishPath(String publishPath);

    public abstract void setOldPath(String oldPath);

    public abstract void setMountPointWipPath(String mountPointWipPath);

    public abstract void setMountPointPublishPath(String mountPointPublishPath);

    public abstract void setMountPointOldPath(String mountPointOldPath);

    public abstract void setFileRevisions(LinkedHashSet<Long> linkedHashSet);

    public LinkedHashSet<Long> getFileRevisions();

}
