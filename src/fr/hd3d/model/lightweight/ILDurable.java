package fr.hd3d.model.lightweight;

import java.util.Date;


public interface ILDurable
{
    public Date getStartDate();

    public Date getEndDate();

    public void setStartDate(Date startDate);

    public void setEndDate(Date endDate);

}
