package fr.hd3d.model.lightweight;

public interface ILPlayListEdit extends ILBase
{
    public Integer getSrcIn();

    public Integer getSrcOut();

    public Integer getRecIn();

    public Integer getRecOut();

    // public Long getFileRevision();
    public Long getProxy();

    public Long getPlayListRevision();

    public String getName();

    public void setSrcIn(Integer srcIn);

    public void setSrcOut(Integer srcOut);

    public void setRecIn(Integer recIn);

    public void setRecOut(Integer recOut);

    // public void setFileRevision(Long fileRevision);
    public void setProxy(Long proxy);

    public void setPlayListRevision(Long playListRevision);

    public void setName(String name);
}
