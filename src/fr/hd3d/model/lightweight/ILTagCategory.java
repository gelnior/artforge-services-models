package fr.hd3d.model.lightweight;

import java.util.List;



public interface ILTagCategory extends ILBase
{
    public abstract String getName();

    public abstract void setName(String name);

    public abstract Long getParent();

    public abstract void setParent(Long parent);

    public abstract List<Long> getChildren();

    public abstract void setChildren(List<Long> children);

    public abstract List<Long> getBoundTags();

    public abstract void setBoundTags(List<Long> boundTags);

}
