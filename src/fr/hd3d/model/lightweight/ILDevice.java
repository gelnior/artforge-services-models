package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for software lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILDevice extends ILInventoryItem
{
    public abstract Float getSize();

    public abstract Long getDeviceTypeId();

    public abstract String getDeviceTypeName();

    public abstract Long getDeviceModelId();

    public abstract String getDeviceModelName();

    public abstract List<Long> getComputerIDs();

    public abstract List<String> getComputerNames();

    public abstract void setSize(Float size);

    public abstract void setDeviceTypeId(Long id);

    public abstract void setDeviceTypeName(String type);

    public abstract void setDeviceModelId(Long typeId);

    public abstract void setDeviceModelName(String name);

    public abstract void setComputerIDs(List<Long> computerIDs);

    public abstract void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);
}
