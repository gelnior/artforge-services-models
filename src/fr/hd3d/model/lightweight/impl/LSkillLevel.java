package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;

import fr.hd3d.model.lightweight.ILSkillLevel;


/**
 * Lightweight implementation for skill level object.
 * 
 * @author HD3D
 */
public class LSkillLevel extends LBase implements ILSkillLevel
{
    String level;
    String motivation;

    Long personID;
    String personName;
    Long skillID;
    String skillName;

    public void clear()
    {
        super.clear();
        level = null;
        motivation = null;
        personID = null;
        personName = null;
        skillID = null;
        skillName = null;
    }

    /**
     * Default Constructor.
     */
    public LSkillLevel()
    {}

    public LSkillLevel(Long id, Timestamp version, String level, String motivation)
    {
        super(id, version);

        this.level = level;
        this.motivation = motivation;
    }

    public static ILSkillLevel getNewInstance(Long id, Timestamp version, String level, String motivation)
    {
        final ILSkillLevel skillLevel = new LSkillLevel(id, version, level, motivation);

        return skillLevel;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public String getMotivation()
    {
        return motivation;
    }

    public void setMotivation(String motivation)
    {
        this.motivation = motivation;
    }

    public Long getPersonId()
    {
        return personID;
    }

    public void setPersonId(Long personId)
    {
        personID = personId;
    }

    public String getPersonName()
    {
        return personName;
    }

    public void setPersonName(String personName)
    {
        this.personName = personName;
    }

    public Long getSkillId()
    {
        return skillID;
    }

    public void setSkillId(Long skillId)
    {
        skillID = skillId;
    }

    public String getSkillName()
    {
        return skillName;
    }

    public void setSkillName(String skillName)
    {
        this.skillName = skillName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((level == null) ? 0 : level.hashCode());
        result = prime * result + ((motivation == null) ? 0 : motivation.hashCode());
        result = prime * result + ((personID == null) ? 0 : personID.hashCode());
        result = prime * result + ((personName == null) ? 0 : personName.hashCode());
        result = prime * result + ((skillID == null) ? 0 : skillID.hashCode());
        result = prime * result + ((skillName == null) ? 0 : skillName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSkillLevel other = (LSkillLevel) obj;
        if (level == null)
        {
            if (other.level != null)
                return false;
        }
        else if (!level.equals(other.level))
            return false;
        if (motivation == null)
        {
            if (other.motivation != null)
                return false;
        }
        else if (!motivation.equals(other.motivation))
            return false;
        if (personID == null)
        {
            if (other.personID != null)
                return false;
        }
        else if (!personID.equals(other.personID))
            return false;
        if (personName == null)
        {
            if (other.personName != null)
                return false;
        }
        else if (!personName.equals(other.personName))
            return false;
        if (skillID == null)
        {
            if (other.skillID != null)
                return false;
        }
        else if (!skillID.equals(other.skillID))
            return false;
        if (skillName == null)
        {
            if (other.skillName != null)
                return false;
        }
        else if (!skillName.equals(other.skillName))
            return false;
        return true;
    }

}
