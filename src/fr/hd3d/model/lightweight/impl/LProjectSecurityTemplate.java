package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;


public class LProjectSecurityTemplate extends LSimple implements ILProjectSecurityTemplate
{

    private List<Long> roleIds;
    private List<String> roleNames;
    private String comment;

    public void clear()
    {
        super.clear();

        if (roleIds != null)
            roleIds.clear();
        roleIds = null;

        if (roleNames != null)
            roleNames.clear();
        roleNames = null;

        comment = null;
    }

    public LProjectSecurityTemplate()
    {
        super();
    }

    public LProjectSecurityTemplate(Long id, Timestamp version, List<Long> tags, String name, String comment)
    {
        super(id, version, tags, name);
        this.comment = comment;
    }

    public static ILProjectSecurityTemplate getNewInstance(Long id, Timestamp version, List<Long> tags, String name,
            String comment)
    {
        final LProjectSecurityTemplate template = new LProjectSecurityTemplate(id, version, tags, name, comment);

        return template;
    }

    public static ILProjectSecurityTemplate getNewInstance(String name, String comment)
    {
        return getNewInstance(null, null, null, name, comment);
    }

    public List<Long> getRoleIds()
    {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds)
    {
        this.roleIds = roleIds;
    }

    public List<String> getRoleNames()
    {
        return roleNames;
    }

    public void setRoleNames(List<String> roleNames)
    {
        this.roleNames = roleNames;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((roleIds == null) ? 0 : roleIds.hashCode());
        result = prime * result + ((roleNames == null) ? 0 : roleNames.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LProjectSecurityTemplate other = (LProjectSecurityTemplate) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (roleIds == null)
        {
            if (other.roleIds != null)
                return false;
        }
        else if (!roleIds.equals(other.roleIds))
            return false;
        if (roleNames == null)
        {
            if (other.roleNames != null)
                return false;
        }
        else if (!roleNames.equals(other.roleNames))
            return false;
        return true;
    }

}
