package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILConstituentLink;


public class LConstituentLink extends LBase implements ILConstituentLink
{
    private Long master;
    private Long slave;
    private String type;

    public void clear()
    {
        super.clear();
        master = null;
        slave = null;
        type = null;
    }

    public LConstituentLink()
    {
        super();
    }

    public static ILConstituentLink getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long master,
            Long slave, String type)
    {
        ILConstituentLink constituentlink = new LConstituentLink();
        constituentlink.setId(id);
        constituentlink.setVersion(version);
        constituentlink.setTags(tags);

        constituentlink.setMaster(master);
        constituentlink.setSlave(slave);
        constituentlink.setType(type);
        return constituentlink;
    }

    public static ILConstituentLink getNewInstance(Long master, Long slave, String type)
    {
        return getNewInstance(null, null, null, master, slave, type);
    }

    public Long getMaster()
    {
        return master;
    }

    public Long getSlave()
    {
        return slave;
    }

    public String getType()
    {
        return type;
    }

    public void setMaster(Long master)
    {
        this.master = master;
    }

    public void setSlave(Long slave)
    {
        this.slave = slave;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((master == null) ? 0 : master.hashCode());
        result = prime * result + ((slave == null) ? 0 : slave.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LConstituentLink other = (LConstituentLink) obj;
        if (master == null)
        {
            if (other.master != null)
                return false;
        }
        else if (!master.equals(other.master))
            return false;
        if (slave == null)
        {
            if (other.slave != null)
                return false;
        }
        else if (!slave.equals(other.slave))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
