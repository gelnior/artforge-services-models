/**
 * 
 */
package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.lightweight.ILPlanning;


/**
 * @author thomas-eskenazi
 * 
 */
public class LMileStone extends LBase implements ILMileStone
{
    ILPlanning planning;
    Long planningID;
    Date date;
    String title;
    String description;
    String color;

    public void clear()
    {
        super.clear();

        if (planning != null)
            planning.clear();
        planning = null;

        planningID = null;
        date = null;
        title = null;
        description = null;
        color = null;
    }

    /**
     * @return the planning
     */
    public ILPlanning getPlanning()
    {
        return planning;
    }

    /**
     * @param planning
     *            the planning to set
     */
    public void setPlanning(ILPlanning planning)
    {
        this.planning = planning;
    }

    /**
     * @return the planningID
     */
    public Long getPlanningID()
    {
        return planningID;
    }

    /**
     * @param planningID
     *            the planningID to set
     */
    public void setPlanningID(Long planningID)
    {
        this.planningID = planningID;
    }

    /**
     * @return the date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(Date date)
    {
        this.date = date;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return Milestone color.
     */
    public String getColor()
    {
        return color;
    }

    /**
     * Set milestone color.
     * 
     * @param color
     *            The color to set.
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((planningID == null) ? 0 : planningID.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LMileStone other = (LMileStone) obj;
        if (date == null)
        {
            if (other.date != null)
                return false;
        }
        else if (!date.equals(other.date))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (planningID == null)
        {
            if (other.planningID != null)
                return false;
        }
        else if (!planningID.equals(other.planningID))
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        return true;
    }

}
