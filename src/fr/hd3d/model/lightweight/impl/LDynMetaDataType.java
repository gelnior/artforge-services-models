package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILDynMetaDataType;


/**
 * @author Try LAM
 */
public class LDynMetaDataType extends LBase implements ILDynMetaDataType
{

    private String name;
    private String nature;// scalar, list
    private String type;// String, List, hql query, etc...
    private String defaultValue;
    private String structName;// name of the structure holding values
    private Long structId;// id of the structure holding values

    public void clear()
    {
        super.clear();
        name = null;
        nature = null;
        type = null;
        defaultValue = null;
        structName = null;
        structId = null;
    }

    public LDynMetaDataType()
    {}

    public static LDynMetaDataType getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            String defaultValue, String name, String nature, String type)
    {
        LDynMetaDataType dynMetaDataType = new LDynMetaDataType();

        dynMetaDataType.setId(id);
        dynMetaDataType.setVersion(version);
        dynMetaDataType.setTags(tags);
        dynMetaDataType.setDefaultValue(defaultValue);
        dynMetaDataType.setName(name);
        dynMetaDataType.setNature(nature);
        dynMetaDataType.setType(type);

        return dynMetaDataType;
    }

    public static LDynMetaDataType getNewInstance(String defaultValue, String name, String nature, String type)
    {
        return getNewInstance(null, null, null, defaultValue, name, nature, type);
    }

    public String getName()
    {
        return name;
    }

    public String getNature()
    {
        return nature;
    }

    public String getType()
    {
        return type;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public String getStructName()
    {
        return structName;
    }

    public Long getStructId()
    {
        return structId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setNature(String nature)
    {
        this.nature = nature;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public void setStructName(String structName)
    {
        this.structName = structName;
    }

    public void setStructId(Long structId)
    {
        this.structId = structId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nature == null) ? 0 : nature.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LDynMetaDataType other = (LDynMetaDataType) obj;
        if (defaultValue == null)
        {
            if (other.defaultValue != null)
                return false;
        }
        else if (!defaultValue.equals(other.defaultValue))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (nature == null)
        {
            if (other.nature != null)
                return false;
        }
        else if (!nature.equals(other.nature))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
