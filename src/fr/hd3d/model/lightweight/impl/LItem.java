package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILItem;


/**
 * @author Try LAM
 */
public class LItem extends LBase implements ILItem
{

    private String name;
    private String type;
    private String query;
    private String controlContent;
    private Long itemGroup;
    private String metaType;
    private String editor;
    private String renderer;
    private String transformer;// fullqualified java class name, applied on result of "query" field
    private String transformerParameters;// parameters used by transformer

    /*
     * for an Item of type = DynMetaDataValue and DynMetaDataType=List, provide the list of possible values
     */
    private List listValues;

    public void clear()
    {
        super.clear();
        name = null;
        type = null;
        query = null;
        controlContent = null;
        itemGroup = null;
        metaType = null;
        editor = null;
        renderer = null;
        transformer = null;
        transformerParameters = null;
        if (listValues != null)
            listValues.clear();
        listValues = null;
    }

    public LItem()
    {}

    public static LItem getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name, String type,
            String query, String controlContent, Long itemGroup, String metaType, String transformer,
            String transformerParameters)
    {
        final LItem item = new LItem();
        item.setId(id);
        item.setVersion(version);
        item.setTags(tags);
        item.setName(name);
        item.setType(type);
        item.setQuery(query);
        item.setControlContent(controlContent);
        item.setItemGroup(itemGroup);
        item.setMetaType(metaType);
        item.setTransformer(transformer);
        item.setTransformerParameters(transformerParameters);

        return item;
    }

    public static LItem getNewInstance(String name, String type, String query, String controlContent, Long itemGroup,
            String metaType, String transformer, String transformerParameters)
    {
        return getNewInstance(null, null, null, name, type, query, controlContent, itemGroup, metaType, transformer,
                transformerParameters);
    }

    public String getName()
    {
        return this.name;
    }

    public String getType()
    {
        return type;
    }

    public String getQuery()
    {
        return query;
    }

    public String getControlContent()
    {
        return controlContent;
    }

    public Long getItemGroup()
    {
        return itemGroup;
    }

    public String getMetaType()
    {
        return metaType;
    }

    public String getTransformer()
    {
        return transformer;
    }

    public String getTransformerParameters()
    {
        return transformerParameters;
    }

    public List getListValues()
    {
        return listValues;
    }

    public void setListValues(List listValues)
    {
        this.listValues = listValues;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public void setControlContent(String controlContent)
    {
        this.controlContent = controlContent;
    }

    public void setItemGroup(Long itemGroup)
    {
        this.itemGroup = itemGroup;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMetaType(String metaType)
    {
        this.metaType = metaType;
    }

    public String getEditor()
    {
        return editor;
    }

    public String getRenderer()
    {
        return renderer;
    }

    public void setEditor(String editor)
    {
        this.editor = editor;
    }

    public void setRenderer(String renderer)
    {
        this.renderer = renderer;
    }

    public void setTransformer(String transformer)
    {
        this.transformer = transformer;
    }

    public void setTransformerParameters(String transformerParameters)
    {
        this.transformerParameters = transformerParameters;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((controlContent == null) ? 0 : controlContent.hashCode());
        result = prime * result + ((editor == null) ? 0 : editor.hashCode());
        result = prime * result + ((itemGroup == null) ? 0 : itemGroup.hashCode());
        result = prime * result + ((metaType == null) ? 0 : metaType.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((query == null) ? 0 : query.hashCode());
        result = prime * result + ((renderer == null) ? 0 : renderer.hashCode());
        result = prime * result + ((transformer == null) ? 0 : transformer.hashCode());
        result = prime * result + ((transformerParameters == null) ? 0 : transformerParameters.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LItem other = (LItem) obj;
        if (controlContent == null)
        {
            if (other.controlContent != null)
                return false;
        }
        else if (!controlContent.equals(other.controlContent))
            return false;
        if (editor == null)
        {
            if (other.editor != null)
                return false;
        }
        else if (!editor.equals(other.editor))
            return false;
        if (itemGroup == null)
        {
            if (other.itemGroup != null)
                return false;
        }
        else if (!itemGroup.equals(other.itemGroup))
            return false;
        if (metaType == null)
        {
            if (other.metaType != null)
                return false;
        }
        else if (!metaType.equals(other.metaType))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (query == null)
        {
            if (other.query != null)
                return false;
        }
        else if (!query.equals(other.query))
            return false;
        if (renderer == null)
        {
            if (other.renderer != null)
                return false;
        }
        else if (!renderer.equals(other.renderer))
            return false;
        if (transformer == null)
        {
            if (other.transformer != null)
                return false;
        }
        else if (!transformer.equals(other.transformer))
            return false;
        if (transformerParameters == null)
        {
            if (other.transformerParameters != null)
                return false;
        }
        else if (!transformerParameters.equals(other.transformerParameters))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
