package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILScreen;


/**
 * Lightweight implementation for screen object.
 * 
 * @author HD3D
 */
public class LScreen extends LInventoryItem implements ILScreen
{
    private String qualification;
    private String type;
    private Float size;
    private Long screenModelId;
    private String screenModelName;
    private List<Long> computerIDs;
    private List<String> computerNames;

    public void clear()
    {
        super.clear();

        qualification = null;
        type = null;
        size = null;
        screenModelId = null;
        screenModelName = null;

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;
    }

    /**
     * Default Constructor.
     */
    public LScreen()
    {
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    public LScreen(Long id, Timestamp version, List<Long> tags, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Float size, String type, String qualification)
    {
        super(id, version, tags, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus);

        this.size = size;
        this.type = type;
        this.qualification = qualification;

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    public static ILScreen getNewInstance(Long id, Timestamp version, List<Long> tags, String name, String serial,
            String billingReference, Date purchaseDate, Date warrantyEnd, String inventoryStatus, Float size,
            String type, String qualification)
    {
        final LScreen screen = new LScreen(id, version, tags, name, serial, billingReference, purchaseDate,
                warrantyEnd, inventoryStatus, size, type, qualification);
        return screen;
    }

    public static ILScreen getNewInstance(String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, String inventoryStatus, Float size, String type, String qualification)
    {
        return getNewInstance(null, null, null, name, serial, billingReference, purchaseDate, warrantyEnd,
                inventoryStatus, size, type, qualification);
    }

    public String getQualification()
    {
        return qualification;
    }

    public void setQualification(String qualification)
    {
        this.qualification = qualification;
    }

    public Long getScreenModelId()
    {
        return screenModelId;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public void setScreenModelId(Long screenModelId)
    {
        this.screenModelId = screenModelId;
    }

    public String getScreenModelName()
    {
        return screenModelName;
    }

    public void setScreenModelName(String screenModelName)
    {
        this.screenModelName = screenModelName;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Float getSize()
    {
        return size;
    }

    public void setSize(Float size)
    {
        this.size = size;
    }

    public List<Long> getComputerIDs()
    {
        return computerIDs;
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public List<String> getComputerNames()
    {
        return computerNames;
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerIDs == null) ? 0 : computerIDs.hashCode());
        result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
        result = prime * result + ((screenModelId == null) ? 0 : screenModelId.hashCode());
        result = prime * result + ((screenModelName == null) ? 0 : screenModelName.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LScreen other = (LScreen) obj;
        if (computerIDs == null)
        {
            if (other.computerIDs != null)
                return false;
        }
        else if (!computerIDs.equals(other.computerIDs))
            return false;
        if (qualification == null)
        {
            if (other.qualification != null)
                return false;
        }
        else if (!qualification.equals(other.qualification))
            return false;
        if (screenModelId == null)
        {
            if (other.screenModelId != null)
                return false;
        }
        else if (!screenModelId.equals(other.screenModelId))
            return false;
        if (screenModelName == null)
        {
            if (other.screenModelName != null)
                return false;
        }
        else if (!screenModelName.equals(other.screenModelName))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
