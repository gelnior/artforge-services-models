package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.hd3d.model.lightweight.ILTag;


/**
 * @author Try LAM
 */
public class LTag extends LBase implements ILTag
{
    private String name;
    private Collection<LTagParentIdType> parents;

    public void clear()
    {
        super.clear();
        name = null;
        if (parents != null)
        {
            for (LTagParentIdType x : parents)
                x.clear();
            parents.clear();
        }
        parents = null;
    }

    public LTag()
    {
        super();
        parents = new ArrayList<LTagParentIdType>();
    }

    public static ILTag getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            Collection<LTagParentIdType> parents)
    {
        final LTag tag = new LTag();
        tag.setId(id);
        tag.setVersion(version);
        tag.setTags(tags);
        tag.setName(name);
        if (parents != null)
        {
            tag.setParents(parents);
        }
        return tag;
    }

    public static ILTag getNewInstance(String name, Collection<LTagParentIdType> parents)
    {
        return getNewInstance(null, null, null, name, parents);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<LTagParentIdType> getParents()
    {
        return this.parents;
    }

    public void setParents(Collection<LTagParentIdType> parents)
    {
        this.parents = parents;
    }

    public void addParent(Long parentId, String parentType)
    {
        if (this.parents != null)
        {
            boolean exist = false;
            for (LTagParentIdType idtype : this.parents)
            {
                if (idtype.getParentId().equals(parentId) && idtype.getParentType().equals(parentType))
                {
                    exist = true;
                }
            }
            if (!exist)
            {
                this.parents.add(new LTagParentIdType(parentId, parentType));
            }
        }
    }

    public void removeParent(Long parentId, String parentType)
    {
        if (this.parents != null)
        {
            for (LTagParentIdType idtype : this.parents)
            {
                if (idtype.getParentId().equals(parentId) && idtype.getParentType().equals(parentType))
                {
                    this.parents.remove(idtype);
                    break;
                }
            }
        }
    }

    public static class LTagParentIdType
    {
        private Long parentId;
        private String parentType;

        public void clear()
        {
            parentId = null;
            parentType = null;
        }

        public LTagParentIdType()
        {}

        public LTagParentIdType(Long parentId, String parentType)
        {
            this.parentId = parentId;
            this.parentType = parentType;
        }

        public Long getParentId()
        {
            return parentId;
        }

        public String getParentType()
        {
            return parentType;
        }

        public void setParentId(Long parentId)
        {
            this.parentId = parentId;
        }

        public void setParentType(String parenttype)
        {
            this.parentType = parenttype;
        }

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
            result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            LTagParentIdType other = (LTagParentIdType) obj;
            if (parentId == null)
            {
                if (other.parentId != null)
                    return false;
            }
            else if (!parentId.equals(other.parentId))
                return false;
            if (parentType == null)
            {
                if (other.parentType != null)
                    return false;
            }
            else if (!parentType.equals(other.parentType))
                return false;
            return true;
        }

    };

}
