package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.List;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.model.lightweight.ILSimpleActivity;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public class LSimpleActivity extends LActivity implements ILSimpleActivity
{

    private String type;

    public void clear()
    {
        super.clear();
        type = null;
    }

    /**
     * Default CTor
     */
    public LSimpleActivity()
    {}

    public static LSimpleActivity getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            ESimpleActivityType type, Long dayID, Long duration, String comment, Long workerId, Long projectId,
            Long filledByID, Date filledDate)
    {
        final LSimpleActivity activity = new LSimpleActivity();
        activity.setId(id);
        activity.setVersion(version);
        activity.setTags(tags);
        activity.setDayID(dayID);
        activity.setDuration(duration);
        activity.setComment(comment);
        activity.setWorkerID(workerId);
        activity.setProjectID(projectId);
        activity.setType(type.toString());
        activity.setFilledByID(filledByID);
        activity.setFilledDate(filledDate);
        return activity;
    }

    public static LSimpleActivity getNewInstance(ESimpleActivityType type, Long dayID, Long duration, String comment,
            Long workerId, Long projectId, Long filledByID, Date filledDate)
    {
        return getNewInstance(null, null, null, type, dayID, duration, comment, workerId, projectId, filledByID,
                filledDate);
    }

    public final String getType()
    {
        return type;
    }

    public final void setType(String type)
    {
        this.type = type;
    }

    // Changing visibility for workerID
    @Override
    public final void setWorkerID(Long workerID)
    {
        super.setWorkerID(workerID);
    }

    // Changing visibility for projectID
    @Override
    public final void setProjectID(Long projectID)
    {
        super.setProjectID(projectID);
    }

    @Override
    public String getTaskName()
    {
        return type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSimpleActivity other = (LSimpleActivity) obj;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
