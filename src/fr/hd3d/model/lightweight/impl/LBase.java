package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.lightweight.ILTask;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public abstract class LBase implements ILBase
{
    private Long id;
    private java.sql.Timestamp version;
    private String internalUUID;
    private List<Long> tags;
    private List<String> tagNames;
    private Collection<ILDynMetaDataValue> dynMetaDataValues;
    private Set<ILEntityTaskLink> entityTaskLinks;
    protected Set<ILApprovalNote> approvalNotes;
    protected Set<ILTask> boundTasks;

    private boolean userCanUpdate;
    private boolean userCanDelete;
    private String defaultPath;

    public void clear()
    {
        id = null;
        version = null;
        internalUUID = null;

        if (tags != null && !tags.isEmpty())
            tags.clear();
        tags = null;

        if (tagNames != null && !tagNames.isEmpty())
            tagNames.clear();
        tagNames = null;

        if (dynMetaDataValues != null)
        {
            for (ILDynMetaDataValue dyn : dynMetaDataValues)
                dyn.clear();
            dynMetaDataValues.clear();
        }
        dynMetaDataValues = null;

        if (entityTaskLinks != null)
        {
            for (ILEntityTaskLink tasklink : entityTaskLinks)
                tasklink.clear();
            entityTaskLinks.clear();
        }
        entityTaskLinks = null;

        if (approvalNotes != null)
        {
            for (ILApprovalNote note : approvalNotes)
                note.clear();
            approvalNotes.clear();
        }
        approvalNotes = null;

        if (boundTasks != null)
        {
            for (ILTask task : boundTasks)
                task.clear();
            boundTasks.clear();
        }
        boundTasks = null;

        defaultPath = null;
    }

    public LBase()
    {
        this.id = null;
        this.version = null;
        this.tags = null;
    }

    protected LBase(Long id, java.sql.Timestamp version)
    {
        this.id = id;
        this.version = version;
    }

    protected LBase(Long id, java.sql.Timestamp version, List<Long> tags)
    {
        this.id = id;
        this.version = version;
        if (tags != null)
        {
            this.tags = tags;
        }
        else
        {
            this.tags = new ArrayList<Long>();
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    public String getInternalUUID()
    {
        return internalUUID;
    }

    public void setInternalUUID(String internalUUID)
    {
        this.internalUUID = internalUUID;
    }

    public List<Long> getTags()
    {
        if (tags == null)
        {
            tags = new ArrayList<Long>(0);
        }
        return tags;
    }

    public void setTags(List<Long> tags)
    {
        this.tags = tags;
    }

    public void addTag(Long tagId)
    {
        if (!this.tags.contains(tagId))
        {
            this.tags.add(tagId);
        }
    }

    public List<String> getTagNames()
    {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames)
    {
        this.tagNames = tagNames;
    }

    public Collection<ILDynMetaDataValue> getDynMetaDataValues()
    {
        return dynMetaDataValues;
    }

    public void setDynMetaDataValues(Collection<ILDynMetaDataValue> dynMetaDataValues)
    {
        this.dynMetaDataValues = dynMetaDataValues;
    }

    public Set<ILEntityTaskLink> getEntityTaskLinks()
    {
        return entityTaskLinks;
    }

    public void setEntityTaskLinks(Set<ILEntityTaskLink> entityTaskLinks)
    {
        this.entityTaskLinks = entityTaskLinks;
    }

    public Set<ILApprovalNote> getApprovalNotes()
    {
        return approvalNotes;
    }

    public void setApprovalNotes(Set<ILApprovalNote> approvalNotes)
    {
        this.approvalNotes = approvalNotes;
    }

    public Set<ILTask> getBoundTasks()
    {
        return boundTasks;
    }

    public void setBoundTasks(Set<ILTask> boundTasks)
    {
        this.boundTasks = boundTasks;
    }

    public boolean getUserCanUpdate()
    {
        return userCanUpdate;
    }

    public boolean getUserCanDelete()
    {
        return userCanDelete;
    }

    public void setUserCanUpdate(boolean canUpdate)
    {
        userCanUpdate = canUpdate;
    }

    public void setUserCanDelete(boolean canDelete)
    {
        userCanDelete = canDelete;
    }

    public String getDefaultPath()
    {
        return defaultPath;
    }

    public void setDefaultPath(String path)
    {
        defaultPath = path;
    }

    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append("id=" + id);
        // if (tagNames != null)
        // buf.append("tagNames=" + tagNames).append(',');
        // if (dynMetaDataValues != null)
        // buf.append("dynMetaDataValues=" + dynMetaDataValues.toString()).append(',');
        // if (entityTaskLinks != null)
        // buf.append("entityTaskLinks=" + entityTaskLinks).append(',');
        // if (approvalNotes != null)
        // buf.append("approvalNotes=" + approvalNotes).append(',');
        // if (boundTasks != null)
        // buf.append("boundTasks=" + boundTasks);

        return buf.toString();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LBase other = (LBase) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }
}
