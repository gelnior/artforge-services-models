package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILModel;


/**
 * Lightweight implementation for object with a name.
 * 
 * @author HD3D
 */
public class LModel extends LSimple implements ILModel
{
    private Long manufacturerId;
    private String manufacturerName;

    public void clear()
    {
        super.clear();
        manufacturerId = null;
        manufacturerName = null;
    }

    /**
     * Default Constructor.
     */
    public LModel()
    {}

    public LModel(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public Long getManufacturerId()
    {
        return manufacturerId;
    }

    public String getManufacturerName()
    {
        return manufacturerName;
    }

    public void setManufacturerId(Long manufacturerId)
    {
        this.manufacturerId = manufacturerId;
    }

    public void setManufacturerName(String manufacturerName)
    {
        this.manufacturerName = manufacturerName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((manufacturerId == null) ? 0 : manufacturerId.hashCode());
        result = prime * result + ((manufacturerName == null) ? 0 : manufacturerName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LModel other = (LModel) obj;
        if (manufacturerId == null)
        {
            if (other.manufacturerId != null)
                return false;
        }
        else if (!manufacturerId.equals(other.manufacturerId))
            return false;
        if (manufacturerName == null)
        {
            if (other.manufacturerName != null)
                return false;
        }
        else if (!manufacturerName.equals(other.manufacturerName))
            return false;
        return true;
    }

}
