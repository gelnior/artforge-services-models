package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILRoom;


/**
 * Lightweight implementation for room object.
 * 
 * @author HD3D
 */
public class LRoom extends LSimple implements ILRoom
{
    /**
     * Default Constructor.
     */
    public LRoom()
    {}

    public LRoom(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILRoom getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LRoom room = new LRoom(id, version, tags, name);

        return room;
    }

    public static ILRoom getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
