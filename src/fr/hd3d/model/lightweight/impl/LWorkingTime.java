/**
 * 
 */
package fr.hd3d.model.lightweight.impl;

import fr.hd3d.model.lightweight.ILWorkingTime;


/**
 * @author Try LAM
 * 
 */
public class LWorkingTime extends LDurableImpl implements ILWorkingTime
{
    private Long personDayId;

    public void clear()
    {
        super.clear();
        personDayId = null;
    }

    public Long getPersonDayId()
    {
        return personDayId;
    }

    public void setPersonDayId(Long personDayId)
    {
        this.personDayId = personDayId;
    }

}
