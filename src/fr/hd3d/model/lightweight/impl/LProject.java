package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.ILTaskType;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html /**
 * @author Try LAM
 */
public class LProject extends LSimple implements ILProject
{
    private String status;
    private Date startDate;
    private Date endDate;
    private String color;
    private LinkedHashSet<Long> resourceGroupIDs;
    private List<String> resourceGroupNames;
    private List<ILResourceGroup> resourceGroups;

    private LinkedHashSet<Long> taskTypeIDs;
    private List<String> taskTypeNames;
    private List<String> taskTypeColors;
    private List<ILTaskType> taskTypes;

    private String hook;
    private Long projectTypeId;
    private String projectTypeName;
    private Long rootCategoryId;
    private Long rootSequenceId;

    /**
     * Default CTor
     */
    public LProject()
    {}

    public void clear()
    {
        super.clear();
        name = null;
        status = null;
        color = null;

        if (resourceGroupIDs != null)
            resourceGroupIDs.clear();
        resourceGroupIDs = null;

        if (resourceGroupNames != null)
            resourceGroupNames.clear();
        resourceGroupNames = null;

        if (resourceGroups != null)
        {
            for (ILResourceGroup group : resourceGroups)
                group.clear();
            resourceGroups.clear();
        }
        resourceGroups = null;

        hook = null;
        projectTypeId = null;
        projectTypeName = null;
    }

    public static ILProject getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String status, Date startDate, Date endDate, String color, String hook, Long projectTypeId,
            String projectTypeName)
    {
        final LProject project = new LProject();
        project.setId(id);
        project.setVersion(version);
        project.setTags(tags);
        project.setName(name);
        project.setStatus(status);
        project.setStartDate(startDate);
        project.setEndDate(endDate);
        project.setColor(color);
        project.setResourceGroupIDs(new LinkedHashSet<Long>());
        project.setResourceGroupNames(new ArrayList<String>());
        project.setHook(hook);
        project.setProjectTypeId(projectTypeId);
        project.setProjectTypeName(projectTypeName);

        return project;
    }

    public static ILProject getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String status, String color, String hook)
    {
        final LProject project = new LProject();
        project.setId(id);
        project.setVersion(version);
        project.setTags(tags);
        project.setName(name);
        project.setStatus(status);
        project.setColor(color);
        project.setResourceGroupIDs(new LinkedHashSet<Long>());
        project.setResourceGroupNames(new ArrayList<String>());
        project.setHook(hook);
        return project;
    }

    public static ILProject getNewInstance(String name, String status, String color, String hook)
    {
        return getNewInstance(null, null, null, name, status, null, null, color, hook, null, null);
    }

    public static ILProject getNewInstance(String name, String status, String color, String hook, Long projectTypeId,
            String projectTypeName)
    {
        return getNewInstance(null, null, null, name, status, null, null, color, hook, projectTypeId, projectTypeName);
    }

    public String getProjectTypeName()
    {
        return projectTypeName;
    }

    public void setProjectTypeName(String projectTypeName)
    {
        this.projectTypeName = projectTypeName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStatus()
     */
    public final String getStatus()
    {
        return status;
    }

    public final Date getStartDate()
    {
        return startDate;
    }

    public final Date getEndDate()
    {
        return endDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getColor()
     */
    public final String getColor()
    {
        return color;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStaffIDs()
     */
    public final LinkedHashSet<Long> getResourceGroupIDs()
    {
        return resourceGroupIDs;
    }

    public void addResourceGroupID(Long id)
    {
        if (getResourceGroupIDs() == null)
            setResourceGroupIDs(new LinkedHashSet<Long>());
        getResourceGroupIDs().add(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStaff()
     */
    public final List<ILResourceGroup> getResourceGroups()
    {
        return resourceGroups;
    }

    public final LinkedHashSet<Long> getTaskTypeIDs()
    {
        return taskTypeIDs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStaffNames()
     */
    public final List<String> getTaskTypeNames()
    {
        return this.taskTypeNames;
    }

    public void addTaskTypeID(Long id)
    {
        if (getTaskTypeIDs() == null)
            setTaskTypeIDs(new LinkedHashSet<Long>());
        getTaskTypeIDs().add(id);
    }

    public void addTaskTypeName(String name)
    {
        if (getTaskTypeNames() == null)
            setTaskTypeNames(new ArrayList<String>());
        getTaskTypeNames().add(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStaff()
     */
    public final List<ILTaskType> getTaskTypes()
    {
        return taskTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#getStaffNames()
     */
    public final List<String> getResourceGroupNames()
    {
        return this.resourceGroupNames;
    }

    public void addResourceGroupName(String name)
    {
        if (getResourceGroupNames() == null)
            setResourceGroupNames(new ArrayList<String>());
        getResourceGroupNames().add(name);
    }

    public String getHook()
    {
        return hook;
    }

    public Long getProjectTypeId()
    {
        return projectTypeId;
    }

    public void setProjectTypeId(Long projectTypeId)
    {
        this.projectTypeId = projectTypeId;
    }

    public Long getRootCategoryId()
    {
        return rootCategoryId;
    }

    public void setRootCategoryId(Long rootCategoryId)
    {
        this.rootCategoryId = rootCategoryId;
    }

    public Long getRootSequenceId()
    {
        return rootSequenceId;
    }

    public void setRootSequenceId(Long rootSequenceId)
    {
        this.rootSequenceId = rootSequenceId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStatus(java.lang.String)
     */
    public final void setStatus(String status)
    {
        this.status = status;
    }

    public final void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public final void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setColor(java.lang.String)
     */
    public final void setColor(String color)
    {
        this.color = color;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStaffIDs(java.util.List)
     */
    public final void setResourceGroupIDs(LinkedHashSet<Long> resourceGroupIDs)
    {
        this.resourceGroupIDs = resourceGroupIDs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStaff(java.util.List)
     */
    public final void setResourceGroup(List<ILResourceGroup> resourceGroups)
    {
        this.resourceGroups = resourceGroups;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStaffNames(java.util.List)
     */
    public final void setTaskTypeNames(List<String> taskTypeNames)
    {
        this.taskTypeNames = taskTypeNames;
    }

    public final void setTaskTypeIDs(LinkedHashSet<Long> taskTypeIDs)
    {
        this.taskTypeIDs = taskTypeIDs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStaff(java.util.List)
     */
    public final void setTaskType(List<ILTaskType> taskTypes)
    {
        this.taskTypes = taskTypes;
    }

    public List<String> getTaskTypeColors()
    {
        return this.taskTypeColors;
    }

    private void setTaskTypeColors(ArrayList<String> taskTypeColors)
    {
        this.taskTypeColors = taskTypeColors;
    }

    public void addTaskTypeColor(String color)
    {
        if (getTaskTypeColors() == null)
            setTaskTypeColors(new ArrayList<String>());
        getTaskTypeColors().add(color);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILProject#setStaffNames(java.util.List)
     */
    public final void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.resourceGroupNames = resourceGroupNames;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((resourceGroupIDs == null) ? 0 : resourceGroupIDs.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LProject other = (LProject) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (resourceGroupIDs == null)
        {
            if (other.resourceGroupIDs != null)
                return false;
        }
        else if (!resourceGroupIDs.equals(other.resourceGroupIDs))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        return true;
    }

}
