package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILSoftware;


/**
 * Lightweight implementation for software object.
 * 
 * @author HD3D
 */
public class LSoftware extends LSimple implements ILSoftware
{
    private List<Long> computerIDs;
    private List<String> computerNames;
    private List<Long> licenseIDs;
    private List<String> licenseNames;

    public void clear()
    {
        super.clear();

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;

        if (licenseIDs != null)
            licenseIDs.clear();
        licenseIDs = null;

        if (licenseNames != null)
            licenseNames.clear();
        licenseNames = null;
    }

    /**
     * Default Constructor.
     */
    public LSoftware()
    {}

    public LSoftware(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
        this.setLicenseIDs(new ArrayList<Long>());
        this.setLicenseNames(new ArrayList<String>());
    }

    /**
     * @param id
     *            Database object id.
     * @param version
     *            Database object version.
     * @param name
     *            Software name.
     * @param serial
     *            Software serial number.
     * @param purchaseDate
     *            Software purchase date.
     * @param numberOfLicenses
     *            Number of Licenses for this software.
     * 
     * @return a new instance of lightweight software.
     */
    public static ILSoftware getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LSoftware software = new LSoftware(id, version, tags, name);

        return software;
    }

    /**
     * @param name
     *            Software name.
     * @param serial
     *            Software serial number.
     * @param purchaseDate
     *            Software purchase date.
     * @param numberOfLicenses
     *            Number of Licenses for this software.
     * 
     * @return a new instance of lightweight software with id, version and tags set to null..
     */
    public static ILSoftware getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }

    public List<Long> getLicenseIDs()
    {
        return licenseIDs;
    }

    public void addLicenseID(Long id)
    {
        if (getLicenseIDs() == null)
            setLicenseIDs(new ArrayList<Long>());
        getLicenseIDs().add(id);
    }

    public void addLicenseName(String name)
    {
        if (getLicenseNames() == null)
            setLicenseNames(new ArrayList<String>());
        getLicenseNames().add(name);
    }

    public void setLicenseIDs(List<Long> licenseIDs)
    {
        if (this.licenseIDs == null)
        {
            this.licenseIDs = new ArrayList<Long>();
        }
        this.licenseIDs = licenseIDs;
    }

    public List<String> getLicenseNames()
    {
        return licenseNames;
    }

    public void setLicenseNames(List<String> licenseNames)
    {
        if (this.licenseNames == null)
        {
            this.licenseNames = new ArrayList<String>();
        }
        this.licenseNames = licenseNames;
    }

    public final List<Long> getComputerIDs()
    {
        return this.computerIDs;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public final void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public final List<String> getComputerNames()
    {
        return this.computerNames;
    }

    public final void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerIDs == null) ? 0 : computerIDs.hashCode());
        result = prime * result + ((licenseIDs == null) ? 0 : licenseIDs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSoftware other = (LSoftware) obj;
        if (computerIDs == null)
        {
            if (other.computerIDs != null)
                return false;
        }
        else if (!computerIDs.equals(other.computerIDs))
            return false;
        if (licenseIDs == null)
        {
            if (other.licenseIDs != null)
                return false;
        }
        else if (!licenseIDs.equals(other.licenseIDs))
            return false;
        return true;
    }

}
