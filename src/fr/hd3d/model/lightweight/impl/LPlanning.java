package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILPlanning;


/**
 * @author michael.guiral
 * 
 */
public class LPlanning extends LBase implements ILPlanning
{
    private String name;
    private Long project;
    private Boolean master;
    private Date startDate;
    private Date endDate;

    public void clear()
    {
        super.clear();

        name = null;
        project = null;
        master = null;
        startDate = null;
        endDate = null;
    }

    public LPlanning()
    {}

    public static ILPlanning getNewInstance(Long projectId, String name, Boolean master, Date startDate, Date endDate)
    {
        final LPlanning planning = new LPlanning();
        planning.setName(name);
        planning.setProject(projectId);
        planning.setMaster(master);
        planning.setStartDate(startDate);
        return planning;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getProject()
    {
        return project;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public Boolean getMaster()
    {
        return master;
    }

    public void setMaster(Boolean master)
    {
        this.master = master;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((master == null) ? 0 : master.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPlanning other = (LPlanning) obj;
        if (endDate == null)
        {
            if (other.endDate != null)
                return false;
        }
        else if (!endDate.equals(other.endDate))
            return false;
        if (master == null)
        {
            if (other.master != null)
                return false;
        }
        else if (!master.equals(other.master))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (startDate == null)
        {
            if (other.startDate != null)
                return false;
        }
        else if (!startDate.equals(other.startDate))
            return false;
        return true;
    }

}
