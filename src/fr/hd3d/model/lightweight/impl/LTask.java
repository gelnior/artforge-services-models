package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.List;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskType;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public class LTask extends LTaskBase implements ILTask
{
    private String name;
    private String status;
    private Byte completion;
    private Date completionDate;
    private ILProject project;
    private Long projectID;
    private String projectName;
    private String projectColor;
    private ILPerson creator;
    private Long creatorID;
    private String creatorName;
    private ILPerson worker;
    private Long workerID;
    private String workerName;
    private Date deadLine;
    private Date actualStartDate;
    private Date actualEndDate;
    private Boolean startable;
    private Boolean confirmed;
    private Boolean startup;
    private Long taskTypeID;
    private ILTaskType taskType;
    private String color;
    private String taskTypeName;
    private Long boundEntityTaskLink;
    private String boundEntityName;
    private Long workObjectId;
    private String workObjectName;
    private String projectTypeName;
    private String workObjectParentsNames;// for Constituents and Shots
    private String commentForApprovalNote;
    private Long totalActivitiesDuration;

    private List<Long> fileRevisionsForApprovalNote;

    public void clear()
    {
        super.clear();
        name = null;
        status = null;
        completion = null;
        completionDate = null;

        if (project != null)
            project.clear();
        project = null;

        projectID = null;
        projectName = null;
        projectColor = null;

        if (creator != null)
            creator.clear();
        creator = null;

        creatorID = null;
        creatorName = null;

        if (worker != null)
            worker.clear();
        worker = null;

        workerID = null;
        workerName = null;
        deadLine = null;
        actualStartDate = null;
        actualEndDate = null;
        startable = null;
        confirmed = null;
        startup = null;
        taskTypeID = null;

        if (taskType != null)
            taskType.clear();
        taskType = null;

        color = null;
        taskTypeName = null;
        boundEntityTaskLink = null;
        workObjectParentsNames = null;
        commentForApprovalNote = null;
        fileRevisionsForApprovalNote = null;
    }

    /**
     * Default CTor
     */
    public LTask()
    {}

    public static ILTask getNewInstance(Long projectID, String name, ETaskStatus status, Long creatorID, Long workerID,
            Long boundEntity, String boundEntityName, Long taskTypeID)
    {
        final LTask task = new LTask();
        task.setProjectID(projectID);
        task.setName(name);
        task.setStatus(status.toString());
        task.setCreatorID(creatorID);
        task.setWorkerID(workerID);
        task.setCompletion(new Byte((byte) 0));// :-(
        task.taskTypeID = taskTypeID;
        task.boundEntityName = boundEntityName;
        task.workObjectId = boundEntity;
        return task;
    }

    public final String getName()
    {
        return name;
    }

    public final String getStatus()
    {
        return status;
    }

    public final Byte getCompletion()
    {
        return completion;
    }

    public final Date getCompletionDate()
    {
        return completionDate;
    }

    public final ILProject getProject()
    {
        return project;
    }

    public final Long getProjectID()
    {
        return projectID;
    }

    public final String getProjectName()
    {
        return projectName;
    }

    public final String getProjectColor()
    {
        return projectColor;
    }

    public final ILPerson getCreator()
    {
        return creator;
    }

    public final Long getCreatorID()
    {
        return creatorID;
    }

    public final String getCreatorName()
    {
        return creatorName;
    }

    public final ILPerson getWorker()
    {
        return worker;
    }

    public final Long getWorkerID()
    {
        return workerID;
    }

    public final String getWorkerName()
    {
        return workerName;
    }

    public Long getBoundEntityTaskLink()
    {
        return boundEntityTaskLink;
    }

    public void setBoundEntityTaskLink(Long boundEntityTaskLink)
    {
        this.boundEntityTaskLink = boundEntityTaskLink;
    }

    public final void setName(String name)
    {
        this.name = name;
    }

    public final void setStatus(String status)
    {
        this.status = status;
    }

    public final void setCompletion(Byte completion)
    {
        this.completion = completion;
    }

    public final void setCompletionDate(Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public final void setProject(ILProject project)
    {
        this.project = project;
    }

    public final void setProjectID(Long projectID)
    {
        this.projectID = projectID;
    }

    public final void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public final void setProjectColor(String projectColor)
    {
        this.projectColor = projectColor;
    }

    public final void setCreator(ILPerson creator)
    {
        this.creator = creator;
    }

    public final void setCreatorID(Long creatorID)
    {
        this.creatorID = creatorID;
    }

    public final void setCreatorName(String creatorName)
    {
        this.creatorName = creatorName;
    }

    public final void setWorker(ILPerson worker)
    {
        this.worker = worker;
    }

    public final void setWorkerID(Long workerID)
    {
        this.workerID = workerID;
    }

    public final void setWorkerName(String workerName)
    {
        this.workerName = workerName;
    }

    public Date getDeadLine()
    {
        return deadLine;
    }

    public void setDeadLine(Date deadLine)
    {
        this.deadLine = deadLine;
    }

    public Date getActualStartDate()
    {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate)
    {
        this.actualStartDate = actualStartDate;
    }

    public Date getActualEndDate()
    {
        return actualEndDate;
    }

    public void setActualEndDate(Date actualEndDate)
    {
        this.actualEndDate = actualEndDate;
    }

    public Boolean getStartable()
    {
        return startable;
    }

    public void setStartable(Boolean startable)
    {
        this.startable = startable;
    }

    public Boolean getConfirmed()
    {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    public Boolean getStartup()
    {
        return startup;
    }

    public void setStartup(Boolean startup)
    {
        this.startup = startup;
    }

    /**
     * @return the taskTypeID
     */
    public Long getTaskTypeID()
    {
        return taskTypeID;
    }

    /**
     * @param taskTypeID
     *            the taskTypeID to set
     */
    public void setTaskTypeID(Long taskTypeID)
    {
        this.taskTypeID = taskTypeID;
    }

    /**
     * @return the taskType
     */
    public ILTaskType getTaskType()
    {
        return taskType;
    }

    /**
     * @param taskType
     *            the taskType to set
     */
    public void setTaskType(ILTaskType taskType)
    {
        this.taskType = taskType;
    }

    /**
     * @return the color
     */
    public String getColor()
    {
        return color;
    }

    /**
     * @param color
     *            the color to set
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    /**
     * @return the taskTypeName
     */
    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    /**
     * @param taskTypeName
     *            the taskTypeName to set
     */
    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public String getProjectTypeName()
    {
        return projectTypeName;
    }

    public void setProjectTypeName(String projectTypeName)
    {
        this.projectTypeName = projectTypeName;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public Long getWorkObjectId()
    {
        return workObjectId;
    }

    public void setWorkObjectId(Long workObjectId)
    {
        this.workObjectId = workObjectId;
    }

    public String getWorkObjectName()
    {
        return workObjectName;
    }

    public void setWorkObjectName(String workObjectName)
    {
        this.workObjectName = workObjectName;
    }

    public String getWorkObjectParentsNames()
    {
        return workObjectParentsNames;
    }

    public void setWorkObjectParentsNames(String parentsNames)
    {
        this.workObjectParentsNames = parentsNames;
    }

    public String getCommentForApprovalNote()
    {
        return commentForApprovalNote;
    }

    public void setCommentForApprovalNote(String commentForApprovalNote)
    {
        this.commentForApprovalNote = commentForApprovalNote;
    }

    public Long getTotalActivitiesDuration()
    {
        return totalActivitiesDuration;
    }

    public void setTotalActivitiesDuration(Long totalActivitiesDuration)
    {
        this.totalActivitiesDuration = totalActivitiesDuration;
    }

    public List<Long> getFileRevisionsForApprovalNote()
    {
        return fileRevisionsForApprovalNote;
    }

    public void setFileRevisionsForApprovalNote(List<Long> fileRevisionForApprovalNotes)
    {
        this.fileRevisionsForApprovalNote = fileRevisionForApprovalNotes;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((actualEndDate == null) ? 0 : actualEndDate.hashCode());
        result = prime * result + ((actualStartDate == null) ? 0 : actualStartDate.hashCode());
        result = prime * result + ((boundEntityTaskLink == null) ? 0 : boundEntityTaskLink.hashCode());
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((completion == null) ? 0 : completion.hashCode());
        result = prime * result + ((completionDate == null) ? 0 : completionDate.hashCode());
        result = prime * result + ((confirmed == null) ? 0 : confirmed.hashCode());
        result = prime * result + ((creatorID == null) ? 0 : creatorID.hashCode());
        result = prime * result + ((creatorName == null) ? 0 : creatorName.hashCode());
        result = prime * result + ((deadLine == null) ? 0 : deadLine.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((projectColor == null) ? 0 : projectColor.hashCode());
        result = prime * result + ((projectID == null) ? 0 : projectID.hashCode());
        result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
        result = prime * result + ((startable == null) ? 0 : startable.hashCode());
        result = prime * result + ((startup == null) ? 0 : startup.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((taskTypeID == null) ? 0 : taskTypeID.hashCode());
        result = prime * result + ((taskTypeName == null) ? 0 : taskTypeName.hashCode());
        result = prime * result + ((workerID == null) ? 0 : workerID.hashCode());
        result = prime * result + ((workerName == null) ? 0 : workerName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTask other = (LTask) obj;
        if (actualEndDate == null)
        {
            if (other.actualEndDate != null)
                return false;
        }
        else if (!actualEndDate.equals(other.actualEndDate))
            return false;
        if (actualStartDate == null)
        {
            if (other.actualStartDate != null)
                return false;
        }
        else if (!actualStartDate.equals(other.actualStartDate))
            return false;
        if (boundEntityTaskLink == null)
        {
            if (other.boundEntityTaskLink != null)
                return false;
        }
        else if (!boundEntityTaskLink.equals(other.boundEntityTaskLink))
            return false;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (completion == null)
        {
            if (other.completion != null)
                return false;
        }
        else if (!completion.equals(other.completion))
            return false;
        if (completionDate == null)
        {
            if (other.completionDate != null)
                return false;
        }
        else if (!completionDate.equals(other.completionDate))
            return false;
        if (confirmed == null)
        {
            if (other.confirmed != null)
                return false;
        }
        else if (!confirmed.equals(other.confirmed))
            return false;
        if (creatorID == null)
        {
            if (other.creatorID != null)
                return false;
        }
        else if (!creatorID.equals(other.creatorID))
            return false;
        if (creatorName == null)
        {
            if (other.creatorName != null)
                return false;
        }
        else if (!creatorName.equals(other.creatorName))
            return false;
        if (deadLine == null)
        {
            if (other.deadLine != null)
                return false;
        }
        else if (!deadLine.equals(other.deadLine))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (projectColor == null)
        {
            if (other.projectColor != null)
                return false;
        }
        else if (!projectColor.equals(other.projectColor))
            return false;
        if (projectID == null)
        {
            if (other.projectID != null)
                return false;
        }
        else if (!projectID.equals(other.projectID))
            return false;
        if (projectName == null)
        {
            if (other.projectName != null)
                return false;
        }
        else if (!projectName.equals(other.projectName))
            return false;
        if (startable == null)
        {
            if (other.startable != null)
                return false;
        }
        else if (!startable.equals(other.startable))
            return false;
        if (startup == null)
        {
            if (other.startup != null)
                return false;
        }
        else if (!startup.equals(other.startup))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (taskTypeID == null)
        {
            if (other.taskTypeID != null)
                return false;
        }
        else if (!taskTypeID.equals(other.taskTypeID))
            return false;
        if (taskTypeName == null)
        {
            if (other.taskTypeName != null)
                return false;
        }
        else if (!taskTypeName.equals(other.taskTypeName))
            return false;
        if (workerID == null)
        {
            if (other.workerID != null)
                return false;
        }
        else if (!workerID.equals(other.workerID))
            return false;
        if (workerName == null)
        {
            if (other.workerName != null)
                return false;
        }
        else if (!workerName.equals(other.workerName))
            return false;
        return true;
    }

}
