package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILDynMetaDataValue;


/**
 * @author Try LAM
 */
public class LDynMetaDataValue extends LBase implements ILDynMetaDataValue
{
    private Long classDynMetaDataType;
    private String classDynMetaDataTypeName;
    private Long parent;
    private String parentType;// needed in LightWeight model, can be obtained in DynMetaDataValue via dynMetaDataType
    private Object value;
    private String classDynMetaDataType_Type;

    public void clear()
    {
        super.clear();
        classDynMetaDataType = null;
        classDynMetaDataTypeName = null;
        parent = null;
        parentType = null;
        value = null;
        classDynMetaDataType_Type = null;
    }

    public LDynMetaDataValue()
    {}

    public static LDynMetaDataValue getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Long classDynMetaDataType, Long parent, String parentType, Object value, String classDynMetaDataType_Type)
    {
        LDynMetaDataValue dynMetaDataValue = new LDynMetaDataValue();
        dynMetaDataValue.setId(id);
        dynMetaDataValue.setVersion(version);
        dynMetaDataValue.setTags(tags);
        dynMetaDataValue.setClassDynMetaDataType(classDynMetaDataType);
        dynMetaDataValue.setParent(parent);
        dynMetaDataValue.setParentType(parentType);
        dynMetaDataValue.setValue(value);
        dynMetaDataValue.setClassDynMetaDataType_Type(classDynMetaDataType_Type);

        return dynMetaDataValue;
    }

    public static LDynMetaDataValue getNewInstance(Long classDynMetaDataType, Long parent, String parentType,
            String value, String classDynMetaDataType_Type)
    {
        return getNewInstance(null, null, null, classDynMetaDataType, parent, parentType, value,
                classDynMetaDataType_Type);
    }

    public Long getClassDynMetaDataType()
    {
        return classDynMetaDataType;
    }

    public String getClassDynMetaDataTypeName()
    {
        return classDynMetaDataTypeName;
    }

    public Long getParent()
    {
        return parent;
    }

    public String getParentType()
    {
        return parentType;
    }

    public Object getValue()
    {
        return value;
    }

    public String getClassDynMetaDataType_Type()
    {
        return classDynMetaDataType_Type;
    }

    public void setClassDynMetaDataType_Type(String classDynMetaDataType_Type)
    {
        this.classDynMetaDataType_Type = classDynMetaDataType_Type;
    }

    public void setClassDynMetaDataTypeName(String classDynMetaDataTypeName)
    {
        this.classDynMetaDataTypeName = classDynMetaDataTypeName;
    }

    public void setClassDynMetaDataType(Long classDynMetaDataType)
    {
        this.classDynMetaDataType = classDynMetaDataType;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((classDynMetaDataType == null) ? 0 : classDynMetaDataType.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LDynMetaDataValue other = (LDynMetaDataValue) obj;
        if (classDynMetaDataType == null)
        {
            if (other.classDynMetaDataType != null)
                return false;
        }
        else if (!classDynMetaDataType.equals(other.classDynMetaDataType))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (parentType == null)
        {
            if (other.parentType != null)
                return false;
        }
        else if (!parentType.equals(other.parentType))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }

    public String toString()
    {
        StringBuilder buf = new StringBuilder(super.toString());
        buf.append(super.toString()).append(',');
        buf.append("classDynMetaDataTypeName=" + classDynMetaDataTypeName).append(',');
        buf.append("parent=" + parent).append(',');
        buf.append("parentType=" + parentType).append(',');
        buf.append("value=" + value).append(',');
        buf.append("classDynMetaDataType_Type=" + classDynMetaDataType_Type);
        return buf.toString();
    }
}
