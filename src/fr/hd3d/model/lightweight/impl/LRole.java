package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.ILRole;


/**
 * 
 * @author florent-della-valle
 * 
 */
public class LRole extends LBase implements ILRole
{

    private String name;
    private List<String> permissions;
    private List<String> bans;
    private LinkedHashSet<Long> resourceGroupIDs;
    private List<ILResourceGroup> resourceGroups;
    private List<String> resourceGroupNames;

    public void clear()
    {
        super.clear();

        name = null;

        if (permissions != null)
            permissions.clear();
        permissions = null;

        if (bans != null)
            bans.clear();
        bans = null;

        if (resourceGroupIDs != null)
            resourceGroupIDs.clear();
        resourceGroupIDs = null;

        if (resourceGroups != null)
        {
            for (ILResourceGroup x : resourceGroups)
                x.clear();
            resourceGroups.clear();
        }
        resourceGroups = null;

        if (resourceGroupNames != null)
            resourceGroupNames.clear();
        resourceGroupNames = null;
    }

    /**
     * Default CTor
     */
    public LRole()
    {}

    public static ILRole getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name)
    {
        final LRole role = new LRole();
        role.setId(id);
        role.setVersion(version);
        role.setTags(tags);
        role.setName(name);
        role.setPermissions(new ArrayList<String>());
        role.setBans(new ArrayList<String>());
        role.setResourceGroupIDs(new LinkedHashSet<Long>());
        role.setResourceGroupNames(new ArrayList<String>());
        return role;
    }

    public static ILRole getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }

    public List<String> getPermissions()
    {
        return permissions;
    }

    public void addPermissions(List<String> permissions)
    {
        if (getPermissions() == null)
            setPermissions(new ArrayList<String>());
        getPermissions().addAll(permissions);
    }

    public void addBans(List<String> bans)
    {
        if (getBans() == null)
            setBans(new ArrayList<String>());
        getBans().addAll(bans);
    }

    public void setPermissions(List<String> permissions)
    {
        this.permissions = permissions;
    }

    public List<String> getBans()
    {
        return bans;
    }

    public void setBans(List<String> bans)
    {
        this.bans = bans;
    }

    public List<ILResourceGroup> getResourceGroups()
    {
        return resourceGroups;
    }

    public LinkedHashSet<Long> getResourceGroupIDs()
    {
        return resourceGroupIDs;
    }

    public void addResourceGroupID(Long id)
    {
        if (getResourceGroupIDs() == null)
            setResourceGroupIDs(new LinkedHashSet<Long>());
        getResourceGroupIDs().add(id);
    }

    public void addResourceGroupName(String name)
    {
        if (getResourceGroupNames() == null)
            setResourceGroupNames(new ArrayList<String>());
        getResourceGroupNames().add(name);
    }

    public void setResourceGroupIDs(LinkedHashSet<Long> resourceGroupsIDs)
    {
        this.resourceGroupIDs = resourceGroupsIDs;
    }

    public List<String> getResourceGroupNames()
    {
        return resourceGroupNames;
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.resourceGroupNames = resourceGroupNames;
    }

    public void setResourceGroups(List<ILResourceGroup> resourceGroups)
    {
        this.resourceGroups = resourceGroups;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bans == null) ? 0 : bans.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((permissions == null) ? 0 : permissions.hashCode());
        result = prime * result + ((resourceGroupIDs == null) ? 0 : resourceGroupIDs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LRole other = (LRole) obj;
        if (bans == null)
        {
            if (other.bans != null)
                return false;
        }
        else if (!bans.equals(other.bans))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (permissions == null)
        {
            if (other.permissions != null)
                return false;
        }
        else if (!permissions.equals(other.permissions))
            return false;
        if (resourceGroupIDs == null)
        {
            if (other.resourceGroupIDs != null)
                return false;
        }
        else if (!resourceGroupIDs.equals(other.resourceGroupIDs))
            return false;
        return true;
    }

}
