/**
 * 
 */
package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.lightweight.ILTaskGroup;


/**
 * @author thomas-eskenazi
 * 
 */
public class LExtraLine extends LBase implements ILExtraLine
{
    private ILTaskGroup taskGroup;
    private Long taskGroupID;
    private List<ILTaskBase> taskBases;
    private List<Long> taskBaseIDs;
    private String name;
    private Long hiddenPersonID;
    private ILPerson hiddenPerson;

    public void clear()
    {
        super.clear();

        if (taskGroup != null)
            taskGroup.clear();
        taskGroup = null;

        taskGroupID = null;

        if (taskBases != null)
        {
            for (ILTaskBase x : taskBases)
                x.clear();
            taskBases.clear();
        }
        taskBases = null;

        if (taskBaseIDs != null)
        {

            taskBaseIDs.clear();
        }
        taskBaseIDs = null;

        name = null;
        hiddenPersonID = null;

        if (hiddenPerson != null)
            hiddenPerson.clear();
        hiddenPerson = null;
    }

    /**
     * @return the taskGroup
     */
    public ILTaskGroup getTaskGroup()
    {
        return taskGroup;
    }

    /**
     * @param taskGroup
     *            the taskGroup to set
     */
    public void setTaskGroup(ILTaskGroup taskGroup)
    {
        this.taskGroup = taskGroup;
    }

    /**
     * @return the taskGroupiD
     */
    public Long getTaskGroupID()
    {
        return taskGroupID;
    }

    /**
     * @param taskGroupiD
     *            the taskGroupiD to set
     */
    public void setTaskGroupID(Long taskGroupID)
    {
        this.taskGroupID = taskGroupID;
    }

    /**
     * @return the taskBase
     */
    public List<ILTaskBase> getTaskBases()
    {
        return taskBases;
    }

    /**
     * @param taskBase
     *            the taskBase to set
     */
    public void setTaskBases(List<ILTaskBase> taskBases)
    {
        if (this.taskBases == null)
        {
            this.taskBases = new ArrayList<ILTaskBase>();
        }
        this.taskBases.clear();
        this.taskBases.addAll(taskBases);
    }

    /**
     * @return the taskBaseID
     */
    public List<Long> getTaskBaseIDs()
    {
        if (taskBaseIDs == null)
        {
            taskBaseIDs = new ArrayList<Long>();
        }
        return taskBaseIDs;
    }

    /**
     * @param taskBaseID
     *            the taskBaseID to set
     */
    public void setTaskBaseIDs(List<Long> taskBaseIDs)
    {
        if (this.taskBaseIDs == null)
        {
            this.taskBaseIDs = new ArrayList<Long>();
        }
        this.taskBaseIDs.clear();
        this.taskBaseIDs.addAll(taskBaseIDs);
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the hiddenPersonID
     */
    public Long getHiddenPersonID()
    {
        return hiddenPersonID;
    }

    /**
     * @param hiddenPersonID
     *            the hiddenPersonID to set
     */
    public void setHiddenPersonID(Long hiddenPersonID)
    {
        this.hiddenPersonID = hiddenPersonID;
    }

    /**
     * @return the hiddenPerson
     */
    public ILPerson getHiddenPerson()
    {
        return hiddenPerson;
    }

    /**
     * @param hiddenPerson
     *            the hiddenPerson to set
     */
    public void setHiddenPerson(ILPerson hiddenPerson)
    {
        this.hiddenPerson = hiddenPerson;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LExtraLine other = (LExtraLine) obj;
        if (hiddenPerson == null)
        {
            if (other.hiddenPerson != null)
                return false;
        }
        else if (!hiddenPerson.equals(other.hiddenPerson))
            return false;
        if (hiddenPersonID == null)
        {
            if (other.hiddenPersonID != null)
                return false;
        }
        else if (!hiddenPersonID.equals(other.hiddenPersonID))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (taskBaseIDs == null)
        {
            if (other.taskBaseIDs != null)
                return false;
        }
        else if (!taskBaseIDs.equals(other.taskBaseIDs))
            return false;
        if (taskBases == null)
        {
            if (other.taskBases != null)
                return false;
        }
        else if (!taskBases.equals(other.taskBases))
            return false;
        if (taskGroup == null)
        {
            if (other.taskGroup != null)
                return false;
        }
        else if (!taskGroup.equals(other.taskGroup))
            return false;
        if (taskGroupID == null)
        {
            if (other.taskGroupID != null)
                return false;
        }
        else if (!taskGroupID.equals(other.taskGroupID))
            return false;
        return true;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hiddenPerson == null) ? 0 : hiddenPerson.hashCode());
        result = prime * result + ((hiddenPersonID == null) ? 0 : hiddenPersonID.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((taskBaseIDs == null) ? 0 : taskBaseIDs.hashCode());
        result = prime * result + ((taskBases == null) ? 0 : taskBases.hashCode());
        result = prime * result + ((taskGroup == null) ? 0 : taskGroup.hashCode());
        result = prime * result + ((taskGroupID == null) ? 0 : taskGroupID.hashCode());
        return result;
    }

}
