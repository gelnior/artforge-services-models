package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILComposition;


/**
 * @author Try LAM
 */
public class LComposition extends LBase implements ILComposition
{

    private String name;
    private String description;
    private Long constituent;
    private String constituentName;
    private Long shot;
    private Long sequence;
    private Integer nbOccurence = 1;
    private Long categoryId;

    public void clear()
    {
        super.clear();

        name = null;
        description = null;
        constituent = null;
        constituentName = null;
        shot = null;
        sequence = null;
        nbOccurence = null;
        categoryId = null;
    }

    public LComposition()
    {
        super();
    }

    public static ILComposition getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description, Long constituent, String constituentName, Long shot, Long sequence,
            Integer nbOccurence, Long categoryId)
    {
        final ILComposition composition = new LComposition();
        composition.setId(id);
        composition.setVersion(version);
        composition.setTags(tags);
        composition.setName(name);
        composition.setDescription(description);
        composition.setConstituent(constituent);
        composition.setConstituentName(constituentName);
        composition.setShot(shot);
        composition.setSequence(sequence);
        // composition.setInput(input);
        // composition.setOutput(output);
        composition.setNbOccurence(nbOccurence);
        composition.setCategoryId(categoryId);
        return composition;
    }

    public static ILComposition getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description, Long constituent, String constituentName, Long shot, Long sequence, Long categoryId)
    {
        return getNewInstance(null, null, null, name, description, constituent, constituentName, shot, sequence, 1,
                categoryId);
    }

    public static ILComposition getNewInstance(String name, String description, Long constituent,
            String constituentName, Long shot, Long sequence, Integer nbOccurence, Long categoryId)
    {
        return getNewInstance(null, null, null, name, description, constituent, constituentName, shot, sequence,
                nbOccurence, categoryId);
    }

    public static ILComposition getNewInstance(String name, String description, Long constituent,
            String constituentName, Long shot, Long sequence, Long categoryId)
    {
        return getNewInstance(null, null, null, name, description, constituent, constituentName, shot, sequence, 1,
                categoryId);
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getConstituent()
    {
        return this.constituent;
    }

    public void setConstituentName(String constituentName)
    {
        this.constituentName = constituentName;
    }

    public String getConstituentName()
    {
        return this.constituentName;
    }

    public Long getShot()
    {
        return shot;
    }

    public void setShot(Long shot)
    {
        this.shot = shot;
    }

    public Long getSequence()
    {
        return sequence;
    }

    public void setSequence(Long sequence)
    {
        this.sequence = sequence;
    }

    public Integer getNbOccurence()
    {
        return nbOccurence;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public void setConstituent(Long constituent)
    {
        this.constituent = constituent;
    }

    public void setNbOccurence(Integer nbOccurence)
    {
        this.nbOccurence = nbOccurence;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((constituent == null) ? 0 : constituent.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nbOccurence == null) ? 0 : nbOccurence.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + ((shot == null) ? 0 : shot.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LComposition other = (LComposition) obj;
        if (constituent == null)
        {
            if (other.constituent != null)
                return false;
        }
        else if (!constituent.equals(other.constituent))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (nbOccurence == null)
        {
            if (other.nbOccurence != null)
                return false;
        }
        else if (!nbOccurence.equals(other.nbOccurence))
            return false;
        if (sequence == null)
        {
            if (other.sequence != null)
                return false;
        }
        else if (!sequence.equals(other.sequence))
            return false;
        if (shot == null)
        {
            if (other.shot != null)
                return false;
        }
        else if (!shot.equals(other.shot))
            return false;
        return true;
    }

}
