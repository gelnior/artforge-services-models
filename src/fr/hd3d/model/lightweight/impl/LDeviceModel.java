package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILDeviceModel;


/**
 * Lightweight implementation for device model object.
 * 
 * @author HD3D
 */
public class LDeviceModel extends LModel implements ILDeviceModel
{
    /**
     * Default Constructor.
     */
    public LDeviceModel()
    {}

    public LDeviceModel(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILDeviceModel getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LDeviceModel deviceModel = new LDeviceModel(id, version, tags, name);

        return deviceModel;
    }

    public static ILDeviceModel getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
