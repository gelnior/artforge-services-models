package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILResourceGroup;


/**
 * Lightweight implementation for resource group object.
 * 
 * @author HD3D
 */
public class LResourceGroup extends LSimple implements ILResourceGroup
{
    private Long parent;

    private Long leaderId;
    private String leaderName;

    private List<Long> resourceIDs;
    private List<String> resourceNames;
    private List<Long> organizationIDs;
    private List<String> organizationNames;
    private List<Long> projectIDs;
    private List<String> projectNames;
    private List<Long> roleIDs;
    private List<String> roleNames;

    public void clear()
    {
        super.clear();

        parent = null;
        leaderId = null;
        leaderName = null;

        if (resourceIDs != null)
            resourceIDs.clear();
        resourceIDs = null;

        if (resourceNames != null)
            resourceNames.clear();
        resourceNames = null;

        if (organizationIDs != null)
            organizationIDs.clear();
        organizationIDs = null;

        if (organizationNames != null)
            organizationNames.clear();
        organizationNames = null;

        if (projectIDs != null)
            projectIDs.clear();
        projectIDs = null;

        if (projectNames != null)
            projectNames.clear();
        projectNames = null;

        if (roleIDs != null)
            roleIDs.clear();
        roleIDs = null;

        if (roleNames != null)
            roleNames.clear();
        roleNames = null;
    }

    public LResourceGroup()
    {
        super();
    }

    public LResourceGroup(Long id, Timestamp version, List<Long> tags, String name, Long parent)
    {
        super(id, version, tags, name);

        this.parent = parent;
    }

    public static ILResourceGroup getNewInstance(Long id, Timestamp version, List<Long> tags, String name, Long parent)
    {
        final LResourceGroup group = new LResourceGroup(id, version, tags, name, parent);

        return group;
    }

    public static ILResourceGroup getNewInstance(String name, Long parent)
    {
        return getNewInstance(null, null, null, name, parent);
    }

    public Long getParent()
    {
        return parent;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public Long getLeaderId()
    {
        return leaderId;
    }

    public void setLeaderId(Long leaderId)
    {
        this.leaderId = leaderId;
    }

    public String getLeaderName()
    {
        return leaderName;
    }

    public void setLeaderName(String leaderName)
    {
        this.leaderName = leaderName;
    }

    public List<Long> getResourceIDs()
    {
        return this.resourceIDs;
    }

    public void addResourceID(Long id)
    {
        if (getResourceIDs() == null)
            setResourceIDs(new ArrayList<Long>());
        getResourceIDs().add(id);
    }

    public void addResourceName(String name)
    {
        if (getResourceNames() == null)
            setResourceNames(new ArrayList<String>());
        getResourceNames().add(name);
    }

    public void setResourceIDs(List<Long> resourceIDs)
    {
        this.resourceIDs = resourceIDs;
    }

    public List<String> getResourceNames()
    {
        return this.resourceNames;
    }

    public void setResourceNames(List<String> resourceNames)
    {
        this.resourceNames = resourceNames;
    }

    public List<Long> getProjectIDs()
    {
        return projectIDs;
    }

    public void addProjectID(Long id)
    {
        if (getProjectIDs() == null)
            setProjectIDs(new ArrayList<Long>());
        getProjectIDs().add(id);
    }

    public void addProjectName(String name)
    {
        if (getProjectNames() == null)
            setProjectNames(new ArrayList<String>());
        getProjectNames().add(name);
    }

    public void setProjectIDs(List<Long> projectIDs)
    {
        this.projectIDs = projectIDs;
    }

    public List<String> getProjectNames()
    {
        return projectNames;
    }

    public void setProjectNames(List<String> projectNames)
    {
        this.projectNames = projectNames;
    }

    public List<Long> getOrganizationIDs()
    {
        return organizationIDs;
    }

    public void addOrganizationID(Long id)
    {
        if (getOrganizationIDs() == null)
            setOrganizationIDs(new ArrayList<Long>());
        getOrganizationIDs().add(id);
    }

    public void addOrganizationName(String name)
    {
        if (getOrganizationNames() == null)
            setOrganizationNames(new ArrayList<String>());
        getOrganizationNames().add(name);
    }

    public void setOrganizationIDs(List<Long> organizationIDs)
    {
        this.organizationIDs = organizationIDs;
    }

    public List<String> getOrganizationNames()
    {
        return organizationNames;
    }

    public void setOrganizationNames(List<String> organizationNames)
    {
        this.organizationNames = organizationNames;
    }

    public List<Long> getRoleIDs()
    {
        return roleIDs;
    }

    public void addRoleID(Long id)
    {
        if (getRoleIDs() == null)
            setRoleIDs(new ArrayList<Long>());
        getRoleIDs().add(id);
    }

    public void addRoleName(String name)
    {
        if (getRoleNames() == null)
            setRoleNames(new ArrayList<String>());
        getRoleNames().add(name);
    }

    public void setRoleIDs(List<Long> rolesIds)
    {
        this.roleIDs = rolesIds;
    }

    public List<String> getRoleNames()
    {
        return roleNames;
    }

    public void setRoleNames(List<String> rolesNames)
    {
        this.roleNames = rolesNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((leaderId == null) ? 0 : leaderId.hashCode());
        result = prime * result + ((leaderName == null) ? 0 : leaderName.hashCode());
        result = prime * result + ((organizationIDs == null) ? 0 : organizationIDs.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((projectIDs == null) ? 0 : projectIDs.hashCode());
        result = prime * result + ((resourceIDs == null) ? 0 : resourceIDs.hashCode());
        result = prime * result + ((roleIDs == null) ? 0 : roleIDs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LResourceGroup other = (LResourceGroup) obj;
        if (leaderId == null)
        {
            if (other.leaderId != null)
                return false;
        }
        else if (!leaderId.equals(other.leaderId))
            return false;
        if (leaderName == null)
        {
            if (other.leaderName != null)
                return false;
        }
        else if (!leaderName.equals(other.leaderName))
            return false;
        if (organizationIDs == null)
        {
            if (other.organizationIDs != null)
                return false;
        }
        else if (!organizationIDs.equals(other.organizationIDs))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (projectIDs == null)
        {
            if (other.projectIDs != null)
                return false;
        }
        else if (!projectIDs.equals(other.projectIDs))
            return false;
        if (resourceIDs == null)
        {
            if (other.resourceIDs != null)
                return false;
        }
        else if (!resourceIDs.equals(other.resourceIDs))
            return false;
        if (roleIDs == null)
        {
            if (other.roleIDs != null)
                return false;
        }
        else if (!roleIDs.equals(other.roleIDs))
            return false;
        return true;
    }

}
