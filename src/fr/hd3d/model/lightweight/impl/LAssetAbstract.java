package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.Set;

import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.lightweight.ILAssetRevision;


public class LAssetAbstract extends LBase implements ILAssetAbstract
{

    private String key;
    private String variation;
    private String name;
    private Long taskType;
    private String taskTypeName;
    private Date creationDate;
    private Long creatorId;
    private Integer lastRevision;
    private Set<ILAssetRevision> revisionObjects;
    private Set<Integer> revisions;
    private Long projectId;

    private Long id = -1L;

    public void clear()
    {
        super.clear();
        key = null;
        variation = null;
        name = null;
        taskType = null;
        creationDate = null;
        creatorId = null;
        lastRevision = null;

        if (revisionObjects != null)
        {
            for (ILAssetRevision assetrev : revisionObjects)
                assetrev.clear();
            revisionObjects.clear();
        }
        revisionObjects = null;

        if (revisions != null)
            revisions.clear();

        projectId = null;
    }

    public LAssetAbstract(Long projectId, Long id, String key, String variation, String name, Long type,
            String typeName, Date creationDate, Long creatorId, Integer lastRevision, Set<Integer> revisions)
    {
        this.projectId = projectId;
        this.id = id;
        this.key = key;
        this.variation = variation;
        this.name = name;
        this.taskType = type;
        this.taskTypeName = typeName;
        this.creationDate = creationDate;
        this.creatorId = creatorId;
        this.lastRevision = lastRevision;
        this.revisions = revisions;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public Long getCreatorId()
    {
        return creatorId;
    }

    public String getKey()
    {
        return key;
    }

    public Long getId()
    {
        return id;
    }

    public Integer getLastRevision()
    {
        return lastRevision;
    }

    public String getName()
    {
        return name;
    }

    public Long getProjectId()
    {
        return this.projectId;
    }

    public Set<ILAssetRevision> getRevisionObjects()
    {
        return revisionObjects;
    }

    public Set<Integer> getRevisions()
    {
        return revisions;
    }

    public Long getTaskType()
    {
        return taskType;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public String getVariation()
    {
        return variation;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setCreatorId(Long creatorId)
    {
        this.creatorId = creatorId;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setLastRevision(Integer lastRevision)
    {
        this.lastRevision = lastRevision;

    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public void setRevisions(Set<Integer> revisions)
    {
        this.revisions = revisions;
    }

    public void setTaskType(Long type)
    {
        this.taskType = type;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    @Override
    public int hashCode()
    {
        final int prime = 32;
        int result = super.hashCode();

        result = prime * result + (key == null ? 0 : key.hashCode());
        result = prime * result + (variation == null ? 0 : variation.hashCode());
        result = prime * result + (projectId == null ? 0 : projectId.hashCode());

        return result;
    }
}
