package fr.hd3d.model.lightweight.impl;

import fr.hd3d.model.lightweight.ILProxyType;


/**
 * 
 * @author HD3D
 * 
 */
public class LProxyType extends LBase implements ILProxyType
{
    private String name;
    private String openType;
    private String description;

    public static ILProxyType getNewInstance(Long id, java.sql.Timestamp version, String name, String openType,
            String description)
    {
        final ILProxyType proxyType = new LProxyType();
        proxyType.setId(id);
        proxyType.setVersion(version);

        proxyType.setName(name);
        proxyType.setOpenType(openType);
        proxyType.setDescription(description);
        return proxyType;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getOpenType()
    {
        return openType;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setOpenType(String openType)
    {
        this.openType = openType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((openType == null) ? 0 : openType.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LProxyType other = (LProxyType) obj;

        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;

        if (openType == null)
        {
            if (other.openType != null)
                return false;
        }
        else if (!openType.equals(other.openType))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        return true;
    }

}
