package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;


public class LFileRevisionAnnotation extends LBase implements ILFileRevisionAnnotation
{

    private Integer markIn;
    private Integer markOut;
    private String name;
    private String comment;
    private Long graphicData;
    private Long annotatedFile;
    private Long annotator;

    /*------------------
     * clear
     ------------------*/
    public void clear()
    {
        super.clear();
        markIn = null;
        markOut = null;
        name = null;
        comment = null;
        graphicData = null;
        annotatedFile = null;
        annotator = null;
    }

    /*-------------
     * Factory
     -------------*/
    public LFileRevisionAnnotation()
    {}

    public static LFileRevisionAnnotation getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Integer markIn, Integer markOut, String name, String comment, Long graphicData, Long annotatedFile,
            Long annotator)
    {
        LFileRevisionAnnotation fileRevisionAnnotation = new LFileRevisionAnnotation();
        fileRevisionAnnotation.setId(id);
        fileRevisionAnnotation.setVersion(version);
        fileRevisionAnnotation.setTags(tags);
        fileRevisionAnnotation.setMarkIn(markIn);
        fileRevisionAnnotation.setMarkOut(markOut);
        fileRevisionAnnotation.setName(name);
        fileRevisionAnnotation.setComment(comment);
        fileRevisionAnnotation.setGraphicData(graphicData);
        fileRevisionAnnotation.setAnnotatedFile(annotatedFile);
        fileRevisionAnnotation.setAnnotator(annotator);

        return fileRevisionAnnotation;
    }

    public static LFileRevisionAnnotation getNewInstance(Integer markIn, Integer markOut, String name, String comment,
            Long graphicData, Long annotatedFile, Long annotator)
    {
        return getNewInstance(null, null, null, markIn, markOut, name, comment, graphicData, annotatedFile, annotator);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    public Integer getMarkIn()
    {
        return markIn;
    }

    public Integer getMarkOut()
    {
        return markOut;
    }

    public String getName()
    {
        return name;
    }

    public String getComment()
    {
        return comment;
    }

    public Long getGraphicData()
    {
        return graphicData;
    }

    public Long getAnnotatedFile()
    {
        return annotatedFile;
    }

    public Long getAnnotator()
    {
        return annotator;
    }

    public void setMarkIn(Integer markIn)
    {
        this.markIn = markIn;
    }

    public void setMarkOut(Integer markOut)
    {
        this.markOut = markOut;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setGraphicData(Long graphicData)
    {
        this.graphicData = graphicData;
    }

    public void setAnnotatedFile(Long annotatedFile)
    {
        this.annotatedFile = annotatedFile;
    }

    public void setAnnotator(Long annotator)
    {
        this.annotator = annotator;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((annotatedFile == null) ? 0 : annotatedFile.hashCode());
        result = prime * result + ((annotator == null) ? 0 : annotator.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((graphicData == null) ? 0 : graphicData.hashCode());
        result = prime * result + ((markIn == null) ? 0 : markIn.hashCode());
        result = prime * result + ((markOut == null) ? 0 : markOut.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFileRevisionAnnotation other = (LFileRevisionAnnotation) obj;
        if (annotatedFile == null)
        {
            if (other.annotatedFile != null)
                return false;
        }
        else if (!annotatedFile.equals(other.annotatedFile))
            return false;
        if (annotator == null)
        {
            if (other.annotator != null)
                return false;
        }
        else if (!annotator.equals(other.annotator))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (graphicData == null)
        {
            if (other.graphicData != null)
                return false;
        }
        else if (!graphicData.equals(other.graphicData))
            return false;
        if (markIn == null)
        {
            if (other.markIn != null)
                return false;
        }
        else if (!markIn.equals(other.markIn))
            return false;
        if (markOut == null)
        {
            if (other.markOut != null)
                return false;
        }
        else if (!markOut.equals(other.markOut))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

}
