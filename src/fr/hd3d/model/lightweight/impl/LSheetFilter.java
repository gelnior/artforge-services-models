package fr.hd3d.model.lightweight.impl;

public class LSheetFilter implements ILPersistent
{
    private Long id;
    protected java.sql.Timestamp version;
    private Long sheetId;
    private String filter;
    private String name;

    public void clear()
    {
        id = null;
        sheetId = null;
        version = null;
        filter = null;
        name = null;
    }

    public LSheetFilter()
    {}

    public LSheetFilter(Long id, java.sql.Timestamp version, Long sheetId, String filter, String name)
    {
        super();
        this.id = id;
        this.version = version;
        this.sheetId = sheetId;
        this.filter = filter;
        this.name = name;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    public Long getSheetId()
    {
        return sheetId;
    }

    public String getName()
    {
        return name;
    }

    public void setSheetId(Long sheetId)
    {
        this.sheetId = sheetId;
    }

    public String getFilter()
    {
        return filter;
    }

    public void setFilter(String filter)
    {
        this.filter = filter;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((filter == null) ? 0 : filter.hashCode());
        result = prime * result + ((sheetId == null) ? 0 : sheetId.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSheetFilter other = (LSheetFilter) obj;
        if (filter == null)
        {
            if (other.filter != null)
                return false;
        }
        else if (!filter.equals(other.filter))
            return false;
        if (sheetId == null)
        {
            if (other.sheetId != null)
                return false;
        }
        else if (!sheetId.equals(other.sheetId))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }
}
