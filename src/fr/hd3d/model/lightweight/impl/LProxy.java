package fr.hd3d.model.lightweight.impl;

import fr.hd3d.model.lightweight.ILProxy;


/**
 * 
 * @author HD3D
 * 
 */
public class LProxy extends LBase implements ILProxy
{

    private Long fileRevisionId;
    private Long proxyTypeId;
    private String proxyTypeName;
    private String location;
    private String description;
    private String mountPoint;
    private String filename;
    private String proxyTypeOpenType;
    private Integer framerate;

    public static ILProxy getNewInstance(Long id, java.sql.Timestamp version, Long fileRevisionId, Long proxyTypeId,
            String location, String filename, String description, Integer framerate)
    {
        final ILProxy proxy = new LProxy();
        proxy.setId(id);
        proxy.setVersion(version);

        proxy.setFileRevisionId(fileRevisionId);
        proxy.setProxyTypeId(proxyTypeId);
        proxy.setLocation(location);
        proxy.setDescription(description);
        proxy.setFilename(filename);
        proxy.setFramerate(framerate);
        return proxy;
    }

    public Long getFileRevisionId()
    {
        return fileRevisionId;
    }

    public String getLocation()
    {
        return location;
    }

    public Long getProxyTypeId()
    {
        return proxyTypeId;
    }

    public String getProxyTypeName()
    {
        return proxyTypeName;
    }

    public void setFileRevisionId(Long fileRevisionId)
    {
        this.fileRevisionId = fileRevisionId;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setProxyTypeName(String proxyTypeName)
    {
        this.proxyTypeName = proxyTypeName;
    }

    public void setProxyTypeId(Long proxyTypeId)
    {
        this.proxyTypeId = proxyTypeId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getMountPoint()
    {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint)
    {
        this.mountPoint = mountPoint;
    }

    public String getFilename()
    {
        return this.filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((fileRevisionId == null) ? 0 : fileRevisionId.hashCode());
        result = prime * result + ((proxyTypeId == null) ? 0 : proxyTypeId.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((mountPoint == null) ? 0 : mountPoint.hashCode());
        result = prime * result + ((filename == null) ? 0 : filename.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LProxy other = (LProxy) obj;

        if (fileRevisionId == null)
        {
            if (other.fileRevisionId != null)
                return false;
        }
        else if (!fileRevisionId.equals(other.fileRevisionId))
            return false;

        if (proxyTypeId == null)
        {
            if (other.proxyTypeId != null)
                return false;
        }
        else if (!proxyTypeId.equals(other.proxyTypeId))
            return false;

        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;

        if (mountPoint == null)
        {
            if (other.mountPoint != null)
                return false;
        }
        else if (!mountPoint.equals(other.mountPoint))
            return false;
        if (filename == null)
        {
            if (other.filename != null)
                return false;
        }
        else if (!filename.equals(other.filename))
            return false;
        return true;
    }

    public String getProxyTypeOpenType()
    {
        return this.proxyTypeOpenType;
    }

    public void setProxyTypeOpenType(String opentType)
    {
        this.proxyTypeOpenType = opentType;
    }

    public Integer getFramerate()
    {
        return framerate;
    }

    public void setFramerate(Integer framerate)
    {
        this.framerate = framerate;
    }

}
