package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskActivity;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public class LTaskActivity extends LActivity implements ILTaskActivity
{

    private Long taskID;
    private ILTask task;
    private String taskName;
    private Long taskTypeId;
    private Long taskDuration;
    private String taskTypeName;
    private String taskTypeColor;
    private Long workObjectId;
    private String workObjectName;
    private String workObjectParentsNames;// for Constituents and Shots
    private String taskStatus;
    private String workObjectEntity;

    public void clear()
    {
        super.clear();
        taskID = null;
        if (task != null)
            task.clear();
        task = null;
        taskName = null;
        workObjectParentsNames = null;
    }

    /**
     * Default CTor
     */
    public LTaskActivity()
    {}

    public static LTaskActivity getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long dayId,
            Date dayDate, Long duration, String comment, ILTask task_, Long workerId, Long projectId, Long filledByID,
            Date filledDate)
    {
        final LTaskActivity activity = new LTaskActivity();
        activity.setId(id);
        activity.setVersion(version);
        activity.setTags(tags);
        activity.setDayID(dayId);
        activity.setDayDate(dayDate);
        activity.setDuration(duration);
        activity.setComment(comment);
        activity.setTaskID(task_.getId());
        activity.setWorkerID(workerId);
        activity.setProjectID(projectId);
        activity.setTaskName(task_.getName());
        activity.setTaskTypeId(task_.getTaskTypeID());
        activity.setTaskTypeName(task_.getTaskTypeName());
        activity.setTaskDuration(task_.getDuration());
        activity.setTaskTypeColor(task_.getColor());

        activity.setFilledByID(filledByID);
        activity.setFilledDate(filledDate);
        return activity;
    }

    public static LTaskActivity getNewInstance(Long dayId, Date dayDate, Long duration, String comment, ILTask task_,
            Long workerId, Long projectId, Long filledByID, Date filledDate)
    {
        return getNewInstance(null, null, null, dayId, dayDate, duration, comment, task_, workerId, projectId,
                filledByID, filledDate);
    }

    /**
     * @return the task
     */
    public final ILTask getTask()
    {
        return task;
    }

    /**
     * @param task
     *            the task to set
     */
    public final void setTask(ILTask task)
    {
        this.task = task;
    }

    public final Long getTaskID()
    {
        return taskID;
    }

    public final void setTaskID(Long taskID)
    {
        this.taskID = taskID;
    }

    @Override
    public String getTaskName()
    {
        return taskName;
    }

    public void setTaskName(String taskName)
    {
        this.taskName = taskName;
    }

    public Long getTaskTypeId()
    {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId)
    {
        this.taskTypeId = taskTypeId;
    }

    public Long getTaskDuration()
    {
        return taskDuration;
    }

    public void setTaskDuration(Long taskDuration)
    {
        this.taskDuration = taskDuration;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public String getTaskTypeColor()
    {
        return taskTypeColor;
    }

    public void setTaskTypeColor(String taskTypeColor)
    {
        this.taskTypeColor = taskTypeColor;
    }

    public String getWorkObjectName()
    {
        return workObjectName;
    }

    public void setWorkObjectName(String workObjectName)
    {
        this.workObjectName = workObjectName;
    }

    public String getWorkObjectParentsNames()
    {
        return workObjectParentsNames;
    }

    public void setWorkObjectParentsNames(String parentsNames)
    {
        this.workObjectParentsNames = parentsNames;
    }

    public String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public Long getWorkObjectId()
    {
        return workObjectId;
    }

    public void setWorkObjectId(Long workObjectId)
    {
        this.workObjectId = workObjectId;
    }

    public String getWorkObjectEntity()
    {
        return workObjectEntity;
    }

    public void setWorkObjectEntity(String entity)
    {
        this.workObjectEntity = entity;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((taskID == null) ? 0 : taskID.hashCode());
        result = prime * result + ((taskName == null) ? 0 : taskName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTaskActivity other = (LTaskActivity) obj;
        if (taskID == null)
        {
            if (other.taskID != null)
                return false;
        }
        else if (!taskID.equals(other.taskID))
            return false;
        if (taskName == null)
        {
            if (other.taskName != null)
                return false;
        }
        else if (!taskName.equals(other.taskName))
            return false;
        return true;
    }

}
