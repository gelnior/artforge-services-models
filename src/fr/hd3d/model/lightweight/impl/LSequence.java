package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILSequence;


/**
 * @author Try LAM
 */
public class LSequence extends LSimple implements ILSequence
{

    private Long parent;// only sequencId needed
    private Long project;// only projectId needed
    private String title;
    private String hook;

    public void clear()
    {
        super.clear();
        parent = null;
        project = null;
        title = null;
        hook = null;
    }

    public LSequence()
    {
        super();
    }

    public static ILSequence getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String title, Long parent, Long project, String hook)
    {
        final LSequence sequence = new LSequence();
        sequence.setId(id);
        sequence.setVersion(version);
        sequence.setTags(tags);
        sequence.setName(name);
        sequence.setTitle(title);
        sequence.setParent(parent);
        sequence.setProject(project);
        sequence.setHook(hook);
        return sequence;
    }

    public static ILSequence getNewInstance(String name, String title, Long parent, Long project, String hook)
    {
        return getNewInstance(null, null, null, name, title, parent, project, hook);
    }

    public Long getParent()
    {
        return parent;
    }

    public Long getProject()
    {
        return project;
    }

    public String getHook()
    {
        return hook;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSequence other = (LSequence) obj;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        return true;
    }
}
