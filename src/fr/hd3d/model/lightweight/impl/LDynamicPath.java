package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILDynamicPath;


/**
 * 
 * @author HD3D
 * 
 */
public class LDynamicPath extends LBase implements ILDynamicPath
{
    private String name;
    private Long taskTypeId;
    private String taskTypeName;
    private Long workObjectId;
    private String workObjectName;
    private String boundEntityName;
    private String wipDynamicPath;
    private String publishDynamicPath;
    private String oldDynamicPath;
    private Long projectId;
    private String taskTypeColor;
    private Long parent;
    private String type;

    public static ILDynamicPath getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String type, Long workObjectId, String boundEntityName, Long taskTypeId, String wipDynamicPath,
            String publishDynamicPath, String oldDynamicPath, Long projectId, Long parent)
    {
        final LDynamicPath dynamicPath = new LDynamicPath();
        dynamicPath.setId(id);
        dynamicPath.setVersion(version);
        dynamicPath.setTags(tags);
        dynamicPath.setName(name);
        dynamicPath.setWorkObjectId(workObjectId);
        dynamicPath.setBoundEntityName(boundEntityName);
        dynamicPath.setTaskTypeId(taskTypeId);
        dynamicPath.setWipDynamicPath(wipDynamicPath);
        dynamicPath.setPublishDynamicPath(publishDynamicPath);
        dynamicPath.setOldDynamicPath(oldDynamicPath);
        dynamicPath.setProjectId(projectId);
        dynamicPath.setParent(parent);
        dynamicPath.setType(type);
        return dynamicPath;
    }

    public String getName()
    {
        return name;
    }

    public Long getProjectId()
    {
        return projectId;
    }

    public Long getWorkObjectId()
    {
        return workObjectId;
    }

    public String getWorkObjectName()
    {
        return workObjectName;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public Long getTaskTypeId()
    {
        return taskTypeId;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public String getTaskTypeColor()
    {
        return taskTypeColor;
    }

    public String getWipDynamicPath()
    {
        return wipDynamicPath;
    }

    public String getPublishDynamicPath()
    {
        return publishDynamicPath;
    }

    public String getOldDynamicPath()
    {
        return oldDynamicPath;
    }

    public Long getParent()
    {
        return parent;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setProjectId(final Long projectId)
    {
        this.projectId = projectId;
    }

    public void setWorkObjectId(final Long workObjectId)
    {
        this.workObjectId = workObjectId;
    }

    public void setWorkObjectName(final String workObjectName)
    {
        this.workObjectName = workObjectName;
    }

    public void setBoundEntityName(final String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTaskTypeId(final Long taskTypeId)
    {
        this.taskTypeId = taskTypeId;
    }

    public void setTaskTypeName(final String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public void setTaskTypeColor(String color)
    {
        this.taskTypeColor = color;
    }

    public void setWipDynamicPath(final String wipDynamicPath)
    {
        this.wipDynamicPath = wipDynamicPath;
    }

    public void setPublishDynamicPath(final String publishDynamicPath)
    {
        this.publishDynamicPath = publishDynamicPath;
    }

    public void setOldDynamicPath(final String oldDynamicPath)
    {
        this.oldDynamicPath = oldDynamicPath;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((workObjectId == null) ? 0 : workObjectId.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((taskTypeId == null) ? 0 : taskTypeId.hashCode());
        result = prime * result + ((wipDynamicPath == null) ? 0 : wipDynamicPath.hashCode());
        result = prime * result + ((publishDynamicPath == null) ? 0 : publishDynamicPath.hashCode());
        result = prime * result + ((oldDynamicPath == null) ? 0 : oldDynamicPath.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LDynamicPath other = (LDynamicPath) obj;

        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (workObjectId == null)
        {
            if (other.workObjectId != null)
                return false;
        }
        else if (!workObjectId.equals(other.workObjectId))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (taskTypeId == null)
        {
            if (other.taskTypeId != null)
                return false;
        }
        else if (!taskTypeId.equals(other.taskTypeId))
            return false;
        if (wipDynamicPath == null)
        {
            if (other.wipDynamicPath != null)
                return false;
        }
        else if (!wipDynamicPath.equals(other.wipDynamicPath))
            return false;
        if (oldDynamicPath == null)
        {
            if (other.oldDynamicPath != null)
                return false;
        }
        else if (!oldDynamicPath.equals(other.oldDynamicPath))
            return false;
        if (publishDynamicPath == null)
        {
            if (other.publishDynamicPath != null)
                return false;
        }
        else if (!publishDynamicPath.equals(other.publishDynamicPath))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;

        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
