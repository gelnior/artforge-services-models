package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILComputer;


/**
 * Lightweight implementation for computer object.
 * 
 * @author HD3D
 */
public class LComputer extends LInventoryItem implements ILComputer
{
    private Long inventoryId;
    private String dnsName;
    private String macAdress;
    private String ipAdress;
    private Integer procFrequency;
    private Integer numberOfProcs;
    private Integer numberOfCores;
    private Integer ramQuantity;
    private Integer xPosition;
    private Integer yPosition;
    private String workerStatus;

    private Long processorId;
    private String processorName;
    private Long computerTypeId;
    private String computerTypeName;
    private Long computerModelId;
    private String computerModelName;
    private Long studioMapId;
    private String studioMapName;
    private Long roomId;
    private String roomName;
    private Long personId;
    private String personName;

    private List<Long> poolIDs;
    private List<String> poolNames;
    private List<Long> softwareIDs;
    private List<String> softwareNames;
    private List<Long> licenseIDs;
    private List<String> licenseNames;
    private List<Long> deviceIDs;
    private List<String> deviceNames;
    private List<Long> screenIDs;
    private List<String> screenNames;

    public void clear()
    {
        super.clear();

        inventoryId = null;
        dnsName = null;
        macAdress = null;
        ipAdress = null;
        procFrequency = null;
        numberOfProcs = null;
        numberOfCores = null;
        ramQuantity = null;
        xPosition = null;
        yPosition = null;
        workerStatus = null;

        processorId = null;
        processorName = null;
        computerTypeId = null;
        computerTypeName = null;
        computerModelId = null;
        computerModelName = null;
        studioMapId = null;
        studioMapName = null;
        roomId = null;
        roomName = null;

        if (poolIDs != null)
            poolIDs.clear();
        poolIDs = null;

        if (poolNames != null)
            poolNames.clear();
        poolNames = null;

        if (softwareIDs != null)
            softwareIDs.clear();
        softwareIDs = null;

        if (softwareNames != null)
            softwareNames.clear();
        softwareNames = null;

        if (licenseIDs != null)
            licenseIDs.clear();
        licenseIDs = null;

        if (licenseNames != null)
            licenseNames.clear();
        licenseNames = null;

        if (deviceIDs != null)
            deviceIDs.clear();
        deviceIDs = null;

        if (deviceNames != null)
            deviceNames.clear();
        deviceNames = null;

        if (screenIDs != null)
            screenIDs.clear();
        screenIDs = null;

        if (screenNames != null)
            screenNames.clear();
        screenNames = null;
    }

    /**
     * Default Constructor.
     */
    public LComputer()
    {}

    public LComputer(Long id, Timestamp version, List<Long> tags, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Long inventoryId, String dnsName,
            String ipAdress, String macAdress, Integer procFrequency, Integer numberOfProcs, Integer numberOfCores,
            Integer ramQuantity, String workerStatus, Integer xPosition, Integer yPosition)
    {
        super(id, version, tags, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus);
        this.setTags(tags);
        this.inventoryId = inventoryId;
        this.dnsName = dnsName;
        this.ipAdress = ipAdress;
        this.macAdress = macAdress;
        this.procFrequency = procFrequency;
        this.numberOfProcs = numberOfProcs;
        this.numberOfCores = numberOfCores;
        this.ramQuantity = ramQuantity;
        this.workerStatus = workerStatus;
        this.xPosition = xPosition;
        this.yPosition = yPosition;

        this.setPoolIDs(new ArrayList<Long>());
        this.setPoolNames(new ArrayList<String>());
        this.setSoftwareIDs(new ArrayList<Long>());
        this.setSoftwareNames(new ArrayList<String>());
        this.setLicenseIDs(new ArrayList<Long>());
        this.setLicenseNames(new ArrayList<String>());
        this.setDeviceIDs(new ArrayList<Long>());
        this.setDeviceNames(new ArrayList<String>());
        this.setScreenIDs(new ArrayList<Long>());
        this.setScreenNames(new ArrayList<String>());
    }

    /**
     * @param id
     *            Database object id.
     * @param version
     *            Database object version.
     * @param tags
     *            Tags associated with object.
     * @param name
     *            Software name.
     * @param serial
     *            Software serial number.
     * @param purchaseDate
     *            Software purchase date.
     * @param procFrequency
     *            Processor frequency.
     * @param numberOfProcs
     *            Number of processors.
     * @param numberOfCores
     *            Number of cores.
     * @param ramQuantity
     *            Ram quantity.
     * @param xPosition
     *            X position on the map.
     * @param yPostion
     *            Y position on the map.
     * @return a new instance of lightweight computer.
     */
    public static ILComputer getNewInstance(Long id, Timestamp version, List<Long> tags, String name, String serial,
            String billingReference, Date purchaseDate, Date warrantyEnd, String inventoryStatus, Long inventoryId,
            String dnsName, String ipAdress, String macAdress, Integer procFrequency, Integer numberOfProcs,
            Integer numberOfCores, Integer ramQuantity, String workerStatus, Integer xPosition, Integer yPosition)
    {
        final LComputer computer = new LComputer(id, version, tags, name, serial, billingReference, purchaseDate,
                warrantyEnd, inventoryStatus, inventoryId, dnsName, ipAdress, macAdress, procFrequency, numberOfProcs,
                numberOfCores, ramQuantity, workerStatus, xPosition, yPosition);

        return computer;
    }

    public static ILComputer getNewInstance(String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, String inventoryStatus, Long inventoryId, String dnsName, String ipAdress,
            String macAdress, Integer procFrequency, Integer numberOfProcs, Integer numberOfCores, Integer ramQuantity,
            String workerStatus, Integer xPosition, Integer yPosition)
    {
        return getNewInstance(null, null, null, name, serial, billingReference, purchaseDate, warrantyEnd,
                inventoryStatus, inventoryId, dnsName, ipAdress, macAdress, procFrequency, numberOfProcs,
                numberOfCores, ramQuantity, workerStatus, xPosition, yPosition);
    }

    public Long getInventoryId()
    {
        return this.inventoryId;
    }

    public void setInventoryId(Long inventoryId)
    {
        this.inventoryId = inventoryId;
    }

    public String getDnsName()
    {
        return this.dnsName;
    }

    public void setDnsName(String dnsName)
    {
        this.dnsName = dnsName;
    }

    public String getIpAdress()
    {
        return ipAdress;
    }

    public void setIpAdress(String ipAdress)
    {
        this.ipAdress = ipAdress;
    }

    public String getMacAdress()
    {
        return this.macAdress;
    }

    public void setMacAdress(String macAdress)
    {
        this.macAdress = macAdress;
    }

    public Integer getProcFrequency()
    {
        return procFrequency;
    }

    public void setProcFrequency(Integer procFrequency)
    {
        this.procFrequency = procFrequency;
    }

    public Integer getNumberOfProcs()
    {
        return numberOfProcs;
    }

    public void setNumberOfProcs(Integer numberOfProcs)
    {
        this.numberOfProcs = numberOfProcs;
    }

    public Integer getNumberOfCores()
    {
        return numberOfCores;
    }

    public void setNumberOfCores(Integer numberOfCores)
    {
        this.numberOfCores = numberOfCores;
    }

    public Integer getRamQuantity()
    {
        return ramQuantity;
    }

    public void setRamQuantity(Integer ramQuantity)
    {
        this.ramQuantity = ramQuantity;
    }

    public Integer getxPosition()
    {
        return xPosition;
    }

    public void setxPosition(Integer xPosition)
    {
        this.xPosition = xPosition;
    }

    public Integer getyPosition()
    {
        return yPosition;
    }

    public String getWorkerStatus()
    {
        return this.workerStatus;
    }

    public void setWorkerStatus(String workerStatus)
    {
        this.workerStatus = workerStatus;
    }

    public void setyPosition(Integer yPosition)
    {
        this.yPosition = yPosition;
    }

    public Long getProcessorId()
    {
        return processorId;
    }

    public void setProcessorId(Long processorId)
    {
        this.processorId = processorId;
    }

    public String getProcessorName()
    {
        return processorName;
    }

    public void setProcessorName(String processorName)
    {
        this.processorName = processorName;
    }

    public Long getComputerModelId()
    {
        return computerModelId;
    }

    public void setComputerModelId(Long computerModelId)
    {
        this.computerModelId = computerModelId;
    }

    public String getComputerModelName()
    {
        return computerModelName;
    }

    public void setComputerModelName(String computerModelName)
    {
        this.computerModelName = computerModelName;
    }

    public Long getStudioMapId()
    {
        return this.studioMapId;
    }

    public void setStudioMapId(Long studioMapId)
    {
        this.studioMapId = studioMapId;
    }

    public String getStudioMapName()
    {
        return this.studioMapName;
    }

    public void setStudioMapName(String studioMapName)
    {
        this.studioMapName = studioMapName;
    }

    public final List<Long> getPoolIDs()
    {
        return this.poolIDs;
    }

    public final void setPoolIDs(List<Long> poolIDs)
    {
        this.poolIDs = poolIDs;
    }

    public final List<String> getPoolNames()
    {
        return this.poolNames;
    }

    public final void setPoolNames(List<String> poolNames)
    {
        this.poolNames = poolNames;
    }

    public List<Long> getLicenseIDs()
    {
        return licenseIDs;
    }

    public void setLicenseIDs(List<Long> licenseIDs)
    {
        this.licenseIDs = licenseIDs;
    }

    public List<String> getLicenseNames()
    {
        return licenseNames;
    }

    public void setLicenseNames(List<String> licenseNames)
    {
        this.licenseNames = licenseNames;
    }

    public final List<Long> getSoftwareIDs()
    {
        return this.softwareIDs;
    }

    public final void setSoftwareIDs(List<Long> softwareIDs)
    {
        this.softwareIDs = softwareIDs;
    }

    public final List<String> getSoftwareNames()
    {
        return this.softwareNames;
    }

    public final void setSoftwareNames(List<String> softwareNames)
    {
        this.softwareNames = softwareNames;
    }

    public final List<Long> getDeviceIDs()
    {
        return this.deviceIDs;
    }

    public final void setDeviceIDs(List<Long> deviceIDs)
    {
        this.deviceIDs = deviceIDs;
    }

    public final List<String> getDeviceNames()
    {
        return this.deviceNames;
    }

    public final void setDeviceNames(List<String> deviceNames)
    {
        this.deviceNames = deviceNames;
    }

    public List<Long> getScreenIDs()
    {
        return screenIDs;
    }

    public List<String> getScreenNames()
    {
        return screenNames;
    }

    public void setScreenIDs(List<Long> screenIDs)
    {
        this.screenIDs = screenIDs;
    }

    public void setScreenNames(List<String> screenNames)
    {
        this.screenNames = screenNames;
    }

    public Long getComputerTypeId()
    {
        return computerTypeId;
    }

    public void setComputerTypeId(Long computerTypeId)
    {
        this.computerTypeId = computerTypeId;
    }

    public String getComputerTypeName()
    {
        return computerTypeName;
    }

    public void setComputerTypeName(String computerTypeName)
    {
        this.computerTypeName = computerTypeName;
    }

    public Long getRoomId()
    {
        return roomId;
    }

    public void setRoomId(Long roomId)
    {
        this.roomId = roomId;
    }

    public String getRoomName()
    {
        return roomName;
    }

    public void setRoomName(String roomName)
    {
        this.roomName = roomName;
    }

    /**
     * @param personId
     *            the personId to set
     */
    public void setPersonId(Long personId)
    {
        this.personId = personId;
    }

    /**
     * @return the personId
     */
    public Long getPersonId()
    {
        return personId;
    }

    /**
     * @param personName
     *            the personName to set
     */
    public void setPersonName(String personName)
    {
        this.personName = personName;
    }

    /**
     * @return the personName
     */
    public String getPersonName()
    {
        return personName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerModelId == null) ? 0 : computerModelId.hashCode());
        result = prime * result + ((computerModelName == null) ? 0 : computerModelName.hashCode());
        result = prime * result + ((computerTypeId == null) ? 0 : computerTypeId.hashCode());
        result = prime * result + ((computerTypeName == null) ? 0 : computerTypeName.hashCode());
        result = prime * result + ((dnsName == null) ? 0 : dnsName.hashCode());
        result = prime * result + ((inventoryId == null) ? 0 : inventoryId.hashCode());
        result = prime * result + ((ipAdress == null) ? 0 : ipAdress.hashCode());
        result = prime * result + ((macAdress == null) ? 0 : macAdress.hashCode());
        result = prime * result + ((numberOfCores == null) ? 0 : numberOfCores.hashCode());
        result = prime * result + ((numberOfProcs == null) ? 0 : numberOfProcs.hashCode());
        result = prime * result + ((procFrequency == null) ? 0 : procFrequency.hashCode());
        result = prime * result + ((processorId == null) ? 0 : processorId.hashCode());
        result = prime * result + ((processorName == null) ? 0 : processorName.hashCode());
        result = prime * result + ((ramQuantity == null) ? 0 : ramQuantity.hashCode());
        result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
        result = prime * result + ((roomName == null) ? 0 : roomName.hashCode());
        result = prime * result + ((studioMapId == null) ? 0 : studioMapId.hashCode());
        result = prime * result + ((studioMapName == null) ? 0 : studioMapName.hashCode());
        result = prime * result + ((workerStatus == null) ? 0 : workerStatus.hashCode());
        result = prime * result + ((xPosition == null) ? 0 : xPosition.hashCode());
        result = prime * result + ((yPosition == null) ? 0 : yPosition.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LComputer other = (LComputer) obj;
        if (computerModelId == null)
        {
            if (other.computerModelId != null)
                return false;
        }
        else if (!computerModelId.equals(other.computerModelId))
            return false;
        if (computerModelName == null)
        {
            if (other.computerModelName != null)
                return false;
        }
        else if (!computerModelName.equals(other.computerModelName))
            return false;
        if (computerTypeId == null)
        {
            if (other.computerTypeId != null)
                return false;
        }
        else if (!computerTypeId.equals(other.computerTypeId))
            return false;
        if (computerTypeName == null)
        {
            if (other.computerTypeName != null)
                return false;
        }
        else if (!computerTypeName.equals(other.computerTypeName))
            return false;
        if (dnsName == null)
        {
            if (other.dnsName != null)
                return false;
        }
        else if (!dnsName.equals(other.dnsName))
            return false;
        if (inventoryId == null)
        {
            if (other.inventoryId != null)
                return false;
        }
        else if (!inventoryId.equals(other.inventoryId))
            return false;
        if (ipAdress == null)
        {
            if (other.ipAdress != null)
                return false;
        }
        else if (!ipAdress.equals(other.ipAdress))
            return false;
        if (macAdress == null)
        {
            if (other.macAdress != null)
                return false;
        }
        else if (!macAdress.equals(other.macAdress))
            return false;
        if (numberOfCores == null)
        {
            if (other.numberOfCores != null)
                return false;
        }
        else if (!numberOfCores.equals(other.numberOfCores))
            return false;
        if (numberOfProcs == null)
        {
            if (other.numberOfProcs != null)
                return false;
        }
        else if (!numberOfProcs.equals(other.numberOfProcs))
            return false;
        if (procFrequency == null)
        {
            if (other.procFrequency != null)
                return false;
        }
        else if (!procFrequency.equals(other.procFrequency))
            return false;
        if (processorId == null)
        {
            if (other.processorId != null)
                return false;
        }
        else if (!processorId.equals(other.processorId))
            return false;
        if (processorName == null)
        {
            if (other.processorName != null)
                return false;
        }
        else if (!processorName.equals(other.processorName))
            return false;
        if (ramQuantity == null)
        {
            if (other.ramQuantity != null)
                return false;
        }
        else if (!ramQuantity.equals(other.ramQuantity))
            return false;
        if (roomId == null)
        {
            if (other.roomId != null)
                return false;
        }
        else if (!roomId.equals(other.roomId))
            return false;
        if (roomName == null)
        {
            if (other.roomName != null)
                return false;
        }
        else if (!roomName.equals(other.roomName))
            return false;
        if (studioMapId == null)
        {
            if (other.studioMapId != null)
                return false;
        }
        else if (!studioMapId.equals(other.studioMapId))
            return false;
        if (studioMapName == null)
        {
            if (other.studioMapName != null)
                return false;
        }
        else if (!studioMapName.equals(other.studioMapName))
            return false;
        if (workerStatus == null)
        {
            if (other.workerStatus != null)
                return false;
        }
        else if (!workerStatus.equals(other.workerStatus))
            return false;
        if (xPosition == null)
        {
            if (other.xPosition != null)
                return false;
        }
        else if (!xPosition.equals(other.xPosition))
            return false;
        if (yPosition == null)
        {
            if (other.yPosition != null)
                return false;
        }
        else if (!yPosition.equals(other.yPosition))
            return false;
        return true;
    }

}
