package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILProcessor;


/**
 * Lightweight implementation for processor object.
 * 
 * @author HD3D
 */
public class LProcessor extends LSimple implements ILProcessor
{
    /**
     * Default Constructor.
     */
    public LProcessor()
    {}

    public LProcessor(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILProcessor getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LProcessor processor = new LProcessor(id, version, tags, name);

        return processor;
    }

    public static ILProcessor getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
