package fr.hd3d.model.lightweight.impl;

public class LSetting implements ILPersistent
{
    private Long id;
    protected java.sql.Timestamp version;
    private String login;
    private Long person;// use id instead of entity for decoupling
    private String key;
    private String value;

    public void clear()
    {
        id = null;
        version = null;
        login = null;
        person = null;
        key = null;
        value = null;
    }

    public LSetting()
    {}

    public LSetting(Long id, java.sql.Timestamp version, String login, Long person, String key, String value)
    {
        super();
        this.id = id;
        this.version = version;
        this.login = login;
        this.person = person;
        this.key = key;
        this.value = value;
    }

    public Long getId()
    {
        return id;
    }

    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public String getLogin()
    {
        return login;
    }

    public Long getPerson()
    {
        return person;
    }

    public String getKey()
    {
        return key;
    }

    public String getValue()
    {
        return value;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public void setPerson(Long person)
    {
        this.person = person;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSetting other = (LSetting) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (login == null)
        {
            if (other.login != null)
                return false;
        }
        else if (!login.equals(other.login))
            return false;
        if (person == null)
        {
            if (other.person != null)
                return false;
        }
        else if (!person.equals(other.person))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

}
