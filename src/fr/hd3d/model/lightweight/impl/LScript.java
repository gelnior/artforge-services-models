package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILScript;


/**
 * @author Try LAM
 */
public class LScript extends LBase implements ILScript
{
    private String trigger;
    private String name;
    private String description;
    private String location;
    private Integer exitCode;
    private java.util.Date lastRun;
    private Integer lastExitCode;
    private String lastRunMessage;

    public void clear()
    {
        super.clear();

        trigger = null;
        name = null;
        description = null;
        location = null;
        exitCode = null;
        lastRun = null;
        lastExitCode = null;
        lastRunMessage = null;
    }

    public LScript()
    {
        super();
    }

    public static ILScript getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String trigger,
            String name, String description, String location, Integer exitCode)
    {
        final ILScript script = new LScript();
        script.setId(id);
        script.setVersion(version);
        script.setTags(tags);
        script.setTrigger(trigger);
        script.setName(name);
        script.setDescription(description);
        script.setLocation(location);
        script.setExitCode(exitCode);
        return script;
    }

    public static ILScript getNewInstance(String trigger, String name, String description, String location,
            Integer exitCode)
    {
        return getNewInstance(null, null, null, trigger, name, description, location, exitCode);
    }

    public String getTrigger()
    {
        return trigger;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getLocation()
    {
        return location;
    }

    public Integer getExitCode()
    {
        return exitCode;
    }

    public java.util.Date getLastRun()
    {
        return lastRun;
    }

    public Integer getLastExitCode()
    {
        return lastExitCode;
    }

    public String getLastRunMessage()
    {
        return lastRunMessage;
    }

    public void setTrigger(String trigger)
    {
        this.trigger = trigger;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setExitCode(Integer exitCode)
    {
        this.exitCode = exitCode;
    }

    public void setLastRun(java.util.Date lastRun)
    {
        this.lastRun = lastRun;
    }

    public void setLastExitCode(Integer lastExitCode)
    {
        this.lastExitCode = lastExitCode;
    }

    public void setLastRunMessage(String lastRunMessage)
    {
        this.lastRunMessage = lastRunMessage;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((exitCode == null) ? 0 : exitCode.hashCode());
        result = prime * result + ((lastExitCode == null) ? 0 : lastExitCode.hashCode());
        result = prime * result + ((lastRun == null) ? 0 : lastRun.hashCode());
        result = prime * result + ((lastRunMessage == null) ? 0 : lastRunMessage.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LScript other = (LScript) obj;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (exitCode == null)
        {
            if (other.exitCode != null)
                return false;
        }
        else if (!exitCode.equals(other.exitCode))
            return false;
        if (lastExitCode == null)
        {
            if (other.lastExitCode != null)
                return false;
        }
        else if (!lastExitCode.equals(other.lastExitCode))
            return false;
        if (lastRun == null)
        {
            if (other.lastRun != null)
                return false;
        }
        else if (!lastRun.equals(other.lastRun))
            return false;
        if (lastRunMessage == null)
        {
            if (other.lastRunMessage != null)
                return false;
        }
        else if (!lastRunMessage.equals(other.lastRunMessage))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (trigger == null)
        {
            if (other.trigger != null)
                return false;
        }
        else if (!trigger.equals(other.trigger))
            return false;
        return true;
    }

}
