package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILFactType;


/**
 * @author Try LAM
 */
public class LFactType extends LBase implements ILFactType
{

    private String name;
    private String description;

    public void clear()
    {
        super.clear();
        name = null;
        description = null;
    }

    public LFactType()
    {

    }

    public static ILFactType getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description)
    {
        final ILFactType facttype = new LFactType();
        facttype.setId(id);
        facttype.setVersion(version);
        facttype.setTags(tags);
        facttype.setName(name);
        facttype.setDescription(description);
        return facttype;
    }

    public static ILFactType getNewInstance(String name, String description)
    {
        return getNewInstance(null, null, null, name, description);
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFactType other = (LFactType) obj;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

}
