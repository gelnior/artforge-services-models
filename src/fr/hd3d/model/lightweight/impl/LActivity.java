package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILProject;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public abstract class LActivity extends LBase implements ILActivity
{
    private ILPersonDay day;
    private Long dayID;
    private Date dayDate;
    private Long duration;
    private String comment;
    private ILPerson worker;
    private String workerName;
    private String workerLogin;
    private Long workerID;
    private ILProject project;
    private Long projectID;
    private String projectName;
    private String projectColor;
    private ILPerson filledBy;
    private Date filledDate;
    private Long filledByID;
    private String filledByName;
    private String filledByLogin;
    private ILPerson approvedBy;
    private String projectTypeName;

    public void clear()
    {
        super.clear();
        if (day != null)
            day.clear();
        day = null;

        dayID = null;
        dayDate = null;
        duration = null;
        comment = null;

        if (worker != null)
            worker.clear();
        worker = null;

        workerName = null;
        workerLogin = null;
        workerID = null;

        if (project != null)
            project.clear();
        project = null;

        projectID = null;
        projectName = null;
        projectColor = null;

        if (filledBy != null)
            filledBy.clear();
        filledBy = null;

        filledDate = null;
        filledByID = null;
        filledByName = null;
        filledByLogin = null;

        if (approvedBy != null)
            approvedBy.clear();
        approvedBy = null;
    }

    public final ILPerson getFilledBy()
    {
        return filledBy;
    }

    public final void setFilledBy(ILPerson filledBy)
    {
        this.filledBy = filledBy;
    }

    public final ILPerson getApprovedBy()
    {
        return approvedBy;
    }

    public final void setApprovedBy(ILPerson approvedBy)
    {
        this.approvedBy = approvedBy;
    }

    public final Long getFilledByID()
    {
        return filledByID;
    }

    public final void setFilledByID(Long filledByID)
    {
        this.filledByID = filledByID;
    }

    public final String getFilledByName()
    {
        return filledByName;
    }

    public final void setFilledByName(String filledByName)
    {
        this.filledByName = filledByName;
    }

    public final String getFilledByLogin()
    {
        return filledByLogin;
    }

    public final void setFilledByLogin(String filledByLogin)
    {
        this.filledByLogin = filledByLogin;
    }

    public String getWorkerLogin()
    {
        return workerLogin;
    }

    public void setWorkerLogin(String workerLogin)
    {
        this.workerLogin = workerLogin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getStartDate()
     */
    public final Long getDayID()
    {
        return dayID;
    }

    public final ILPersonDay getDay()
    {
        return day;
    }

    public final Date getDayDate()
    {
        return dayDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getDuration()
     */
    public final Long getDuration()
    {
        return duration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getComment()
     */
    public final String getComment()
    {
        return comment;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getWorker()
     */
    public final ILPerson getWorker()
    {
        return worker;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getWorkerName()
     */
    public final String getWorkerName()
    {
        return workerName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getWorkerID()
     */
    public final Long getWorkerID()
    {
        return workerID;
    }

    public abstract String getTaskName();

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getProject()
     */
    public final ILProject getProject()
    {
        return project;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getProjectID()
     */
    public final Long getProjectID()
    {
        return projectID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getProjectName()
     */
    public final String getProjectName()
    {
        return projectName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#getProjectColor()
     */
    public final String getProjectColor()
    {
        return projectColor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#setStartDate(java.util.Date)
     */
    public final void setDay(ILPersonDay day)
    {
        this.day = day;
    }

    public final void setDayID(Long dayId)
    {
        this.dayID = dayId;
    }

    public final void setDayDate(Date dayDate)
    {
        this.dayDate = dayDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#setDuration(java.lang.Integer)
     */
    public final void setDuration(Long duration)
    {
        this.duration = duration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#setComment(java.lang.String)
     */
    public final void setComment(String comment)
    {
        this.comment = comment;
    }

    public final void setWorker(ILPerson worker)
    {
        this.worker = worker;
    }

    public final void setWorkerName(String workerName)
    {
        this.workerName = workerName;
    }

    public void setWorkerID(Long workerID)
    {
        this.workerID = workerID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILActivity#setProject(fr.hd3d.model.lightweight.ILProject)
     */
    public final void setProject(ILProject project)
    {
        this.project = project;
    }

    public void setProjectID(Long projectID)
    {
        this.projectID = projectID;
    }

    public final void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public final void setProjectColor(String projectColor)
    {
        this.projectColor = projectColor;
    }

    public final void setFilledDate(Date filledDate)
    {
        this.filledDate = filledDate;
    }

    public final Date getFilledDate()
    {
        return filledDate;
    }

    public String getProjectTypeName()
    {
        return projectTypeName;
    }

    public void setProjectTypeName(String projectTypeName)
    {
        this.projectTypeName = projectTypeName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((approvedBy == null) ? 0 : approvedBy.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((day == null) ? 0 : day.hashCode());
        result = prime * result + ((dayDate == null) ? 0 : dayDate.hashCode());
        result = prime * result + ((dayID == null) ? 0 : dayID.hashCode());
        result = prime * result + ((duration == null) ? 0 : duration.hashCode());
        result = prime * result + ((filledBy == null) ? 0 : filledBy.hashCode());
        result = prime * result + ((filledByID == null) ? 0 : filledByID.hashCode());
        result = prime * result + ((filledByLogin == null) ? 0 : filledByLogin.hashCode());
        result = prime * result + ((filledByName == null) ? 0 : filledByName.hashCode());
        result = prime * result + ((filledDate == null) ? 0 : filledDate.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((projectColor == null) ? 0 : projectColor.hashCode());
        result = prime * result + ((projectID == null) ? 0 : projectID.hashCode());
        result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
        result = prime * result + ((worker == null) ? 0 : worker.hashCode());
        result = prime * result + ((workerID == null) ? 0 : workerID.hashCode());
        result = prime * result + ((workerLogin == null) ? 0 : workerLogin.hashCode());
        result = prime * result + ((workerName == null) ? 0 : workerName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LActivity other = (LActivity) obj;
        if (approvedBy == null)
        {
            if (other.approvedBy != null)
                return false;
        }
        else if (!approvedBy.equals(other.approvedBy))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (day == null)
        {
            if (other.day != null)
                return false;
        }
        else if (!day.equals(other.day))
            return false;
        if (dayDate == null)
        {
            if (other.dayDate != null)
                return false;
        }
        else if (!dayDate.equals(other.dayDate))
            return false;
        if (dayID == null)
        {
            if (other.dayID != null)
                return false;
        }
        else if (!dayID.equals(other.dayID))
            return false;
        if (duration == null)
        {
            if (other.duration != null)
                return false;
        }
        else if (!duration.equals(other.duration))
            return false;
        if (filledBy == null)
        {
            if (other.filledBy != null)
                return false;
        }
        else if (!filledBy.equals(other.filledBy))
            return false;
        if (filledByID == null)
        {
            if (other.filledByID != null)
                return false;
        }
        else if (!filledByID.equals(other.filledByID))
            return false;
        if (filledByLogin == null)
        {
            if (other.filledByLogin != null)
                return false;
        }
        else if (!filledByLogin.equals(other.filledByLogin))
            return false;
        if (filledByName == null)
        {
            if (other.filledByName != null)
                return false;
        }
        else if (!filledByName.equals(other.filledByName))
            return false;
        if (filledDate == null)
        {
            if (other.filledDate != null)
                return false;
        }
        else if (!filledDate.equals(other.filledDate))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (projectColor == null)
        {
            if (other.projectColor != null)
                return false;
        }
        else if (!projectColor.equals(other.projectColor))
            return false;
        if (projectID == null)
        {
            if (other.projectID != null)
                return false;
        }
        else if (!projectID.equals(other.projectID))
            return false;
        if (projectName == null)
        {
            if (other.projectName != null)
                return false;
        }
        else if (!projectName.equals(other.projectName))
            return false;
        if (worker == null)
        {
            if (other.worker != null)
                return false;
        }
        else if (!worker.equals(other.worker))
            return false;
        if (workerID == null)
        {
            if (other.workerID != null)
                return false;
        }
        else if (!workerID.equals(other.workerID))
            return false;
        if (workerLogin == null)
        {
            if (other.workerLogin != null)
                return false;
        }
        else if (!workerLogin.equals(other.workerLogin))
            return false;
        if (workerName == null)
        {
            if (other.workerName != null)
                return false;
        }
        else if (!workerName.equals(other.workerName))
            return false;
        return true;
    }

}
