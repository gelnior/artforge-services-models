package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public class LInventoryItem extends LResource
{
    protected String name;
    protected String serial;
    protected String billingReference;
    protected Date purchaseDate;
    protected Date warrantyEnd;
    private String inventorystatus;

    public void clear()
    {
        super.clear();
        name = null;
        serial = null;
        billingReference = null;
        purchaseDate = null;
        warrantyEnd = null;
        inventorystatus = null;
    }

    public LInventoryItem()
    {}

    public LInventoryItem(Long id, Timestamp version, List<Long> tags, String name, String serial,
            String billingReference, Date purchaseDate, Date warrantyEnd, String status)
    {
        super(id, version);
        this.setTags(tags);
        this.name = name;
        this.serial = serial;
        this.billingReference = billingReference;
        this.purchaseDate = purchaseDate;
        this.warrantyEnd = warrantyEnd;
        this.inventorystatus = status;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSerial()
    {
        return serial;
    }

    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    public String getBillingReference()
    {
        return billingReference;
    }

    public void setBillingReference(String billingReference)
    {
        this.billingReference = billingReference;
    }

    public Date getPurchaseDate()
    {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate)
    {
        this.purchaseDate = purchaseDate;
    }

    public Date getWarrantyEnd()
    {
        return warrantyEnd;
    }

    public void setWarrantyEnd(Date warrantyEnd)
    {
        this.warrantyEnd = warrantyEnd;
    }

    public String getInventoryStatus()
    {
        return this.inventorystatus;
    }

    public void setInventoryStatus(String inventorystatus)
    {
        this.inventorystatus = inventorystatus;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((billingReference == null) ? 0 : billingReference.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
        result = prime * result + ((serial == null) ? 0 : serial.hashCode());
        result = prime * result + ((warrantyEnd == null) ? 0 : warrantyEnd.hashCode());
        result = prime * result + ((inventorystatus == null) ? 0 : inventorystatus.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LInventoryItem other = (LInventoryItem) obj;
        if (billingReference == null)
        {
            if (other.billingReference != null)
                return false;
        }
        else if (!billingReference.equals(other.billingReference))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (purchaseDate == null)
        {
            if (other.purchaseDate != null)
                return false;
        }
        else if (!purchaseDate.equals(other.purchaseDate))
            return false;
        if (serial == null)
        {
            if (other.serial != null)
                return false;
        }
        else if (!serial.equals(other.serial))
            return false;
        if (warrantyEnd == null)
        {
            if (other.warrantyEnd != null)
                return false;
        }
        else if (!warrantyEnd.equals(other.warrantyEnd))
            return false;
        if (inventorystatus == null)
        {
            if (other.inventorystatus != null)
                return false;
        }
        else if (!inventorystatus.equals(other.inventorystatus))
            return false;
        return true;
    }
}
