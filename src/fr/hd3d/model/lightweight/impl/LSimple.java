package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILSimple;


/**
 * Lightweight implementation for object with a name.
 * 
 * @author HD3D
 */
public class LSimple extends LBase implements ILSimple
{
    protected String name;

    public void clear()
    {
        super.clear();
        name = null;
    }

    /**
     * Default Constructor.
     */
    public LSimple()
    {}

    public LSimple(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags);
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSimple other = (LSimple) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    public String toString()
    {
        return getName();
    }

}
