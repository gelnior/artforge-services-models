package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILAssetRevisionLink;


public class LAssetRevisionLink extends LBase implements ILAssetRevisionLink
{

    private Long inputAsset;
    private Long outputAsset;
    private String type;

    public void clear()
    {
        super.clear();
        inputAsset = null;
        outputAsset = null;
        type = null;
    }

    public LAssetRevisionLink()
    {
        super();
    }

    public static ILAssetRevisionLink getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Long inputAsset, Long outputAsset, String type)
    {
        ILAssetRevisionLink assetlink = new LAssetRevisionLink();
        assetlink.setId(id);
        assetlink.setVersion(version);
        assetlink.setTags(tags);
        assetlink.setInputAsset(inputAsset);
        assetlink.setOutputAsset(outputAsset);
        assetlink.setType(type);
        return assetlink;
    }

    public static ILAssetRevisionLink getNewInstance(Long inputAsset, Long outputAsset, String type)
    {
        return getNewInstance(null, null, null, inputAsset, outputAsset, type);
    }

    public Long getInputAsset()
    {
        return inputAsset;
    }

    public Long getOutputAsset()
    {
        return outputAsset;
    }

    public String getType()
    {
        return type;
    }

    public void setInputAsset(Long inputAsset)
    {
        this.inputAsset = inputAsset;
    }

    public void setOutputAsset(Long outputAsset)
    {
        this.outputAsset = outputAsset;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((inputAsset == null) ? 0 : inputAsset.hashCode());
        result = prime * result + ((outputAsset == null) ? 0 : outputAsset.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LAssetRevisionLink other = (LAssetRevisionLink) obj;
        if (inputAsset == null)
        {
            if (other.inputAsset != null)
                return false;
        }
        else if (!inputAsset.equals(other.inputAsset))
            return false;
        if (outputAsset == null)
        {
            if (other.outputAsset != null)
                return false;
        }
        else if (!outputAsset.equals(other.outputAsset))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
