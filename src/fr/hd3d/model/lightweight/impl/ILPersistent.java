package fr.hd3d.model.lightweight.impl;

public interface ILPersistent
{
    Long getId();

    void setId(final Long id);

    java.sql.Timestamp getVersion();

    void setVersion(final java.sql.Timestamp version);
}
