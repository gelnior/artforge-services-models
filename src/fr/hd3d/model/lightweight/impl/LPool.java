package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILPool;


/**
 * Lightweight implementation for pool object.
 * 
 * @author HD3D
 */
public class LPool extends LSimple implements ILPool
{
    private Boolean isDispatcherAllowed = false;
    private List<Long> computerIDs;
    private List<String> computerNames;

    public void clear()
    {
        super.clear();

        isDispatcherAllowed = null;

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;
    }

    /**
     * Default Constructor.
     */
    public LPool()
    {}

    public LPool(Long id, Timestamp version, List<Long> tags, String name, Boolean isDispatcherAllowed)
    {
        super(id, version, tags, name);

        this.isDispatcherAllowed = isDispatcherAllowed;

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    public static ILPool getNewInstance(Long id, Timestamp version, List<Long> tags, String name,
            Boolean isDispatcherAllowed)
    {
        final LPool pool = new LPool(id, version, tags, name, isDispatcherAllowed);

        return pool;
    }

    public static ILPool getNewInstance(String name, Boolean isDispatcherAllowed)
    {
        return getNewInstance(null, null, null, name, isDispatcherAllowed);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Boolean getIsDispatcherAllowed()
    {
        if (this.isDispatcherAllowed != null)
        {
            return isDispatcherAllowed;
        }
        else
        {
            return false;
        }
    }

    public void setIsDispatcherAllowed(Boolean isDispatcherAllowed)
    {
        this.isDispatcherAllowed = isDispatcherAllowed;
    }

    public final List<Long> getComputerIDs()
    {
        return this.computerIDs;
    }

    public final List<String> getComputerNames()
    {
        return this.computerNames;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public final void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public final void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerIDs == null) ? 0 : computerIDs.hashCode());
        result = prime * result + ((computerNames == null) ? 0 : computerNames.hashCode());
        result = prime * result + ((isDispatcherAllowed == null) ? 0 : isDispatcherAllowed.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPool other = (LPool) obj;
        if (computerIDs == null)
        {
            if (other.computerIDs != null)
                return false;
        }
        else if (!computerIDs.equals(other.computerIDs))
            return false;
        if (computerNames == null)
        {
            if (other.computerNames != null)
                return false;
        }
        else if (!computerNames.equals(other.computerNames))
            return false;
        if (isDispatcherAllowed == null)
        {
            if (other.isDispatcherAllowed != null)
                return false;
        }
        else if (!isDispatcherAllowed.equals(other.isDispatcherAllowed))
            return false;
        return true;
    }

}
