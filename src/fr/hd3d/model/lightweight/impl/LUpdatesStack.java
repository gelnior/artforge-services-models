package fr.hd3d.model.lightweight.impl;

public class LUpdatesStack implements ILPersistent
{
    private Long id;
    protected java.sql.Timestamp version;
    private String operation;

    public void clear()
    {
        id = null;
        version = null;
        operation = null;
    }

    public LUpdatesStack()
    {}

    public LUpdatesStack(final Long id, final java.sql.Timestamp version, final String operation)
    {
        super();
        this.id = id;
        this.version = version;
        this.operation = operation;
    }

    public Long getId()
    {
        return id;
    }

    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((operation == null) ? 0 : operation.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LUpdatesStack other = (LUpdatesStack) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (operation == null)
        {
            if (other.operation != null)
                return false;
        }
        else if (!operation.equals(other.operation))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

}
