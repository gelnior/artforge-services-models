package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.ILAssetRevision;


public class LAssetRevision extends LBase implements ILAssetRevision
{
    private Set<Long> assetRevisionGroups;
    private Long project;
    private Date creationDate;
    private String key;
    private String variation;
    private Integer revision;
    private Long creator;
    private String name;
    private Long taskType;
    private String taskTypeName;
    private String creatorName;
    private String status;// Use an Enum ?
    private Date lastOperationDate;
    private Long lastUser;
    private String lastUserName;
    private String comment;
    private String validity;// Enum ?
    private String taskTypeColor;
    private String workObjectPath;
    private String workObjectName;
    private Long workObjectId;
    private String boundEntityName;

    private String mountPointWipPath;
    private String mountPointPublishPath;
    private String mountPointOldPath;
    private String wipPath;
    private String publishPath;
    private String oldPath;

    private LinkedHashSet<Long> fileRevisions;

    public void clear()
    {
        super.clear();

        project = null;
        creationDate = null;
        key = null;
        variation = null;
        revision = null;
        creator = null;
        taskType = null;
        name = null;
        status = null;
        lastOperationDate = null;
        lastUser = null;
        comment = null;
        validity = null;

        mountPointWipPath = null;
        mountPointPublishPath = null;
        mountPointOldPath = null;
        wipPath = null;
        publishPath = null;
        oldPath = null;
    }

    public LAssetRevision()
    {
        super();
    }

    public static ILAssetRevision getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long project,
            Date creationDate, String key, String variation, Integer revision, Long creator, Long type,
            String typeName, String name, String status, Date lastOperationDate, Long lastUser, String comment,
            String validity, String wipPath, String publishPath, String oldPath)
    {
        ILAssetRevision asset = new LAssetRevision();
        asset.setId(id);
        asset.setVersion(version);
        asset.setTags(tags);
        asset.setProject(project);
        asset.setCreationDate(creationDate);
        asset.setKey(key);
        asset.setVariation(variation);
        asset.setRevision(revision);
        asset.setCreator(creator);
        asset.setTaskType(type);
        asset.setTaskTypeName(typeName);
        asset.setName(name);
        asset.setStatus(status);
        asset.setLastOperationDate(lastOperationDate);
        asset.setLastUser(lastUser);
        asset.setComment(comment);
        asset.setValidity(validity);
        asset.setWipPath(wipPath);
        asset.setPublishPath(publishPath);
        asset.setOldPath(oldPath);
        asset.setFileRevisions(new LinkedHashSet<Long>());
        return asset;
    }

    public static ILAssetRevision getNewInstance(Long project, Date creationDate, String key, String variation,
            Integer revision, Long creator, Long type, String typeName, String name, String status,
            Date lastOperationDate, Long lastUser, String comment, String validity, String wipPath, String publishPath,
            String oldPath)
    {
        return getNewInstance(null, null, null, project, creationDate, key, variation, revision, creator, type,
                typeName, name, status, lastOperationDate, lastUser, comment, validity, wipPath, publishPath, oldPath);
    }

    public Set<Long> getAssetRevisionGroups()
    {
        return assetRevisionGroups;
    }

    public Long getProject()
    {
        return project;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public String getKey()
    {
        return key;
    }

    public String getVariation()
    {
        return variation;
    }

    public Integer getRevision()
    {
        return revision;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public Long getCreator()
    {
        return creator;
    }

    public String getCreatorName()
    {
        return this.creatorName;
    }

    public Long getTaskType()
    {
        return taskType;
    }

    public String getName()
    {
        return name;
    }

    public String getStatus()
    {
        return status;
    }

    public Date getLastOperationDate()
    {
        return lastOperationDate;
    }

    public Long getLastUser()
    {
        return lastUser;
    }

    public String getLastUserName()
    {
        return this.lastUserName;
    }

    public String getComment()
    {
        return comment;
    }

    public String getValidity()
    {
        return validity;
    }

    public String getWipPath()
    {
        return wipPath;
    }

    public String getPublishPath()
    {
        return publishPath;
    }

    public String getOldPath()
    {
        return oldPath;
    }

    public String getMountPointWipPath()
    {
        return mountPointWipPath;
    }

    public String getMountPointPublishPath()
    {
        return mountPointPublishPath;
    }

    public String getMountPointOldPath()
    {
        return mountPointOldPath;
    }

    public Long getWorkObjectId()
    {
        return workObjectId;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setAssetRevisionGroups(Set<Long> assetRevisionGroups)
    {
        this.assetRevisionGroups = assetRevisionGroups;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setCreator(Long creator)
    {
        this.creator = creator;
    }

    public void setCreatorName(String creatorName)
    {
        this.creatorName = creatorName;
    }

    public void setTaskType(Long type)
    {
        this.taskType = type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setLastOperationDate(Date lastOperationDate)
    {
        this.lastOperationDate = lastOperationDate;
    }

    public void setLastUser(Long lastUser)
    {
        this.lastUser = lastUser;
    }

    public void setLastUserName(String lastUsername)
    {
        this.lastUserName = lastUsername;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setValidity(String validity)
    {
        this.validity = validity;
    }

    public void setWipPath(String wipPath)
    {
        this.wipPath = wipPath;
    }

    public void setPublishPath(String publishPath)
    {
        this.publishPath = publishPath;
    }

    public void setOldPath(String oldPath)
    {
        this.oldPath = oldPath;
    }

    public String getTaskTypeColor()
    {
        return taskTypeColor;
    }

    public void setTaskTypeColor(String taskTypeColor)
    {
        this.taskTypeColor = taskTypeColor;
    }

    public String getWorkObjectName()
    {
        return this.workObjectName;
    }

    public String getWorkObjectPath()
    {
        return this.workObjectPath;
    }

    public void setWorkObjectName(String name)
    {
        this.workObjectName = name;
    }

    public void setWorkObjectId(Long workObjectId)
    {
        this.workObjectId = workObjectId;
    }

    public void setWorkObjectPath(String path)
    {
        this.workObjectPath = path;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((lastOperationDate == null) ? 0 : lastOperationDate.hashCode());
        result = prime * result + ((lastUser == null) ? 0 : lastUser.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((variation == null) ? 0 : variation.hashCode());
        result = prime * result + ((wipPath == null) ? 0 : wipPath.hashCode());
        result = prime * result + ((publishPath == null) ? 0 : publishPath.hashCode());
        result = prime * result + ((oldPath == null) ? 0 : oldPath.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LAssetRevision other = (LAssetRevision) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (creationDate == null)
        {
            if (other.creationDate != null)
                return false;
        }
        else if (!creationDate.equals(other.creationDate))
            return false;
        if (creator == null)
        {
            if (other.creator != null)
                return false;
        }
        else if (!creator.equals(other.creator))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (lastOperationDate == null)
        {
            if (other.lastOperationDate != null)
                return false;
        }
        else if (!lastOperationDate.equals(other.lastOperationDate))
            return false;
        if (lastUser == null)
        {
            if (other.lastUser != null)
                return false;
        }
        else if (!lastUser.equals(other.lastUser))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        if (validity == null)
        {
            if (other.validity != null)
                return false;
        }
        else if (!validity.equals(other.validity))
            return false;
        if (variation == null)
        {
            if (other.variation != null)
                return false;
        }
        else if (!variation.equals(other.variation))
            return false;
        if (wipPath == null)
        {
            if (other.wipPath != null)
                return false;
        }
        else if (!wipPath.equals(other.wipPath))
            return false;
        if (publishPath == null)
        {
            if (other.publishPath != null)
                return false;
        }
        else if (!publishPath.equals(other.publishPath))
            return false;
        if (oldPath == null)
        {
            if (other.oldPath != null)
                return false;
        }
        else if (!oldPath.equals(other.oldPath))
            return false;
        return true;
    }

    public void setFileRevisions(LinkedHashSet<Long> fileRevisionsId)
    {
        this.fileRevisions = fileRevisionsId;
    }

    public LinkedHashSet<Long> getFileRevisions()
    {
        return fileRevisions;
    }

    public void setMountPointOldPath(String mountPointOldPath)
    {
        this.mountPointOldPath = mountPointOldPath;
    }

    public void setMountPointPublishPath(String mountPointPublishPath)
    {
        this.mountPointPublishPath = mountPointPublishPath;
    }

    public void setMountPointWipPath(String mountPointWipPath)
    {
        this.mountPointWipPath = mountPointWipPath;
    }

}
