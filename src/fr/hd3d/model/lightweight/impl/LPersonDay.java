package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILWorkingTime;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class LPersonDay extends LBase implements ILPersonDay
{
    private Date date;
    private ILPerson person;
    private Long personID;
    private String personName;
    private List<ILActivity> activities;
    private LinkedHashSet<Long> activitiesIDs;
    private ILPerson activitiesApprovedBy;
    private Long activitiesApprovedByID;
    private String activitiesApprovedByLogin;
    private Date activitiesApprovedDate;
    private List<ILWorkingTime> workingTimes;
    private List<Long> workingTimesIDs;
    // workaround for mcguff
    private Date startDate1;
    private Date endDate1;
    private Date startDate2;
    private Date endDate2;

    public void clear()
    {
        super.clear();

        date = null;

        if (person != null)
            person.clear();
        person = null;

        personID = null;
        personName = null;

        if (activitiesIDs != null)
        {
            for (ILActivity a : activities)
                a.clear();
            activitiesIDs.clear();
        }

        activitiesIDs = null;

        if (activitiesIDs != null)
            activitiesIDs.clear();
        activitiesIDs = null;

        if (activitiesApprovedBy != null)
            activitiesApprovedBy.clear();
        activitiesApprovedBy = null;

        activitiesApprovedByID = null;
        activitiesApprovedByLogin = null;
        activitiesApprovedDate = null;

        if (workingTimes != null)
        {
            for (ILWorkingTime a : workingTimes)
                a.clear();
            workingTimes.clear();
        }

        workingTimes = null;

        if (workingTimesIDs != null)
            workingTimesIDs.clear();
        workingTimesIDs = null;

        // workaround for mcguff
        startDate1 = null;
        endDate1 = null;
        startDate2 = null;
        endDate2 = null;
    }

    public LPersonDay()
    {
    // TODO Auto-generated constructor stub
    }

    public static ILPersonDay getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Date day,
            Long personId)
    {
        final LPersonDay personDay = new LPersonDay();
        personDay.setId(id);
        personDay.setVersion(version);
        personDay.setTags(tags);
        personDay.setDate(day);
        personDay.setPersonID(personId);
        return personDay;
    }

    public static ILPersonDay getNewInstance(Date day, Long personId)
    {
        return getNewInstance(null, null, null, day, personId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getDay()
     */
    public final Date getDate()
    {
        return date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setDay(java.util.Date)
     */
    public final void setDate(Date date)
    {
        this.date = date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getPerson()
     */
    public final ILPerson getPerson()
    {
        return person;
    }

    /*
     * (non-Javadoc)
     * 
     * @seefr.hd3d.model.lightweight.impl.ILPersonDay#setPerson(fr.hd3d.model. lightweight.ILPerson)
     */
    public final void setPerson(ILPerson person)
    {
        this.person = person;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivities()
     */
    public final List<ILActivity> getActivities()
    {
        return activities;
    }

    public void addActivity(ILActivity activity)
    {
        if (getActivities() == null)
            setActivities(new ArrayList<ILActivity>());
        getActivities().add(activity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivities(java.util.List)
     */
    public final void setActivities(List<ILActivity> activities)
    {
        this.activities = activities;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivitiesApprovedBy()
     */
    public final ILPerson getActivitiesApprovedBy()
    {
        return activitiesApprovedBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivitiesApprovedBy(fr .hd3d.model.lightweight.ILPerson)
     */
    public final void setActivitiesApprovedBy(ILPerson activitiesApprovedBy)
    {
        this.activitiesApprovedBy = activitiesApprovedBy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivitiesApprovedById()
     */
    public final Long getActivitiesApprovedByID()
    {
        return activitiesApprovedByID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivitiesApprovedById( java.lang.Long)
     */
    public final void setActivitiesApprovedByID(Long activitiesApprovedById)
    {
        this.activitiesApprovedByID = activitiesApprovedById;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivitiesApprovedByLogin()
     */
    public final String getActivitiesApprovedByLogin()
    {
        return activitiesApprovedByLogin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivitiesApprovedByLogin (java.lang.String)
     */
    public final void setActivitiesApprovedByLogin(String activitiesApprovedByLogin)
    {
        this.activitiesApprovedByLogin = activitiesApprovedByLogin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivitiesApprovedDate()
     */
    public final Date getActivitiesApprovedDate()
    {
        return activitiesApprovedDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivitiesApprovedDate( java.util.Date)
     */
    public final void setActivitiesApprovedDate(Date activitiesApprovedDate)
    {
        this.activitiesApprovedDate = activitiesApprovedDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#getActivitiesID()
     */
    public final LinkedHashSet<Long> getActivitiesIDs()
    {
        return activitiesIDs;
    }

    public void addWorkingTime(ILWorkingTime workingTime)
    {
        if (getWorkingTimes() == null)
            setWorkingTimes(new ArrayList<ILWorkingTime>());
        getWorkingTimes().add(workingTime);
    }

    public void addWorkingTimeID(Long id)
    {
        if (getWorkingTimesIDs() == null)
            setWorkingTimesIDs(new ArrayList<Long>());
        getWorkingTimesIDs().add(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.lightweight.impl.ILPersonDay#setActivitiesID(java.util. LinkedHashSet)
     */
    public final void setActivitiesIDs(LinkedHashSet<Long> activitiesID)
    {
        this.activitiesIDs = activitiesID;
    }

    public final Long getPersonID()
    {
        return this.personID;
    }

    public final void setPersonID(Long personID)
    {
        this.personID = personID;
    }

    public final String getPersonName()
    {
        return personName;
    }

    public final void setPersonName(String personLogin)
    {
        this.personName = personLogin;
    }

    public List<ILWorkingTime> getWorkingTimes()
    {
        return workingTimes;
    }

    public List<Long> getWorkingTimesIDs()
    {
        return workingTimesIDs;
    }

    public void addActivityID(Long id)
    {
        if (getActivitiesIDs() == null)
            setActivitiesIDs(new LinkedHashSet<Long>());
        getActivitiesIDs().add(id);
    }

    public void setWorkingTimes(List<ILWorkingTime> workingTimes)
    {
        this.workingTimes = workingTimes;
    }

    public void setWorkingTimesIDs(List<Long> workingTimesIDs)
    {
        this.workingTimesIDs = workingTimesIDs;
    }

    public Date getStartDate1()
    {
        return startDate1;
    }

    public Date getEndDate1()
    {
        return endDate1;
    }

    public Date getStartDate2()
    {
        return startDate2;
    }

    public Date getEndDate2()
    {
        return endDate2;
    }

    public void setStartDate1(Date startDate1)
    {
        this.startDate1 = startDate1;
    }

    public void setEndDate1(Date endDate1)
    {
        this.endDate1 = endDate1;
    }

    public void setStartDate2(Date startDate2)
    {
        this.startDate2 = startDate2;
    }

    public void setEndDate2(Date endDate2)
    {
        this.endDate2 = endDate2;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((activitiesApprovedBy == null) ? 0 : activitiesApprovedBy.hashCode());
        result = prime * result + ((activitiesApprovedByID == null) ? 0 : activitiesApprovedByID.hashCode());
        result = prime * result + ((activitiesApprovedByLogin == null) ? 0 : activitiesApprovedByLogin.hashCode());
        result = prime * result + ((activitiesApprovedDate == null) ? 0 : activitiesApprovedDate.hashCode());
        result = prime * result + ((activitiesIDs == null) ? 0 : activitiesIDs.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        result = prime * result + ((personID == null) ? 0 : personID.hashCode());
        result = prime * result + ((personName == null) ? 0 : personName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPersonDay other = (LPersonDay) obj;
        if (activitiesApprovedBy == null)
        {
            if (other.activitiesApprovedBy != null)
                return false;
        }
        else if (!activitiesApprovedBy.equals(other.activitiesApprovedBy))
            return false;
        if (activitiesApprovedByID == null)
        {
            if (other.activitiesApprovedByID != null)
                return false;
        }
        else if (!activitiesApprovedByID.equals(other.activitiesApprovedByID))
            return false;
        if (activitiesApprovedByLogin == null)
        {
            if (other.activitiesApprovedByLogin != null)
                return false;
        }
        else if (!activitiesApprovedByLogin.equals(other.activitiesApprovedByLogin))
            return false;
        if (activitiesApprovedDate == null)
        {
            if (other.activitiesApprovedDate != null)
                return false;
        }
        else if (!activitiesApprovedDate.equals(other.activitiesApprovedDate))
            return false;
        if (activitiesIDs == null)
        {
            if (other.activitiesIDs != null)
                return false;
        }
        else if (!activitiesIDs.equals(other.activitiesIDs))
            return false;
        if (date == null)
        {
            if (other.date != null)
                return false;
        }
        else if (!date.equals(other.date))
            return false;
        if (person == null)
        {
            if (other.person != null)
                return false;
        }
        else if (!person.equals(other.person))
            return false;
        if (personID == null)
        {
            if (other.personID != null)
                return false;
        }
        else if (!personID.equals(other.personID))
            return false;
        if (personName == null)
        {
            if (other.personName != null)
                return false;
        }
        else if (!personName.equals(other.personName))
            return false;
        return true;
    }

}
