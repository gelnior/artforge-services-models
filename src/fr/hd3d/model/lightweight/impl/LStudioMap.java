package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILStudioMap;


public class LStudioMap extends LBase implements ILStudioMap
{
    private String name;
    private String imagePath;
    private Integer sizeX;
    private Integer sizeY;
    private List<Long> computerIDs;
    private List<String> computerNames;

    public void clear()
    {
        super.clear();

        name = null;
        imagePath = null;
        sizeX = null;
        sizeY = null;

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;
    }

    public LStudioMap()
    {}

    public LStudioMap(String name, String imagePath, Integer sizeX, Integer sizeY)
    {
        super();
        this.name = name;
        this.imagePath = imagePath;
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    /**
     * @param id
     *            object database id.
     * @param version
     *            object database version.
     * @param name
     *            map name.
     * @param imagePath
     *            image path that represents map.
     * @param sizeX
     *            width in pixel.
     * @param sizeY
     *            height in pixel.
     * @return a new instance of lightweight studio map.
     */
    public static ILStudioMap getNewInstance(Long id, Timestamp version, List<Long> tags, String name,
            String imagePath, Integer sizeX, Integer sizeY)
    {
        final LStudioMap studioMap = new LStudioMap(name, imagePath, sizeX, sizeY);
        studioMap.setId(id);
        studioMap.setVersion(version);
        studioMap.setTags(tags);
        return studioMap;
    }

    /**
     * @param name
     *            map name.
     * @param imagePath
     *            image path that represents map.
     * @param sizeX
     *            width in pixel.
     * @param sizeY
     *            height in pixel.
     * @return a new instance of lightweight studio map with id, version and tags set to null.
     */
    public static ILStudioMap getNewInstance(String name, String imagePath, Integer sizeX, Integer sizeY)
    {
        return getNewInstance(null, null, null, name, imagePath, sizeX, sizeY);
    }

    public String getName()
    {
        return name;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public Integer getSizeX()
    {
        return sizeX;
    }

    public Integer getSizeY()
    {
        return sizeY;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    public void setSizeX(Integer sizeX)
    {
        this.sizeX = sizeX;
    }

    public void setSizeY(Integer sizeY)
    {
        this.sizeY = sizeY;
    }

    public List<Long> getComputerIDs()
    {
        return this.computerIDs;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public List<String> getComputerNames()
    {
        return this.computerNames;
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerIDs == null) ? 0 : computerIDs.hashCode());
        result = prime * result + ((imagePath == null) ? 0 : imagePath.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((sizeX == null) ? 0 : sizeX.hashCode());
        result = prime * result + ((sizeY == null) ? 0 : sizeY.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LStudioMap other = (LStudioMap) obj;
        if (computerIDs == null)
        {
            if (other.computerIDs != null)
                return false;
        }
        else if (!computerIDs.equals(other.computerIDs))
            return false;
        if (imagePath == null)
        {
            if (other.imagePath != null)
                return false;
        }
        else if (!imagePath.equals(other.imagePath))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (sizeX == null)
        {
            if (other.sizeX != null)
                return false;
        }
        else if (!sizeX.equals(other.sizeX))
            return false;
        if (sizeY == null)
        {
            if (other.sizeY != null)
                return false;
        }
        else if (!sizeY.equals(other.sizeY))
            return false;
        return true;
    }

}
