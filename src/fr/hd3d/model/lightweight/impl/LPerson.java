package fr.hd3d.model.lightweight.impl;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.model.lightweight.ILPerson;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public class LPerson extends LResource implements ILPerson
{

    private String login;
    private String firstName;
    private String lastName;
    private String name;
    private String email;
    private String phone;
    private String photoPath;
    private Long accountStatus;

    private List<Long> skillLevelIDs;
    private List<String> skillLevelNames;
    private List<Long> contractIDs;
    private List<String> contractNames;

    public void clear()
    {
        super.clear();
        login = null;
        firstName = null;
        lastName = null;
        name = null;
        email = null;
        phone = null;
        photoPath = null;
        accountStatus = null;
        if (skillLevelIDs != null)
            skillLevelIDs.clear();
        skillLevelIDs = null;
        if (skillLevelNames != null)
            skillLevelNames.clear();
        skillLevelNames = null;
        if (contractIDs != null)
            contractIDs.clear();
        contractIDs = null;
        if (contractNames != null)
            contractNames.clear();
        contractNames = null;
    }

    /**
     * Default CTor
     */
    public LPerson()
    {}

    public static ILPerson getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String login,
            String firstName, String lastName, String email, String phone, String photoPath, Long accountStatus)
    {
        final LPerson person = new LPerson();
        person.setId(id);
        person.setVersion(version);
        person.setTags(tags);
        person.setLogin(login);
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setEmail(email);
        person.setPhone(phone);
        person.setPhotoPath(photoPath);
        person.setSkillLevelIDs(new ArrayList<Long>());
        person.setSkillLevelNames(new ArrayList<String>());
        person.setAccountStatus(accountStatus);
        person.setName(person.buildName());

        return person;
    }

    public static ILPerson getNewInstance(String login, String firstName, String lastName, String email, String phone,
            String photoPath, Long accountStatus)
    {
        return getNewInstance(null, null, null, login, firstName, lastName, email, phone, photoPath, accountStatus);
    }

    public final String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public final String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public final String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public final String buildName()
    {
        // final String first = getFirstName();
        // final String last = getLastName();
        // StringBuilder buffer = new StringBuilder();
        // buffer.append(first.substring(0, 1).toUpperCase());
        // buffer.append(first.substring(1).toLowerCase());
        // buffer.append(' ');
        // buffer.append(last.substring(0, 1).toUpperCase());
        // buffer.append(last.substring(1).toLowerCase());
        // buffer.trimToSize();
        // return buffer.toString();
        return PersonNameUtils.getFullName(getLastName(), getFirstName(), getLogin());
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhotoPath()
    {
        return photoPath;
    }

    public void setPhotoPath(String photoPath)
    {
        this.photoPath = photoPath;
    }

    public final List<Long> getSkillLevelIDs()
    {
        return this.skillLevelIDs;
    }

    public final void setSkillLevelIDs(List<Long> skillLevelIDs)
    {
        this.skillLevelIDs = skillLevelIDs;
    }

    public final List<String> getSkillLevelNames()
    {
        return this.skillLevelNames;
    }

    public void addSkillLevelID(Long id)
    {
        if (getSkillLevelIDs() == null)
            setSkillLevelIDs(new ArrayList<Long>());
        getSkillLevelIDs().add(id);
    }

    public final void setSkillLevelNames(List<String> skillLevelNames)
    {
        this.skillLevelNames = skillLevelNames;
    }

    public void addSkillLevelName(String name)
    {
        if (getSkillLevelNames() == null)
            setSkillLevelNames(new ArrayList<String>());
        getSkillLevelNames().add(name);
    }

    public List<Long> getContractIDs()
    {
        return contractIDs;
    }

    public void addContractID(Long id)
    {
        if (getContractIDs() == null)
            setContractIDs(new ArrayList<Long>());
        getContractIDs().add(id);
    }

    public void setContractIDs(List<Long> contractIDs)
    {
        this.contractIDs = contractIDs;
    }

    public List<String> getContractNames()
    {
        return contractNames;
    }

    public void addContractName(String name)
    {
        if (getContractNames() == null)
            setContractNames(new ArrayList<String>());
        getContractNames().add(name);
    }

    public void setContractNames(List<String> contractNames)
    {
        this.contractNames = contractNames;
    }

    public Long getAccountStatus()
    {
        return accountStatus;
    }

    public void setAccountStatus(Long accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((accountStatus == null) ? 0 : accountStatus.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((photoPath == null) ? 0 : photoPath.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPerson other = (LPerson) obj;
        if (accountStatus == null)
        {
            if (other.accountStatus != null)
                return false;
        }
        else if (!accountStatus.equals(other.accountStatus))
            return false;
        if (email == null)
        {
            if (other.email != null)
                return false;
        }
        else if (!email.equals(other.email))
            return false;
        if (firstName == null)
        {
            if (other.firstName != null)
                return false;
        }
        else if (!firstName.equals(other.firstName))
            return false;
        if (lastName == null)
        {
            if (other.lastName != null)
                return false;
        }
        else if (!lastName.equals(other.lastName))
            return false;
        if (login == null)
        {
            if (other.login != null)
                return false;
        }
        else if (!login.equals(other.login))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (phone == null)
        {
            if (other.phone != null)
                return false;
        }
        else if (!phone.equals(other.phone))
            return false;
        if (photoPath == null)
        {
            if (other.photoPath != null)
                return false;
        }
        else if (!photoPath.equals(other.photoPath))
            return false;
        return true;
    }

    public String toString()
    {
        return this.getName();
    }
}
