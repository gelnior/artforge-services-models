package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILHint;


/**
 * @author Try LAM
 */
public class LHint extends LBase implements ILHint
{
    private String name;
    private String description;

    public void clear()
    {
        super.clear();
        name = null;
        description = null;
    }

    public LHint()
    {}

    public static ILHint getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description)
    {
        final ILHint hint = new LHint();
        hint.setId(id);
        hint.setVersion(version);
        hint.setTags(tags);
        hint.setName(name);
        hint.setDescription(description);
        return hint;
    }

    public static ILHint getNewInstance(String name, String description)
    {
        return getNewInstance(null, null, null, name, description);
    }

    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LHint other = (LHint) obj;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

}
