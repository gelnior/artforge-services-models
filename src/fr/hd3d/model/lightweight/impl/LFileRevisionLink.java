package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILFileRevisionLink;


public class LFileRevisionLink extends LBase implements ILFileRevisionLink
{
    private Long inputFile;
    private Long outputFile;
    private String type;

    public void clear()
    {
        super.clear();
        inputFile = null;
        outputFile = null;
        type = null;
    }

    public LFileRevisionLink()
    {
        super();
    }

    public static ILFileRevisionLink getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Long inputFile, Long outputFile, String type)
    {
        ILFileRevisionLink filelink = new LFileRevisionLink();
        filelink.setId(id);
        filelink.setVersion(version);
        filelink.setTags(tags);
        filelink.setInputFile(inputFile);
        filelink.setOutputFile(outputFile);
        filelink.setType(type);
        return filelink;
    }

    public static ILFileRevisionLink getNewInstance(Long inputFile, Long outputFile, String type)
    {
        return getNewInstance(null, null, null, inputFile, outputFile, type);
    }

    public Long getInputFile()
    {
        return inputFile;
    }

    public Long getOutputFile()
    {
        return outputFile;
    }

    public String getType()
    {
        return type;
    }

    public void setInputFile(Long inputFile)
    {
        this.inputFile = inputFile;
    }

    public void setOutputFile(Long outputFile)
    {
        this.outputFile = outputFile;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((inputFile == null) ? 0 : inputFile.hashCode());
        result = prime * result + ((outputFile == null) ? 0 : outputFile.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFileRevisionLink other = (LFileRevisionLink) obj;
        if (inputFile == null)
        {
            if (other.inputFile != null)
                return false;
        }
        else if (!inputFile.equals(other.inputFile))
            return false;
        if (outputFile == null)
        {
            if (other.outputFile != null)
                return false;
        }
        else if (!outputFile.equals(other.outputFile))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
