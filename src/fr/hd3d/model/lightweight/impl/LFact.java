package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILFact;


/**
 * @author Try LAM
 */
public class LFact extends LBase implements ILFact
{

    private String name;
    private String description;
    private Long facttype;
    private Long composition;

    public void clear()
    {
        super.clear();
        name = null;
        description = null;
        facttype = null;
        composition = null;
    }

    public LFact()
    {

    }

    public static ILFact getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description, Long facttype)
    {
        final ILFact fact = new LFact();
        fact.setId(id);
        fact.setVersion(version);
        fact.setTags(tags);
        fact.setName(name);
        fact.setDescription(description);
        fact.setFactType(facttype);
        return fact;
    }

    public static ILFact getNewInstance(String name, String description, Long facttype)
    {
        return getNewInstance(null, null, null, name, description, facttype);
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getComposition()
    {
        return composition;
    }

    public void setComposition(Long composition)
    {
        this.composition = composition;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getFactType()
    {
        return facttype;
    }

    public void setFactType(Long facttype)
    {
        this.facttype = facttype;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((composition == null) ? 0 : composition.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((facttype == null) ? 0 : facttype.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFact other = (LFact) obj;
        if (composition == null)
        {
            if (other.composition != null)
                return false;
        }
        else if (!composition.equals(other.composition))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (facttype == null)
        {
            if (other.facttype != null)
                return false;
        }
        else if (!facttype.equals(other.facttype))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

}
