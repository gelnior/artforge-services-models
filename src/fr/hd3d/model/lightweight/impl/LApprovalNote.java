package fr.hd3d.model.lightweight.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;


public class LApprovalNote extends LBase implements ILApprovalNote
{
    private Long boundEntity;
    private String boundEntityName;
    private Date approvalDate;
    private String status;
    private String comment;
    private Long approver;
    private String approverName;
    private List<Long> filerevisions;
    private Collection<Long> proxies;
    private String type;
    private Long approvalNoteType;
    private String media;
    private String approvalNoteTypeName;
    private Long taskTypeId;// READ-ONLY
    private String taskTypeName;// READ-ONLY
    private Collection<Long> graphicalAnnotationIDs;

    public void clear()
    {
        super.clear();
        boundEntity = null;
        boundEntityName = null;
        approvalDate = null;
        status = null;
        comment = null;
        approver = null;
        setApproverName(null);
        filerevisions = null;
        type = null;
        approvalNoteType = null;
        media = null;
        taskTypeId = null;
        taskTypeName = null;
    }

    public LApprovalNote()
    {}

    public static ILApprovalNote getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Date approvalDate, String status, String comment, Long approver, Long entity, String entityName,
            String type, Long approvalNoteType, String media)
    {
        final ILApprovalNote note = new LApprovalNote();
        note.setId(id);
        note.setVersion(version);
        note.setTags(tags);
        note.setApprovalDate(approvalDate);
        note.setStatus(status);
        note.setComment(comment);
        note.setApprover(approver);
        note.setBoundEntity(entity);
        note.setBoundEntityName(entityName);
        note.setType(type);
        note.setMedia(media);
        note.setApprovalNoteType(approvalNoteType);
        return note;
    }

    public static ILApprovalNote getNewInstance(Date approvalDate, String status, String comment, Long approver,
            Long entity, String entityName, String type, Long approvalNoteType, String media)
    {
        return getNewInstance(null, null, null, approvalDate, status, comment, approver, entity, entityName, type,
                approvalNoteType, media);
    }

    public Long getBoundEntity()
    {
        return boundEntity;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public Date getApprovalDate()
    {
        return approvalDate;
    }

    public String getStatus()
    {
        return status;
    }

    public String getComment()
    {
        return comment;
    }

    public Long getApprover()
    {
        return approver;
    }

    public List<Long> getFilerevisions()
    {
        if (filerevisions == null)
        {
            filerevisions = new ArrayList<Long>();
        }
        return filerevisions;
    }

    public String getType()
    {
        return type;
    }

    public Long getApprovalNoteType()
    {
        return approvalNoteType;
    }

    public void setApprovalNoteType(Long approvalNoteType)
    {
        this.approvalNoteType = approvalNoteType;
    }

    public String getMedia()
    {
        return media;
    }

    public String getApproverName()
    {
        return approverName;
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setApprovalDate(Date approvalDate)
    {
        this.approvalDate = approvalDate;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setApprover(Long approver)
    {
        this.approver = approver;
    }

    public void setFilerevisions(List<Long> filerevision)
    {
        this.filerevisions = filerevision;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setMedia(String media)
    {
        this.media = media;
    }

    public void setApproverName(String approverName)
    {
        this.approverName = approverName;
    }

    public String getApprovalNoteTypeName()
    {
        return this.approvalNoteTypeName;
    }

    public void setApprovalNoteTypeName(String approvalNoteTypeName)
    {
        this.approvalNoteTypeName = approvalNoteTypeName;
    }

    public Long getTaskTypeId()
    {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId)
    {
        this.taskTypeId = taskTypeId;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public String toString()
    {
        SimpleDateFormat format = new SimpleDateFormat(ILBase.DATE_PATTERN);
        return format.format(approvalDate) + "-" + status;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((approvalDate == null) ? 0 : approvalDate.hashCode());
        result = prime * result + ((approvalNoteType == null) ? 0 : approvalNoteType.hashCode());
        result = prime * result + ((approver == null) ? 0 : approver.hashCode());
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((filerevisions == null) ? 0 : filerevisions.hashCode());
        result = prime * result + ((media == null) ? 0 : media.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LApprovalNote other = (LApprovalNote) obj;
        if (approvalDate == null)
        {
            if (other.approvalDate != null)
                return false;
        }
        else if (!approvalDate.equals(other.approvalDate))
            return false;
        if (approvalNoteType == null)
        {
            if (other.approvalNoteType != null)
                return false;
        }
        else if (!approvalNoteType.equals(other.approvalNoteType))
            return false;
        if (approver == null)
        {
            if (other.approver != null)
                return false;
        }
        else if (!approver.equals(other.approver))
            return false;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (filerevisions == null)
        {
            if (other.filerevisions != null)
                return false;
        }
        else if (!filerevisions.equals(other.filerevisions))
            return false;
        if (media == null)
        {
            if (other.media != null)
                return false;
        }
        else if (!media.equals(other.media))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    public Collection<Long> getProxies()
    {
        if (proxies == null)
        {
            proxies = new ArrayList<Long>();
        }
        return proxies;
    }

    public void setProxies(Collection<Long> proxies)
    {
        this.proxies = proxies;
    }

    public Collection<Long> getGraphicalAnnotationIDs()
    {
        if (graphicalAnnotationIDs == null)
        {
            graphicalAnnotationIDs = new ArrayList<Long>();
        }
        return graphicalAnnotationIDs;
    }

    public void setGraphicalAnnotationIDs(Collection<Long> graphicalAnnotations)
    {
        this.graphicalAnnotationIDs = graphicalAnnotations;
    }

}
