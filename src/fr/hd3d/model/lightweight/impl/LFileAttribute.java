package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILFileAttribute;


public class LFileAttribute extends LBase implements ILFileAttribute
{
    private Long file;
    private String name;
    private String value;

    public void clear()
    {
        super.clear();
        file = null;
        name = null;
        value = null;
    }

    public LFileAttribute()
    {
        super();
    }

    public static ILFileAttribute getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long file,
            String name, String value)
    {
        ILFileAttribute fileattribute = new LFileAttribute();
        fileattribute.setId(id);
        fileattribute.setVersion(version);
        fileattribute.setTags(tags);
        fileattribute.setFile(file);
        fileattribute.setName(name);
        fileattribute.setValue(value);
        return fileattribute;
    }

    public static ILFileAttribute getNewInstance(Long inputFile, Long file, String name, String value)
    {
        return getNewInstance(null, null, null, file, name, value);
    }

    public Long getFile()
    {
        return file;
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public void setFile(Long file)
    {
        this.file = file;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((file == null) ? 0 : file.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFileAttribute other = (LFileAttribute) obj;
        if (file == null)
        {
            if (other.file != null)
                return false;
        }
        else if (!file.equals(other.file))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }

}
