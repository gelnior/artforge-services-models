package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILScreenModel;


/**
 * Lightweight implementation for processor object.
 * 
 * @author HD3D
 */
public class LScreenModel extends LModel implements ILScreenModel
{
    /**
     * Default Constructor.
     */
    public LScreenModel()
    {}

    public LScreenModel(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILScreenModel getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LScreenModel screenModel = new LScreenModel(id, version, tags, name);

        return screenModel;
    }

    public static ILScreenModel getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
