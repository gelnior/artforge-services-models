package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILEntityTaskLink;


public class LEntityTaskLink extends LBase implements ILEntityTaskLink
{
    private Long boundEntity;
    private String boundEntityName;
    private Long task;
    private String extra;

    public void clear()
    {
        super.clear();
        boundEntity = null;
        boundEntityName = null;
        task = null;
        extra = null;
    }

    public LEntityTaskLink()
    {}

    public static ILEntityTaskLink getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Long boundEntity, String boundEntityName, Long task, String extra)
    {
        final ILEntityTaskLink entityTaskLink = new LEntityTaskLink();
        entityTaskLink.setId(id);
        entityTaskLink.setVersion(version);
        entityTaskLink.setTags(tags);
        entityTaskLink.setBoundEntity(boundEntity);
        entityTaskLink.setBoundEntityName(boundEntityName);
        entityTaskLink.setTask(task);
        entityTaskLink.setExtra(extra);
        return entityTaskLink;
    }

    public static ILEntityTaskLink getNewInstance(Long boundEntity, String boundEntityName, Long task, String extra)
    {
        return getNewInstance(null, null, null, boundEntity, boundEntityName, task, extra);
    }

    public Long getBoundEntity()
    {
        return boundEntity;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public Long getTask()
    {
        return task;
    }

    public String getExtra()
    {
        return extra;
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTask(Long task)
    {
        this.task = task;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((task == null) ? 0 : task.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LEntityTaskLink other = (LEntityTaskLink) obj;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (task == null)
        {
            if (other.task != null)
                return false;
        }
        else if (!task.equals(other.task))
            return false;
        return true;
    }

}
