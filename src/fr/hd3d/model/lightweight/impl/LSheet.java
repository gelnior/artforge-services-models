package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILSheet;


/**
 * @author Try LAM
 */
public class LSheet extends LBase implements ILSheet
{

    private String name;
    private String description;
    private Long project;
    private String boundClassName;
    private List<Long> itemGroups;
    private String type;

    public void clear()
    {
        super.clear();
        name = null;
        description = null;
        project = null;
        boundClassName = null;
        if (itemGroups != null)
            itemGroups.clear();
        itemGroups = null;
    }

    public LSheet()
    {}

    public static LSheet getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            String description, Long project, String boundClassName, List<Long> itemGroups)
    {
        final LSheet sheet = new LSheet();
        sheet.setId(id);
        sheet.setVersion(version);
        sheet.setTags(tags);
        sheet.setName(name);
        sheet.setDescription(description);
        sheet.setProject(project);
        sheet.setBoundClassName(boundClassName);
        sheet.setItemGroups(itemGroups);
        return sheet;
    }

    public static LSheet getNewInstance(String name, String description, Long project, String boundClassName,
            List<Long> itemGroups)
    {
        return getNewInstance(null, null, null, name, description, project, boundClassName, itemGroups);
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public Long getProject()
    {
        return project;
    }

    public String getBoundClassName()
    {
        return boundClassName;
    }

    public List<Long> getItemGroups()
    {
        return itemGroups;
    }

    public String getType()
    {
        return type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public void setBoundClassName(String boundClassName)
    {
        this.boundClassName = boundClassName;
    }

    public void setItemGroups(List<Long> itemGroups)
    {
        this.itemGroups = itemGroups;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundClassName == null) ? 0 : boundClassName.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((itemGroups == null) ? 0 : itemGroups.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSheet other = (LSheet) obj;
        if (boundClassName == null)
        {
            if (other.boundClassName != null)
                return false;
        }
        else if (!boundClassName.equals(other.boundClassName))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (itemGroups == null)
        {
            if (other.itemGroups != null)
                return false;
        }
        else if (!itemGroups.equals(other.itemGroups))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    public String toString()
    {
        StringBuilder buf = new StringBuilder(super.toString());
        buf.append(super.toString()).append(',');
        buf.append("name=" + name).append(',');
        buf.append("project=" + project).append(',');
        buf.append("boundClassName=" + boundClassName).append(',');
        buf.append("itemGroups=" + itemGroups).append(',');
        buf.append("type=" + type);
        return buf.toString();
    }

}
