package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILDevice;


/**
 * Lightweight implementation for device object.
 * 
 * @author HD3D
 */
public class LDevice extends LInventoryItem implements ILDevice
{
    private Float size;
    private Long deviceTypeId;
    private String deviceTypeName;
    private Long deviceModelId;
    private String deviceModelName;
    private List<Long> computerIDs;
    private List<String> computerNames;

    public void clear()
    {
        super.clear();

        size = null;
        deviceTypeId = null;
        deviceTypeName = null;
        deviceModelId = null;
        deviceModelName = null;

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;
    }

    /**
     * Default Constructor.
     */
    public LDevice()
    {}

    public LDevice(Long id, Timestamp version, List<Long> tags, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Float size)
    {
        super(id, version, null, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus);

        this.size = size;

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    public static ILDevice getNewInstance(Long id, Timestamp version, List<Long> tags, String name, String serial,
            String billingReference, Date purchaseDate, Date warrantyEnd, String inventoryStatus, Float size)
    {
        final LDevice device = new LDevice(id, version, tags, name, serial, billingReference, purchaseDate,
                warrantyEnd, inventoryStatus, size);
        return device;
    }

    public static ILDevice getNewInstance(String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, String inventoryStatus, Float size)
    {
        return getNewInstance(null, null, null, name, serial, billingReference, purchaseDate, warrantyEnd,
                inventoryStatus, size);
    }

    public Float getSize()
    {
        return size;
    }

    public void setSize(Float size)
    {
        this.size = size;
    }

    public Long getDeviceTypeId()
    {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long typeId)
    {
        this.deviceTypeId = typeId;
    }

    public String getDeviceTypeName()
    {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String typeName)
    {
        this.deviceTypeName = typeName;
    }

    public Long getDeviceModelId()
    {
        return deviceModelId;
    }

    public void setDeviceModelId(Long deviceModelId)
    {
        this.deviceModelId = deviceModelId;
    }

    public String getDeviceModelName()
    {
        return deviceModelName;
    }

    public void setDeviceModelName(String deviceModelName)
    {
        this.deviceModelName = deviceModelName;
    }

    public final List<Long> getComputerIDs()
    {
        return this.computerIDs;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public final void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public final List<String> getComputerNames()
    {
        return this.computerNames;
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public final void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((deviceModelId == null) ? 0 : deviceModelId.hashCode());
        result = prime * result + ((deviceModelName == null) ? 0 : deviceModelName.hashCode());
        result = prime * result + ((deviceTypeId == null) ? 0 : deviceTypeId.hashCode());
        result = prime * result + ((deviceTypeName == null) ? 0 : deviceTypeName.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LDevice other = (LDevice) obj;
        if (deviceModelId == null)
        {
            if (other.deviceModelId != null)
                return false;
        }
        else if (!deviceModelId.equals(other.deviceModelId))
            return false;
        if (deviceModelName == null)
        {
            if (other.deviceModelName != null)
                return false;
        }
        else if (!deviceModelName.equals(other.deviceModelName))
            return false;
        if (deviceTypeId == null)
        {
            if (other.deviceTypeId != null)
                return false;
        }
        else if (!deviceTypeId.equals(other.deviceTypeId))
            return false;
        if (deviceTypeName == null)
        {
            if (other.deviceTypeName != null)
                return false;
        }
        else if (!deviceTypeName.equals(other.deviceTypeName))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        return true;
    }

}
