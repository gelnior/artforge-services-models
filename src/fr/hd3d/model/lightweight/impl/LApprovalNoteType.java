package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILApprovalNoteType;


public class LApprovalNoteType extends LBase implements ILApprovalNoteType
{
    private Long project;
    private String name;
    private Long taskType;
    private String taskTypeName;
    private String color;

    public void clear()
    {
        super.clear();
        project = null;
        name = null;
        taskType = null;
        taskTypeName = null;
    }

    public LApprovalNoteType()
    {}

    public static LApprovalNoteType getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            Long taskType, Long projectId)
    {
        final LApprovalNoteType noteType = new LApprovalNoteType();

        noteType.setId(id);
        noteType.setVersion(version);
        noteType.setTags(tags);
        noteType.setName(name);
        noteType.setTaskType(taskType);
        noteType.setProject(projectId);
        noteType.setTaskTypeName("");
        return noteType;
    }

    public static ILApprovalNoteType getNewInstance(String name, Long taskType, Long projectId)
    {
        return getNewInstance(null, null, null, name, taskType, projectId);
    }

    public String getName()
    {
        return name;
    }

    public Long getTaskType()
    {
        return taskType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTaskType(Long taskType)
    {
        this.taskType = taskType;
    }

    public Long getProject()
    {
        return project;
    }

    public void setProject(Long projectID)
    {
        this.project = projectID;
    }

    public String getTaskTypeName()
    {
        return this.taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public String getTaskTypeColor()
    {
        return this.color;
    }

    public void setTaskTypeColor(String color)
    {
        this.color = color;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        result = prime * result + ((taskTypeName == null) ? 0 : taskTypeName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LApprovalNoteType other = (LApprovalNoteType) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        if (taskTypeName == null)
        {
            if (other.taskTypeName != null)
                return false;
        }
        else if (!taskTypeName.equals(other.taskTypeName))
            return false;
        return true;
    }

}
