package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskChanges;


public class LTaskChanges extends LTaskBase implements ILTaskChanges
{
    private ILTask task;
    private String taskName;
    private Long taskID;
    private ILPerson worker;
    private Long workerID;
    private String personName;
    private ILPlanning planning;
    private Long planningID;
    private String workObjectName;
    private String color;

    public void clear()
    {
        super.clear();

        if (task != null)
            task.clear();
        task = null;

        taskName = null;
        taskID = null;

        if (worker != null)
            worker.clear();
        worker = null;

        workerID = null;
        personName = null;

        if (planning != null)
            planning.clear();
        planning = null;

        planningID = null;

        workObjectName = null;
        color = null;
    }

    /**
     * Default CTor
     */
    public LTaskChanges()
    {}

    public static LTaskChanges getNewInstance(Date startDate, Date endDate, Long duration, Long personID,
            Long planningID, ILTask task)
    {
        final LTaskChanges taskChanges = new LTaskChanges();
        taskChanges.setStartDate(startDate);
        taskChanges.setEndDate(endDate);
        taskChanges.setDuration(duration);
        taskChanges.setWorkerID(personID);
        taskChanges.setPlanningID(planningID);
        taskChanges.setTaskID(task.getId());
        taskChanges.setWorkObjectName(task.getWorkerName());
        return taskChanges;
    }

    public ILTask getTask()
    {
        return task;
    }

    public void setTask(ILTask task)
    {
        this.task = task;
    }

    public Long getTaskID()
    {
        return taskID;
    }

    public void setTaskID(Long taskID)
    {
        this.taskID = taskID;
    }

    public ILPerson getWorker()
    {
        return worker;
    }

    public void setWorker(ILPerson worker)
    {
        this.worker = worker;
    }

    public Long getWorkerID()
    {
        return workerID;
    }

    public void setWorkerID(Long personID)
    {
        this.workerID = personID;
    }

    public ILPlanning getPlanning()
    {
        return planning;
    }

    public void setPlanning(ILPlanning planning)
    {
        this.planning = planning;
    }

    public Long getPlanningID()
    {
        return planningID;
    }

    public void setPlanningID(Long planningID)
    {
        this.planningID = planningID;
    }

    public String getTaskName()
    {
        return taskName;
    }

    public void setTaskName(String taskName)
    {
        this.taskName = taskName;
    }

    public String getPersonName()
    {
        return personName;
    }

    public void setPersonName(String personName)
    {
        this.personName = personName;
    }

    public String getWorkObjectName()
    {
        return workObjectName;
    }

    public void setWorkObjectName(String workObjectName)
    {
        this.workObjectName = workObjectName;
    }

    /**
     * @return the color
     */
    public String getColor()
    {
        return color;
    }

    /**
     * @param color
     *            the color to set
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((personName == null) ? 0 : personName.hashCode());
        result = prime * result + ((planning == null) ? 0 : planning.hashCode());
        result = prime * result + ((planningID == null) ? 0 : planningID.hashCode());
        result = prime * result + ((task == null) ? 0 : task.hashCode());
        result = prime * result + ((taskID == null) ? 0 : taskID.hashCode());
        result = prime * result + ((taskName == null) ? 0 : taskName.hashCode());
        result = prime * result + ((workObjectName == null) ? 0 : workObjectName.hashCode());
        result = prime * result + ((worker == null) ? 0 : worker.hashCode());
        result = prime * result + ((workerID == null) ? 0 : workerID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTaskChanges other = (LTaskChanges) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (personName == null)
        {
            if (other.personName != null)
                return false;
        }
        else if (!personName.equals(other.personName))
            return false;
        if (planning == null)
        {
            if (other.planning != null)
                return false;
        }
        else if (!planning.equals(other.planning))
            return false;
        if (planningID == null)
        {
            if (other.planningID != null)
                return false;
        }
        else if (!planningID.equals(other.planningID))
            return false;
        if (task == null)
        {
            if (other.task != null)
                return false;
        }
        else if (!task.equals(other.task))
            return false;
        if (taskID == null)
        {
            if (other.taskID != null)
                return false;
        }
        else if (!taskID.equals(other.taskID))
            return false;
        if (taskName == null)
        {
            if (other.taskName != null)
                return false;
        }
        else if (!taskName.equals(other.taskName))
            return false;
        if (workObjectName == null)
        {
            if (other.workObjectName != null)
                return false;
        }
        else if (!workObjectName.equals(other.workObjectName))
            return false;
        if (worker == null)
        {
            if (other.worker != null)
                return false;
        }
        else if (!worker.equals(other.worker))
            return false;
        if (workerID == null)
        {
            if (other.workerID != null)
                return false;
        }
        else if (!workerID.equals(other.workerID))
            return false;
        return true;
    }

}
