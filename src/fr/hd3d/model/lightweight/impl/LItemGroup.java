package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILItemGroup;


/**
 * @author Try LAM
 */
public class LItemGroup extends LBase implements ILItemGroup
{

    private String name;
    private List<Long> items;
    private Long sheet;
    private String hook;

    public void clear()
    {
        super.clear();
        name = null;

        if (items != null)
            items.clear();
        items = null;

        sheet = null;
        hook = null;
    }

    public LItemGroup()
    {}

    public static LItemGroup getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            List<Long> items, Long sheet, String hook)
    {
        final LItemGroup itemgroup = new LItemGroup();
        itemgroup.setId(id);
        itemgroup.setVersion(version);
        itemgroup.setTags(tags);
        itemgroup.setName(name);
        itemgroup.setItems(items);
        itemgroup.setSheet(sheet);
        itemgroup.setHook(hook);
        return itemgroup;
    }

    public static LItemGroup getNewInstance(String name, List<Long> items, Long sheet, String hook)
    {
        return getNewInstance(null, null, null, name, items, sheet, hook);
    }

    public String getName()
    {
        return this.name;
    }

    public List<Long> getItems()
    {
        return items;
    }

    public Long getSheet()
    {
        return sheet;
    }

    public String getHook()
    {
        return hook;
    }

    public void setItems(List<Long> items)
    {
        this.items = items;
    }

    public void setSheet(Long sheet)
    {
        this.sheet = sheet;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((items == null) ? 0 : items.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((sheet == null) ? 0 : sheet.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LItemGroup other = (LItemGroup) obj;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (items == null)
        {
            if (other.items != null)
                return false;
        }
        else if (!items.equals(other.items))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (sheet == null)
        {
            if (other.sheet != null)
                return false;
        }
        else if (!sheet.equals(other.sheet))
            return false;
        return true;
    }

}
