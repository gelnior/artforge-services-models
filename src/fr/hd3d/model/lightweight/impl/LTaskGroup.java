package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILTaskGroup;


public class LTaskGroup extends LTaskBase implements ILTaskGroup
{
    private String color;
    private String name;
    private Long planning;
    private String filter;
    private Long taskType;
    private List<Long> extraLineIDs;

    public void clear()
    {
        super.clear();
        color = null;
        name = null;
        planning = null;
        filter = null;

        if (extraLineIDs != null)
            extraLineIDs.clear();
        extraLineIDs = null;
    }

    public LTaskGroup()
    {
        super();
    }

    public static ILTaskGroup getNewInstance(String name, String color, Long planning, Date startDate, Date endDate,
            Long duration, Long taskGroupId, String filter)
    {
        final LTaskGroup taskGroup = new LTaskGroup();
        taskGroup.setName(name);
        taskGroup.setColor(color);
        taskGroup.setPlanning(planning);
        taskGroup.setStartDate(startDate);
        taskGroup.setEndDate(endDate);
        taskGroup.setDuration(duration);
        taskGroup.setTaskGroupID(taskGroupId);
        taskGroup.setFilter(filter);
        return taskGroup;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public Long getPlanning()
    {
        return planning;
    }

    public void setPlanning(Long planning)
    {
        this.planning = planning;
    }

    public String getFilter()
    {
        return filter;
    }

    public void setFilter(String filter)
    {
        this.filter = filter;
    }

    public Long getTaskTypeId()
    {
        return taskType;
    }

    public void setTaskTypeId(Long taskType)
    {
        this.taskType = taskType;
    }

    public List<Long> getExtraLineIDs()
    {
        return extraLineIDs;
    }

    public void setExtraLineIDs(List<Long> extraLineIDs)
    {
        this.extraLineIDs = extraLineIDs;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((extraLineIDs == null) ? 0 : extraLineIDs.hashCode());
        result = prime * result + ((filter == null) ? 0 : filter.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((planning == null) ? 0 : planning.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTaskGroup other = (LTaskGroup) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (extraLineIDs == null)
        {
            if (other.extraLineIDs != null)
                return false;
        }
        else if (!extraLineIDs.equals(other.extraLineIDs))
            return false;
        if (filter == null)
        {
            if (other.filter != null)
                return false;
        }
        else if (!filter.equals(other.filter))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (planning == null)
        {
            if (other.planning != null)
                return false;
        }
        else if (!planning.equals(other.planning))
            return false;
        return true;
    }

}
