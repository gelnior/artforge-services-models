package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.lightweight.ILTaskGroup;


public abstract class LTaskBase extends LBase implements ILTaskBase
{

    private Date startDate;
    private Date endDate;
    private Long duration;
    private ILTaskGroup taskGroup;
    private Long taskGroupID;
    private ILExtraLine extraLine;
    private Long extraLineID;

    public void clear()
    {
        super.clear();
        startDate = null;
        endDate = null;
        duration = null;

        if (taskGroup != null)
            taskGroup.clear();
        taskGroup = null;

        taskGroupID = null;

        if (extraLine != null)
            extraLine.clear();
        extraLine = null;

        extraLineID = null;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Long getDuration()
    {
        return duration;
    }

    public void setDuration(Long duration)
    {
        this.duration = duration;
    }

    public ILTaskGroup getTaskGroup()
    {
        return taskGroup;
    }

    public void setTaskGroup(ILTaskGroup taskGroup)
    {
        this.taskGroup = taskGroup;
    }

    public Long getTaskGroupID()
    {
        return taskGroupID;
    }

    public void setTaskGroupID(Long taskGroupID)
    {
        this.taskGroupID = taskGroupID;
    }

    public ILExtraLine getExtraLine()
    {
        return extraLine;
    }

    public void setExtraLine(ILExtraLine extraLine)
    {
        this.extraLine = extraLine;
    }

    public Long getExtraLineID()
    {
        return extraLineID;
    }

    public void setExtraLineID(Long extraLineID)
    {
        this.extraLineID = extraLineID;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((duration == null) ? 0 : duration.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((extraLineID == null) ? 0 : extraLineID.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((taskGroupID == null) ? 0 : taskGroupID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTaskBase other = (LTaskBase) obj;
        if (duration == null)
        {
            if (other.duration != null)
                return false;
        }
        else if (!duration.equals(other.duration))
            return false;
        if (endDate == null)
        {
            if (other.endDate != null)
                return false;
        }
        else if (!endDate.equals(other.endDate))
            return false;
        if (extraLineID == null)
        {
            if (other.extraLineID != null)
                return false;
        }
        else if (!extraLineID.equals(other.extraLineID))
            return false;
        if (startDate == null)
        {
            if (other.startDate != null)
                return false;
        }
        else if (!startDate.equals(other.startDate))
            return false;
        if (taskGroupID == null)
        {
            if (other.taskGroupID != null)
                return false;
        }
        else if (!taskGroupID.equals(other.taskGroupID))
            return false;
        return true;
    }

}
