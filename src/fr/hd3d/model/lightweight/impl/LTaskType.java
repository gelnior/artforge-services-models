/**
 * 
 */
package fr.hd3d.model.lightweight.impl;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTaskType;


/**
 * Lightweight representation of task type aimed to be converted as json.
 * 
 * @author HD3D
 */
public class LTaskType extends LBase implements ILTaskType
{
    ILProject project;
    Long projectID;
    String color;
    String name;
    String description;
    String entityName;

    public void clear()
    {
        super.clear();

        if (project != null)
            project.clear();
        project = null;

        projectID = null;
        color = null;
        name = null;
        description = null;
    }

    /**
     * @return the project
     */
    public ILProject getProject()
    {
        return project;
    }

    /**
     * @param project
     *            the project to set
     */
    public void setProject(ILProject project)
    {
        this.project = project;
    }

    /**
     * @return the projectID
     */
    public Long getProjectID()
    {
        return projectID;
    }

    /**
     * @param projectID
     *            the projectID to set
     */
    public void setProjectID(Long projectID)
    {
        this.projectID = projectID;
    }

    /**
     * @return the color
     */
    public String getColor()
    {
        return color;
    }

    /**
     * @param color
     *            the color to set
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return Entity to which task type is linked.
     */
    public String getEntityName()
    {
        return entityName;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Set entity to which task type is linked.
     */
    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((projectID == null) ? 0 : projectID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTaskType other = (LTaskType) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (projectID == null)
        {
            if (other.projectID != null)
                return false;
        }
        else if (!projectID.equals(other.projectID))
            return false;
        return true;
    }

    public String toString()
    {
        return name;
    }
}
