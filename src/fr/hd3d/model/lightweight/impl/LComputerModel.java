package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILComputerModel;


/**
 * Lightweight implementation for computer model object.
 * 
 * @author HD3D
 */
public class LComputerModel extends LModel implements ILComputerModel
{
    /**
     * Default Constructor.
     */
    public LComputerModel()
    {}

    public LComputerModel(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILComputerModel getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LComputerModel computerModel = new LComputerModel(id, version, tags, name);

        return computerModel;
    }

    public static ILComputerModel getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
