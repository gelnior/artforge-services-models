/**
 * 
 */
package fr.hd3d.model.lightweight.impl;

import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.lightweight.ILPerson;


/**
 * @author thomas-eskenazi
 * 
 */
public class LAbsence extends LDurableImpl implements ILAbsence
{
    private ILPerson worker;
    private Long workerID;
    private String comment;
    private String type;

    public void clear()
    {
        super.clear();
        if (worker != null)
            worker.clear();
        worker = null;
        workerID = null;
        comment = null;
        type = null;
    }

    /**
     * @return the worker
     */
    public ILPerson getWorker()
    {
        return worker;
    }

    /**
     * @param worker
     *            the worker to set
     */
    public void setWorker(ILPerson worker)
    {
        this.worker = worker;
    }

    /**
     * @return the workerID
     */
    public Long getWorkerID()
    {
        return workerID;
    }

    /**
     * @param workerID
     *            the workerID to set
     */
    public void setWorkerID(Long workerID)
    {
        this.workerID = workerID;
    }

    /**
     * @return the comment
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((worker == null) ? 0 : worker.hashCode());
        result = prime * result + ((workerID == null) ? 0 : workerID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LAbsence other = (LAbsence) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        if (worker == null)
        {
            if (other.worker != null)
                return false;
        }
        else if (!worker.equals(other.worker))
            return false;
        if (workerID == null)
        {
            if (other.workerID != null)
                return false;
        }
        else if (!workerID.equals(other.workerID))
            return false;
        return true;
    }

}
