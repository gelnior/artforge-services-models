package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILComputerType;


/**
 * Lightweight implementation for computerType object.
 * 
 * @author HD3D
 */
public class LComputerType extends LSimple implements ILComputerType
{
    /**
     * Default Constructor.
     */
    public LComputerType()
    {}

    public LComputerType(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILComputerType getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LComputerType computerType = new LComputerType(id, version, tags, name);

        return computerType;
    }

    public static ILComputerType getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
