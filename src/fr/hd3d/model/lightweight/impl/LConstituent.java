package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILConstituent;


/**
 * @author Try LAM
 */
public class LConstituent extends LBase implements ILConstituent
{

    private Long category;
    private String categoryName;
    private String name;
    private Integer completion;
    private String description;
    private Integer difficulty;
    private String hook;
    private String label;
    private Integer trust;
    private Boolean design;
    private Boolean modeling;
    private Boolean setup;
    private Boolean texturing;
    private Boolean shading;
    private Boolean hairs;
    private String categoriesNames;

    public void clear()
    {
        super.clear();

        category = null;
        categoryName = null;
        name = null;
        completion = null;
        description = null;
        difficulty = null;
        hook = null;
        label = null;
        trust = null;
        design = null;
        modeling = null;
        setup = null;
        texturing = null;
        shading = null;
        hairs = null;
        categoriesNames = null;
    }

    public LConstituent()
    {}

    public static ILConstituent getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            List<String> tagNames, Long categoryID, String categoryName, Integer completion, String description,
            Integer difficulty, String hook, String label, Integer trust, Boolean design, Boolean modeling,
            Boolean setup, Boolean texturing, Boolean shading, Boolean hairs, String categoriesNames)
    {
        final ILConstituent constituent = new LConstituent();
        constituent.setId(id);
        constituent.setVersion(version);
        constituent.setTags(tags);
        constituent.setTagNames(tagNames);
        constituent.setCategory(categoryID);
        constituent.setCategoryName(categoryName);
        if (completion != null)
            constituent.setCompletion(completion);
        constituent.setDescription(description);
        if (difficulty != null)
            constituent.setDifficulty(difficulty);
        constituent.setHook(hook);
        constituent.setLabel(label);
        if (trust != null)
            constituent.setTrust(trust);
        if (design != null)
            constituent.setDesign(design);
        if (modeling != null)
            constituent.setModeling(modeling);
        if (setup != null)
            constituent.setSetup(setup);
        if (texturing != null)
            constituent.setTexturing(texturing);
        if (shading != null)
            constituent.setShading(shading);
        if (hairs != null)
            constituent.setHairs(hairs);
        if (categoriesNames != null)
            constituent.setCategoriesNames(categoriesNames);
        return constituent;
    }

    public static ILConstituent getNewInstance(Long categoryID, String categoryName, Integer completion,
            String description, Integer difficulty, String hook, String label, Integer trust, Boolean design,
            Boolean modeling, Boolean setup, Boolean texturing, Boolean shading, Boolean hairs, String categoriesNames)
    {
        return getNewInstance(null, null, null, null, categoryID, categoryName, completion, description, difficulty,
                hook, label, trust, design, modeling, setup, texturing, shading, hairs, categoriesNames);
    }

    public Long getCategory()
    {
        return category;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public Integer getCompletion()
    {
        return completion;
    }

    public String getDescription()
    {
        return description;
    }

    public Integer getDifficulty()
    {
        return difficulty;
    }

    public String getHook()
    {
        return hook;
    }

    public String getLabel()
    {
        return label;
    }

    public Integer getTrust()
    {
        return trust;
    }

    public Boolean getDesign()
    {
        return design;
    }

    public Boolean getModeling()
    {
        return modeling;
    }

    public Boolean getSetup()
    {
        return setup;
    }

    public Boolean getTexturing()
    {
        return texturing;
    }

    public Boolean getShading()
    {
        return shading;
    }

    public Boolean getHairs()
    {
        return hairs;
    }

    public String getCategoriesNames()
    {
        return categoriesNames;
    }

    public void setCategory(Long categoryId)
    {
        this.category = categoryId;
    }

    public void setCategoryName(String name)
    {
        this.categoryName = name;
    }

    public void setCompletion(Integer completion)
    {
        this.completion = completion;
    }

    public void setDescription(String desc)
    {
        this.description = desc;
    }

    public void setDifficulty(Integer difficulty)
    {
        this.difficulty = difficulty;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setLabel(String label)
    {
        this.label = label;
        this.name = label;
    }

    public void setTrust(Integer trust)
    {
        this.trust = trust;
    }

    public void setDesign(Boolean design)
    {
        this.design = design;
    }

    public void setModeling(Boolean modeling)
    {
        this.modeling = modeling;
    }

    public void setSetup(Boolean setup)
    {
        this.setup = setup;
    }

    public void setTexturing(Boolean texturing)
    {
        this.texturing = texturing;
    }

    public void setShading(Boolean shading)
    {
        this.shading = shading;
    }

    public void setHairs(Boolean hairs)
    {
        this.hairs = hairs;
    }

    public void setCategoriesNames(String categoriesNames)
    {
        this.categoriesNames = categoriesNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((category == null) ? 0 : category.hashCode());
        result = prime * result + completion;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + difficulty;
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + trust;
        // result = prime * result + step;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LConstituent other = (LConstituent) obj;
        if (category == null)
        {
            if (other.category != null)
                return false;
        }
        else if (!category.equals(other.category))
            return false;
        if (completion != other.completion)
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (difficulty != other.difficulty)
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (label == null)
        {
            if (other.label != null)
                return false;
        }
        else if (!label.equals(other.label))
            return false;
        if (trust != other.trust)
            return false;
        if (design != other.design)
            return false;
        if (modeling != other.modeling)
            return false;
        if (setup != other.setup)
            return false;
        if (texturing != other.texturing)
            return false;
        if (shading != other.shading)
            return false;
        if (hairs != other.hairs)
            return false;
        return true;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

}
