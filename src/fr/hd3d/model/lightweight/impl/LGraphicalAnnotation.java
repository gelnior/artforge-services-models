package fr.hd3d.model.lightweight.impl;

import java.util.Date;

import fr.hd3d.model.lightweight.ILGraphicalAnnotation;


public class LGraphicalAnnotation extends LBase implements ILGraphicalAnnotation
{

    private String annotationSvg;
    private Long markIn;
    private Long markOut;
    private String comment;
    private Long author;
    private Long proxy;
    private Long approvalNote;
    private Date date;

    public void clear()
    {
        super.clear();
        annotationSvg = null;
        markIn = null;
        markOut = null;
        comment = null;
        author = null;
        proxy = null;
        approvalNote = null;
        date = null;
    }

    public LGraphicalAnnotation()
    {
        super();
    }

    public String getAnnotationSvg()
    {
        return annotationSvg;
    }

    public Long getMarkIn()
    {
        return markIn;
    }

    public Long getMarkOut()
    {
        return markOut;
    }

    public String getComment()
    {
        return comment;
    }

    public Long getAuthor()
    {
        return author;
    }

    public Long getProxy()
    {
        return proxy;
    }

    public Long getApprovalNote()
    {
        return approvalNote;
    }

    public Date getDate()
    {
        return date;
    }

    public void setAnnotationSvg(String annotationSvg)
    {
        this.annotationSvg = annotationSvg;
    }

    public void setMarkIn(Long markIn)
    {
        this.markIn = markIn;
    }

    public void setMarkOut(Long markOut)
    {
        this.markOut = markOut;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setAuthor(Long author)
    {
        this.author = author;
    }

    public void setProxy(Long proxy)
    {
        this.proxy = proxy;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setApprovalNote(Long approvalNote)
    {
        this.approvalNote = approvalNote;
    }

}
