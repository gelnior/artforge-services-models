package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILTagCategory;


public class LTagCategory extends LBase implements ILTagCategory
{
    private String name;
    private Long parent;
    private List<Long> children;
    private List<Long> boundTags;

    public void clear()
    {
        super.clear();
        name = null;
        parent = null;

        if (children != null)
            children.clear();
        children = null;

        if (boundTags != null)
            boundTags.clear();
        boundTags = null;
    }

    public LTagCategory()
    {}

    public static ILTagCategory getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            Long parent, List<Long> children, List<Long> boundTags)
    {
        final ILTagCategory tagCategory = new LTagCategory();
        tagCategory.setId(id);
        tagCategory.setVersion(version);
        tagCategory.setTags(tags);
        tagCategory.setName(name);
        tagCategory.setParent(parent);
        tagCategory.setChildren(children);
        tagCategory.setBoundTags(boundTags);
        return tagCategory;
    }

    public static ILTagCategory getNewInstance(String name, Long parent, List<Long> children, List<Long> boundTags)
    {
        return getNewInstance(null, null, null, name, parent, children, boundTags);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getParent()
    {
        return parent;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public List<Long> getChildren()
    {
        return children;
    }

    public void setChildren(List<Long> children)
    {
        this.children = children;
    }

    public List<Long> getBoundTags()
    {
        return boundTags;
    }

    public void setBoundTags(List<Long> boundTags)
    {
        this.boundTags = boundTags;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundTags == null) ? 0 : boundTags.hashCode());
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LTagCategory other = (LTagCategory) obj;
        if (boundTags == null)
        {
            if (other.boundTags != null)
                return false;
        }
        else if (!boundTags.equals(other.boundTags))
            return false;
        if (children == null)
        {
            if (other.children != null)
                return false;
        }
        else if (!children.equals(other.children))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        return true;
    }

}
