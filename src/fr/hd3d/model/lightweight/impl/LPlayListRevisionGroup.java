package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;


public class LPlayListRevisionGroup extends LBase implements ILPlayListRevisionGroup
{

    private String name;
    private Long project;
    private Long parent;
    private Collection<Long> children;
    private List<Long> playListRevisions;

    public void clear()
    {
        super.clear();
        name = null;
        parent = null;
        if (children != null)
            children.clear();
        children = null;
        if (playListRevisions != null)
            playListRevisions.clear();
        playListRevisions = null;
        project = null;
    }

    public LPlayListRevisionGroup()
    {
        super();
    }

    public LPlayListRevisionGroup(Long id, Timestamp version, String name, Long project, Long parent,
            Collection<Long> children, List<Long> playListRevisions)
    {
        super(id, version);
        this.name = name;
        this.project = project;
        this.parent = parent;
        this.children = children;
        this.playListRevisions = playListRevisions;
    }

    public String getName()
    {
        return name;
    }

    public Long getProject()
    {
        return project;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public Long getParent()
    {
        return parent;
    }

    public Collection<Long> getChildren()
    {
        return children;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setChildren(Collection<Long> children)
    {
        this.children = children;
    }

    public List<Long> getPlayListRevisions()
    {
        return playListRevisions;
    }

    public void setPlayListRevisions(List<Long> playListRevisions)
    {
        this.playListRevisions = playListRevisions;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((playListRevisions == null) ? 0 : playListRevisions.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPlayListRevisionGroup other = (LPlayListRevisionGroup) obj;
        if (children == null)
        {
            if (other.children != null)
                return false;
        }
        else if (!children.equals(other.children))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (playListRevisions == null)
        {
            if (other.playListRevisions != null)
                return false;
        }
        else if (!playListRevisions.equals(other.playListRevisions))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        return true;
    }

}
