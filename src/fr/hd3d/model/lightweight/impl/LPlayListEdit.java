package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILPlayListEdit;


public class LPlayListEdit extends LBase implements ILPlayListEdit
{
    private Integer srcIn;
    private Integer srcOut;
    private Integer recIn;
    private Integer recOut;
    // private Long fileRevision;
    private Long proxy;
    private Long playListRevision;
    private String name;

    /*------------------
     * clear
     ------------------*/
    public void clear()
    {
        super.clear();
        srcIn = null;
        srcOut = null;
        recIn = null;
        recOut = null;
        proxy = null;
        playListRevision = null;
        name = null;
    }

    /*-------------
     * Factory
     -------------*/
    public LPlayListEdit()
    {}

    public static LPlayListEdit getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Integer srcIn,
            Integer srcOut, Integer recIn, Integer recOut, Long proxy, Long playListRevision, String name)
    {
        final LPlayListEdit playListEdit = new LPlayListEdit();
        playListEdit.setId(id);
        playListEdit.setVersion(version);
        playListEdit.setTags(tags);
        playListEdit.setSrcIn(srcIn);
        playListEdit.setSrcOut(srcOut);
        playListEdit.setRecIn(recIn);
        playListEdit.setRecOut(recOut);
        playListEdit.setProxy(proxy);
        playListEdit.setPlayListRevision(playListRevision);
        playListEdit.setName(name);

        return playListEdit;
    }

    public static LPlayListEdit getNewInstance(Integer srcIn, Integer srcOut, Integer recIn, Integer recOut,
            Long proxy, Long playListRevision, String name)
    {
        return getNewInstance(null, null, null, srcIn, srcOut, recIn, recOut, proxy, playListRevision, name);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    public Integer getSrcIn()
    {
        return srcIn;
    }

    public Integer getSrcOut()
    {
        return srcOut;
    }

    public Integer getRecIn()
    {
        return recIn;
    }

    public Integer getRecOut()
    {
        return recOut;
    }

    public Long getProxy()
    {
        return proxy;
    }

    public Long getPlayListRevision()
    {
        return playListRevision;
    }

    public String getName()
    {
        return name;
    }

    public void setSrcIn(Integer srcIn)
    {
        this.srcIn = srcIn;
    }

    public void setSrcOut(Integer srcOut)
    {
        this.srcOut = srcOut;
    }

    public void setRecIn(Integer recIn)
    {
        this.recIn = recIn;
    }

    public void setRecOut(Integer recOut)
    {
        this.recOut = recOut;
    }

    public void setProxy(Long proxy)
    {
        this.proxy = proxy;
    }

    public void setPlayListRevision(Long playListRevision)
    {
        this.playListRevision = playListRevision;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((playListRevision == null) ? 0 : playListRevision.hashCode());
        result = prime * result + ((recIn == null) ? 0 : recIn.hashCode());
        result = prime * result + ((recOut == null) ? 0 : recOut.hashCode());
        result = prime * result + ((srcIn == null) ? 0 : srcIn.hashCode());
        result = prime * result + ((srcOut == null) ? 0 : srcOut.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPlayListEdit other = (LPlayListEdit) obj;
        if (proxy == null)
        {
            if (other.proxy != null)
                return false;
        }
        else if (!proxy.equals(other.proxy))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (playListRevision == null)
        {
            if (other.playListRevision != null)
                return false;
        }
        else if (!playListRevision.equals(other.playListRevision))
            return false;
        if (recIn == null)
        {
            if (other.recIn != null)
                return false;
        }
        else if (!recIn.equals(other.recIn))
            return false;
        if (recOut == null)
        {
            if (other.recOut != null)
                return false;
        }
        else if (!recOut.equals(other.recOut))
            return false;
        if (srcIn == null)
        {
            if (other.srcIn != null)
                return false;
        }
        else if (!srcIn.equals(other.srcIn))
            return false;
        if (srcOut == null)
        {
            if (other.srcOut != null)
                return false;
        }
        else if (!srcOut.equals(other.srcOut))
            return false;
        return true;
    }

}
