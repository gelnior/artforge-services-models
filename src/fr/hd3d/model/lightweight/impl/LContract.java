package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.Date;

import fr.hd3d.model.lightweight.ILContract;


/**
 * Lightweight implementation for computer object.
 * 
 * @author HD3D
 */
public class LContract extends LSimple implements ILContract
{
    private Date startDate;
    private Date endDate;
    private Date realEndDate;
    private String docPath;
    private String type;

    private Long personId;
    private Long qualificationId;
    private Long organizationId;
    private String personName;
    private String qualificationName;
    private String organizationName;

    private Float salary;

    public void clear()
    {
        super.clear();

        startDate = null;
        endDate = null;
        realEndDate = null;
        docPath = null;
        type = null;

        personId = null;
        qualificationId = null;
        organizationId = null;
        personName = null;
        qualificationName = null;
        organizationName = null;

        salary = null;
    }

    /**
     * Default Constructor.
     */
    public LContract()
    {}

    public LContract(Long id, Timestamp version, String name, Date startDate, Date endDate, Date realEndDate,
            String docPath, String type, Float salary)
    {
        super(id, version, null, name);

        this.startDate = startDate;
        this.endDate = endDate;
        this.realEndDate = realEndDate;
        this.docPath = docPath;
        this.type = type;
        this.salary = salary;
    }

    public static ILContract getNewInstance(Long id, Timestamp version, String name, Date startDate, Date endDate,
            Date realEndDate, String docPath, String type, Float salary)
    {
        final LContract contract = new LContract(id, version, name, startDate, endDate, realEndDate, docPath, type,
                salary);

        return contract;
    }

    public static ILContract getNewInstance(String name, Date startDate, Date endDate, Date realEndDate,
            String docPath, String type, Float salary)
    {
        return getNewInstance(null, null, name, startDate, endDate, realEndDate, docPath, type, salary);
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Date getRealEndDate()
    {
        return realEndDate;
    }

    public void setRealEndDate(Date realEndDate)
    {
        this.realEndDate = realEndDate;
    }

    public String getDocPath()
    {
        return docPath;
    }

    public void setDocPath(String docPath)
    {
        this.docPath = docPath;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Long getPersonId()
    {
        return personId;
    }

    public void setPersonId(Long personId)
    {
        this.personId = personId;
    }

    public Long getQualificationId()
    {
        return qualificationId;
    }

    public void setQualificationId(Long qualificationId)
    {
        this.qualificationId = qualificationId;
    }

    public Long getOrganizationId()
    {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId)
    {
        this.organizationId = organizationId;
    }

    public String getPersonName()
    {
        return personName;
    }

    public void setPersonName(String personName)
    {
        this.personName = personName;
    }

    public String getQualificationName()
    {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName)
    {
        this.qualificationName = qualificationName;
    }

    public String getOrganizationName()
    {
        return organizationName;
    }

    public void setOrganizationName(String organizationName)
    {
        this.organizationName = organizationName;
    }

    public Float getSalary()
    {
        return salary;
    }

    public void setSalary(Float salary)
    {
        this.salary = salary;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((docPath == null) ? 0 : docPath.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        result = prime * result + ((organizationName == null) ? 0 : organizationName.hashCode());
        result = prime * result + ((personId == null) ? 0 : personId.hashCode());
        result = prime * result + ((personName == null) ? 0 : personName.hashCode());
        result = prime * result + ((qualificationId == null) ? 0 : qualificationId.hashCode());
        result = prime * result + ((qualificationName == null) ? 0 : qualificationName.hashCode());
        result = prime * result + ((realEndDate == null) ? 0 : realEndDate.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((salary == null) ? 0 : salary.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LContract other = (LContract) obj;
        if (docPath == null)
        {
            if (other.docPath != null)
                return false;
        }
        else if (!docPath.equals(other.docPath))
            return false;
        if (endDate == null)
        {
            if (other.endDate != null)
                return false;
        }
        else if (!endDate.equals(other.endDate))
            return false;
        if (organizationId == null)
        {
            if (other.organizationId != null)
                return false;
        }
        else if (!organizationId.equals(other.organizationId))
            return false;
        if (organizationName == null)
        {
            if (other.organizationName != null)
                return false;
        }
        else if (!organizationName.equals(other.organizationName))
            return false;
        if (personId == null)
        {
            if (other.personId != null)
                return false;
        }
        else if (!personId.equals(other.personId))
            return false;
        if (personName == null)
        {
            if (other.personName != null)
                return false;
        }
        else if (!personName.equals(other.personName))
            return false;
        if (qualificationId == null)
        {
            if (other.qualificationId != null)
                return false;
        }
        else if (!qualificationId.equals(other.qualificationId))
            return false;
        if (qualificationName == null)
        {
            if (other.qualificationName != null)
                return false;
        }
        else if (!qualificationName.equals(other.qualificationName))
            return false;
        if (realEndDate == null)
        {
            if (other.realEndDate != null)
                return false;
        }
        else if (!realEndDate.equals(other.realEndDate))
            return false;
        if (startDate == null)
        {
            if (other.startDate != null)
                return false;
        }
        else if (!startDate.equals(other.startDate))
            return false;
        if (salary == null)
        {
            if (other.salary != null)
                return false;
        }
        else if (!salary.equals(other.salary))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
