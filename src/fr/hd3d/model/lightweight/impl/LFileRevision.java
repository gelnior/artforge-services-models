package fr.hd3d.model.lightweight.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.ILFileRevision;


public class LFileRevision extends LBase implements ILFileRevision
{
    private String uriScheme;// file,http...
    private Date creationDate;
    private String key;
    private String variation;
    private Integer revision;
    private Long creator;
    private String creatorName;
    private String format;
    private Long size;
    private String checksum;
    private String location;
    private String relativePath;
    private String status;
    private Date lastOperationDate;
    private Long lastUser;
    private String lastUserName;
    private String comment;
    private String validity;// Enum ?
    // Sequence: null if standalone file; (start, step, stop) if sequence file
    private String sequence;

    private String state;

    private String mountPoint;

    private Long assetRevision;

    private Set<Long> attributes;

    private Set<Long> proxies;

    private String taskTypeName;

    public void clear()
    {
        super.clear();

        uriScheme = null;
        creationDate = null;
        key = null;
        variation = null;
        revision = null;
        creator = null;
        format = null;
        size = null;
        checksum = null;
        location = null;
        relativePath = null;
        status = null;
        lastOperationDate = null;
        lastUser = null;
        comment = null;
        validity = null;
        sequence = null;

        assetRevision = null;
        state = null;

        if (attributes != null)
            attributes.clear();
        attributes = null;

        if (proxies != null)
            proxies.clear();
        proxies = null;

        mountPoint = null;

    }

    public LFileRevision()
    {}

    public static ILFileRevision getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            Date creationDate, String key, String variation, Integer revision, Long creator, String format, Long size,
            String checksum, String location, String relativePath, String status, Date lastOperationDate,
            Long lastUser, String comment, String validity, String sequence, Long assetRevision, String state,
            String mountPoint)
    {
        ILFileRevision file = new LFileRevision();
        file.setId(id);
        file.setVersion(version);
        file.setTags(tags);
        file.setCreationDate(creationDate);
        file.setKey(key);
        file.setVariation(variation);
        file.setRevision(revision);
        file.setCreator(creator);
        file.setFormat(format);
        file.setSize(size);
        file.setChecksum(checksum);
        file.setLocation(location);
        file.setRelativePath(relativePath);
        file.setStatus(status);
        file.setLastOperationDate(lastOperationDate);
        file.setLastUser(lastUser);
        file.setComment(comment);
        file.setValidity(validity);
        file.setSequence(sequence);
        file.setAssetRevision(assetRevision);
        file.setState(state);
        file.setMountPoint(mountPoint);
        return file;
    }

    public static ILFileRevision getNewInstance(Date creationDate, String key, String variation, Integer revision,
            Long creator, String format, Long size, String checksum, String location, String relativePath,
            String status, Date lastOperationDate, Long lastUser, String comment, String validity, String sequence,
            Long assetRevision, String state, String mountPoint)
    {
        return getNewInstance(null, null, null, creationDate, key, variation, revision, creator, format, size,
                checksum, location, relativePath, status, lastOperationDate, lastUser, comment, validity, sequence,
                assetRevision, state, mountPoint);
    }

    public String getUriScheme()
    {
        return uriScheme;
    }

    public void setUriScheme(String uriScheme)
    {
        this.uriScheme = uriScheme;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public String getKey()
    {
        return key;
    }

    public String getVariation()
    {
        return variation;
    }

    public Integer getRevision()
    {
        return revision;
    }

    public Long getCreator()
    {
        return creator;
    }

    public String getCreatorName()
    {
        return this.creatorName;
    }

    public String getFormat()
    {
        return format;
    }

    public Long getSize()
    {
        return size;
    }

    public String getChecksum()
    {
        return checksum;
    }

    public String getLocation()
    {
        return location;
    }

    public String getRelativePath()
    {
        return relativePath;
    }

    public String getStatus()
    {
        return status;
    }

    public Date getLastOperationDate()
    {
        return lastOperationDate;
    }

    public Long getLastUser()
    {
        return lastUser;
    }

    public String getLastUserName()
    {
        return this.lastUserName;
    }

    public String getComment()
    {
        return comment;
    }

    public String getValidity()
    {
        return validity;
    }

    public String getSequence()
    {
        return sequence;
    }

    public Set<Long> getAttributes()
    {
        return attributes;
    }

    public String getFullPath()
    {
        return getLocation() + getRelativePath();
    }

    public String getState()
    {
        return state;
    }

    public Set<Long> getProxies()
    {
        return proxies;
    }

    public Long getAssetRevision()
    {
        return assetRevision;
    }

    public void setAssetRevision(Long assetRevision)
    {
        this.assetRevision = assetRevision;
    }

    public String getMountPoint()
    {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint)
    {
        this.mountPoint = mountPoint;
    }

    public void setProxies(Set<Long> proxies)
    {
        this.proxies = proxies;
    }

    public void setAttributes(Set<Long> attributes)
    {
        this.attributes = attributes;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setCreator(Long creator)
    {
        this.creator = creator;
    }

    public void setCreatorName(String creatorName)
    {
        this.creatorName = creatorName;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setRelativePath(String relativePath)
    {
        this.relativePath = relativePath;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setLastOperationDate(Date lastOperationDate)
    {
        this.lastOperationDate = lastOperationDate;
    }

    public void setLastUser(Long lastUser)
    {
        this.lastUser = lastUser;
    }

    public void setLastUserName(String lastUsername)
    {
        this.lastUserName = lastUsername;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setValidity(String validity)
    {
        this.validity = validity;
    }

    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((attributes == null) ? 0 : attributes.hashCode());
        result = prime * result + ((checksum == null) ? 0 : checksum.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((format == null) ? 0 : format.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((lastOperationDate == null) ? 0 : lastOperationDate.hashCode());
        result = prime * result + ((lastUser == null) ? 0 : lastUser.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((relativePath == null) ? 0 : relativePath.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((variation == null) ? 0 : variation.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((assetRevision == null) ? 0 : assetRevision.hashCode());
        result = prime * result + ((mountPoint == null) ? 0 : mountPoint.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFileRevision other = (LFileRevision) obj;
        if (attributes == null)
        {
            if (other.attributes != null)
                return false;
        }
        else if (!attributes.equals(other.attributes))
            return false;
        if (checksum == null)
        {
            if (other.checksum != null)
                return false;
        }
        else if (!checksum.equals(other.checksum))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (creationDate == null)
        {
            if (other.creationDate != null)
                return false;
        }
        else if (!creationDate.equals(other.creationDate))
            return false;
        if (creator == null)
        {
            if (other.creator != null)
                return false;
        }
        else if (!creator.equals(other.creator))
            return false;
        if (format == null)
        {
            if (other.format != null)
                return false;
        }
        else if (!format.equals(other.format))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (lastOperationDate == null)
        {
            if (other.lastOperationDate != null)
                return false;
        }
        else if (!lastOperationDate.equals(other.lastOperationDate))
            return false;
        if (lastUser == null)
        {
            if (other.lastUser != null)
                return false;
        }
        else if (!lastUser.equals(other.lastUser))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (relativePath == null)
        {
            if (other.relativePath != null)
                return false;
        }
        else if (!relativePath.equals(other.relativePath))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (sequence == null)
        {
            if (other.sequence != null)
                return false;
        }
        else if (!sequence.equals(other.sequence))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (validity == null)
        {
            if (other.validity != null)
                return false;
        }
        else if (!validity.equals(other.validity))
            return false;
        if (variation == null)
        {
            if (other.variation != null)
                return false;
        }
        else if (!variation.equals(other.variation))
            return false;
        if (assetRevision == null)
        {
            if (other.assetRevision != null)
                return false;
        }
        else if (!assetRevision.equals(other.assetRevision))
            return false;
        if (state == null)
        {
            if (other.state != null)
                return false;
        }
        else if (!state.equals(other.state))
            return false;
        if (mountPoint == null)
        {
            if (other.mountPoint != null)
                return false;
        }
        else if (!mountPoint.equals(other.mountPoint))
            return false;
        return true;
    }

}
