package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILAssetRevisionGroup;


public class LAssetRevisionGroup extends LBase implements ILAssetRevisionGroup
{
    private String name;
    private List<Long> assetRevisions;
    private String criteria;
    private String value;
    private Long project;

    public void clear()
    {
        super.clear();
        name = null;

        if (assetRevisions != null)
            assetRevisions.clear();
        assetRevisions = null;

        criteria = null;
        value = null;
        project = null;
    }

    public LAssetRevisionGroup()
    {
        super();
    }

    public static ILAssetRevisionGroup getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            String name, List<Long> assetRevisions, String criteria, String value, Long project)
    {
        ILAssetRevisionGroup assetgroup = new LAssetRevisionGroup();
        assetgroup.setId(id);
        assetgroup.setVersion(version);
        assetgroup.setTags(tags);
        assetgroup.setName(name);
        assetgroup.setAssetRevisions(assetRevisions);
        assetgroup.setCriteria(criteria);
        assetgroup.setValue(value);
        assetgroup.setProject(project);
        return assetgroup;
    }

    public static ILAssetRevisionGroup getNewInstance(String name, List<Long> assetRevisions, String criteria,
            String value, Long project)
    {
        return getNewInstance(null, null, null, name, assetRevisions, criteria, value, project);
    }

    public String getName()
    {
        return name;
    }

    public List<Long> getAssetRevisions()
    {
        return assetRevisions;
    }

    public String getCriteria()
    {
        return criteria;
    }

    public String getValue()
    {
        return value;
    }

    public Long getProject()
    {
        return project;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAssetRevisions(List<Long> assetRevisions)
    {
        this.assetRevisions = assetRevisions;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((criteria == null) ? 0 : criteria.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LAssetRevisionGroup other = (LAssetRevisionGroup) obj;
        if (criteria == null)
        {
            if (other.criteria != null)
                return false;
        }
        else if (!criteria.equals(other.criteria))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }

}
