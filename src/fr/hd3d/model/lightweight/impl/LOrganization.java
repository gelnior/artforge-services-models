package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILOrganization;


/**
 * Lightweight implementation for organization object.
 * 
 * @author HD3D
 */
public class LOrganization extends LSimple implements ILOrganization
{
    private List<Long> resourceGroupIds;
    private List<String> resourceGroupNames;
    private Long parent;

    public void clear()
    {
        super.clear();

        if (resourceGroupIds != null)
            resourceGroupIds.clear();
        resourceGroupIds = null;

        if (resourceGroupNames != null)
            resourceGroupNames.clear();
        resourceGroupNames = null;

        parent = null;
    }

    public LOrganization()
    {
        super();
    }

    public LOrganization(Long id, Timestamp version, String name, Long parent)
    {
        super(id, version, null, name);

        this.parent = parent;
    }

    public static ILOrganization getNewInstance(Long id, Timestamp version, String name, Long parent)
    {
        final LOrganization group = new LOrganization(id, version, name, parent);

        return group;
    }

    public static ILOrganization getNewInstance(String name, Long parent)
    {
        return getNewInstance(null, null, name, parent);
    }

    public Long getParent()
    {
        return parent;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public List<Long> getResourceGroupIds()
    {
        return this.resourceGroupIds;
    }

    public List<String> getResourceGroupNames()
    {
        return this.resourceGroupNames;
    }

    public void addResourceGroupID(Long id)
    {
        if (getResourceGroupIds() == null)
            setResourceGroupIds(new ArrayList<Long>());
        getResourceGroupIds().add(id);
    }

    public void addResourceGroupName(String name)
    {
        if (getResourceGroupNames() == null)
            setResourceGroupNames(new ArrayList<String>());
        getResourceGroupNames().add(name);
    }

    public void setResourceGroupIds(List<Long> resourceGroupIds)
    {
        this.resourceGroupIds = resourceGroupIds;
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.resourceGroupNames = resourceGroupNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((resourceGroupIds == null) ? 0 : resourceGroupIds.hashCode());
        result = prime * result + ((resourceGroupNames == null) ? 0 : resourceGroupNames.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LOrganization other = (LOrganization) obj;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (resourceGroupIds == null)
        {
            if (other.resourceGroupIds != null)
                return false;
        }
        else if (!resourceGroupIds.equals(other.resourceGroupIds))
            return false;
        if (resourceGroupNames == null)
        {
            if (other.resourceGroupNames != null)
                return false;
        }
        else if (!resourceGroupNames.equals(other.resourceGroupNames))
            return false;
        return true;
    }

}
