package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILAssetRevisionFileRevision;


public class LAssetRevisionFileRevision extends LBase implements ILAssetRevisionFileRevision
{

    private String assetKey;
    private String fileKey;
    private String assetVariation;
    private String fileVariation;
    private Integer assetRevision;
    private Integer fileRevision;

    public void clear()
    {
        super.clear();
        assetKey = null;
        fileKey = null;
        assetVariation = null;
        fileVariation = null;
        assetRevision = null;
        fileRevision = null;
    }

    public LAssetRevisionFileRevision()
    {
        super();
    }

    public static ILAssetRevisionFileRevision getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            String assetKey, String fileKey, String assetVariation, String fileVariation, Integer assetRevision,
            Integer fileRevision)
    {
        ILAssetRevisionFileRevision assetfile = new LAssetRevisionFileRevision();
        assetfile.setId(id);
        assetfile.setVersion(version);
        assetfile.setTags(tags);
        assetfile.setAssetKey(assetKey);
        assetfile.setFileKey(fileKey);
        assetfile.setAssetVariation(assetVariation);
        assetfile.setFileVariation(fileVariation);
        assetfile.setAssetRevision(assetRevision);
        assetfile.setFileRevision(fileRevision);
        return assetfile;
    }

    public static ILAssetRevisionFileRevision getNewInstance(String assetKey, String fileKey, String assetVariation,
            String fileVariation, Integer assetRevision, Integer fileRevision)
    {
        return getNewInstance(null, null, null, assetKey, fileKey, assetVariation, fileVariation, assetRevision,
                fileRevision);
    }

    public String getAssetKey()
    {
        return assetKey;
    }

    public String getFileKey()
    {
        return fileKey;
    }

    public String getAssetVariation()
    {
        return assetVariation;
    }

    public String getFileVariation()
    {
        return fileVariation;
    }

    public Integer getAssetRevision()
    {
        return assetRevision;
    }

    public Integer getFileRevision()
    {
        return fileRevision;
    }

    public void setAssetKey(String assetKey)
    {
        this.assetKey = assetKey;
    }

    public void setFileKey(String fileKey)
    {
        this.fileKey = fileKey;
    }

    public void setAssetVariation(String assetVariation)
    {
        this.assetVariation = assetVariation;
    }

    public void setFileVariation(String fileVariation)
    {
        this.fileVariation = fileVariation;
    }

    public void setAssetRevision(Integer assetRevision)
    {
        this.assetRevision = assetRevision;
    }

    public void setFileRevision(Integer fileRevision)
    {
        this.fileRevision = fileRevision;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((assetKey == null) ? 0 : assetKey.hashCode());
        result = prime * result + ((assetRevision == null) ? 0 : assetRevision.hashCode());
        result = prime * result + ((assetVariation == null) ? 0 : assetVariation.hashCode());
        result = prime * result + ((fileKey == null) ? 0 : fileKey.hashCode());
        result = prime * result + ((fileRevision == null) ? 0 : fileRevision.hashCode());
        result = prime * result + ((fileVariation == null) ? 0 : fileVariation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LAssetRevisionFileRevision other = (LAssetRevisionFileRevision) obj;
        if (assetKey == null)
        {
            if (other.assetKey != null)
                return false;
        }
        else if (!assetKey.equals(other.assetKey))
            return false;
        if (assetRevision == null)
        {
            if (other.assetRevision != null)
                return false;
        }
        else if (!assetRevision.equals(other.assetRevision))
            return false;
        if (assetVariation == null)
        {
            if (other.assetVariation != null)
                return false;
        }
        else if (!assetVariation.equals(other.assetVariation))
            return false;
        if (fileKey == null)
        {
            if (other.fileKey != null)
                return false;
        }
        else if (!fileKey.equals(other.fileKey))
            return false;
        if (fileRevision == null)
        {
            if (other.fileRevision != null)
                return false;
        }
        else if (!fileRevision.equals(other.fileRevision))
            return false;
        if (fileVariation == null)
        {
            if (other.fileVariation != null)
                return false;
        }
        else if (!fileVariation.equals(other.fileVariation))
            return false;
        return true;
    }

}
