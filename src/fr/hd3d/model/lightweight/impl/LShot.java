package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILShot;


/**
 * @author Try LAM
 */
public class LShot extends LBase implements ILShot
{
    private Long sequence;
    private Integer completion;
    private String description;
    private Integer difficulty;
    private String hook;
    private String label;
    private String name;
    private Integer trust;
    private Boolean mattePainting;
    private Boolean layout;
    private Boolean animation;
    private Boolean export;
    private Boolean tracking;
    private Boolean dressing;
    private Boolean lighting;
    private Boolean rendering;
    private Boolean compositing;
    private Integer nbFrame;
    private String sequencesNames;

    private Integer startFrame;
    private Integer endFrame;

    public void clear()
    {
        super.clear();
        sequence = null;
        completion = null;
        description = null;
        difficulty = null;
        hook = null;
        label = null;
        name = null;
        trust = null;
        mattePainting = null;
        layout = null;
        animation = null;
        export = null;
        tracking = null;
        dressing = null;
        lighting = null;
        rendering = null;
        compositing = null;
        nbFrame = null;
        sequencesNames = null;
    }

    public LShot()
    {}

    public LShot(ILShot shot)
    {
        super(shot.getId(), shot.getVersion());
        this.setSequence(shot.getSequence());
        this.setCompletion(shot.getCompletion());
        this.setDescription(shot.getDescription());
        this.setDifficulty(shot.getDifficulty());
        this.setHook(shot.getHook());
        this.setLabel(shot.getLabel());
        this.setTrust(shot.getTrust());
        this.setMattePainting(shot.getMattePainting());
        this.setLayout(shot.getLayout());
        this.setAnimation(shot.getAnimation());
        this.setExport(shot.getExport());
        this.setTracking(shot.getTracking());
        this.setDressing(shot.getDressing());
        this.setLighting(shot.getLighting());
        this.setRendering(shot.getRendering());
        this.setCompositing(shot.getCompositing());
        this.setNbFrame(shot.getNbFrame());
        this.setSequencesNames(shot.getSequencesNames());
        this.setStartFrame(shot.getStartFrame());
        this.setEndFrame(shot.getEndFrame());
    }

    public static ILShot getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, List<String> tagNames,
            Long sequence, Integer completion, String description, Integer difficulty, String hook, String label,
            Integer trust, Boolean mattePainting, Boolean layout, Boolean animation, Boolean export, Boolean tracking,
            Boolean dressing, Boolean lighting, Boolean rendering, Boolean compositing, Integer nBFrame,
            String sequencesNames, Integer startFrame, Integer endFrame)
    {
        final ILShot shot = new LShot();
        shot.setId(id);
        shot.setVersion(version);
        shot.setTags(tags);
        shot.setTagNames(tagNames);
        shot.setSequence(sequence);
        if (completion != null)
            shot.setCompletion(completion);
        shot.setDescription(description);
        if (difficulty != null)
            shot.setDifficulty(difficulty);
        shot.setHook(hook);
        shot.setLabel(label);
        if (trust != null)
            shot.setTrust(trust);
        if (mattePainting != null)
            shot.setMattePainting(mattePainting);
        if (layout != null)
            shot.setLayout(layout);
        if (animation != null)
            shot.setAnimation(animation);
        if (export != null)
            shot.setExport(export);
        if (tracking != null)
            shot.setTracking(tracking);
        if (dressing != null)
            shot.setDressing(dressing);
        if (lighting != null)
            shot.setLighting(lighting);
        if (rendering != null)
            shot.setRendering(rendering);
        if (compositing != null)
            shot.setCompositing(compositing);
        if (nBFrame != null)
            shot.setNbFrame(nBFrame);
        if (sequencesNames != null)
            shot.setSequencesNames(sequencesNames);
        if (startFrame != null)
            shot.setStartFrame(startFrame);
        if (endFrame != null)
            shot.setEndFrame(endFrame);
        return shot;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static ILShot getNewInstance(Long sequence, Integer completion, String description, Integer difficulty,
            String hook, String label, Integer trust, Boolean mattePainting, Boolean layout, Boolean animation,
            Boolean export, Boolean tracking, Boolean dressing, Boolean lighting, Boolean rendering,
            Boolean compositing, Integer nBFrame, String sequencesNames, Integer startFrame, Integer endFrame)
    {
        return getNewInstance(null, null, null, null, sequence, completion, description, difficulty, hook, label,
                trust, mattePainting, layout, animation, export, tracking, dressing, lighting, rendering, compositing,
                nBFrame, sequencesNames, startFrame, endFrame);
    }

    public Long getSequence()
    {
        return sequence;
    }

    public Integer getCompletion()
    {
        return completion;
    }

    public String getDescription()
    {
        return description;
    }

    public Integer getDifficulty()
    {
        return difficulty;
    }

    public String getHook()
    {
        return hook;
    }

    public String getLabel()
    {
        return label;
    }

    public Integer getTrust()
    {
        return trust;
    }

    public Boolean getMattePainting()
    {

        return mattePainting;
    }

    public Boolean getLayout()
    {

        return layout;
    }

    public Boolean getAnimation()
    {

        return animation;
    }

    public Boolean getExport()
    {

        return export;
    }

    public Boolean getTracking()
    {

        return tracking;
    }

    public Boolean getDressing()
    {

        return dressing;
    }

    public Boolean getLighting()
    {

        return lighting;
    }

    public Boolean getRendering()
    {

        return rendering;
    }

    public Boolean getCompositing()
    {

        return compositing;
    }

    public String getSequencesNames()
    {
        return sequencesNames;
    }

    public void setSequence(Long sequenceId)
    {
        this.sequence = sequenceId;
    }

    public void setCompletion(Integer completion)
    {
        this.completion = completion;
    }

    public void setDescription(String desc)
    {
        this.description = desc;
    }

    public void setDifficulty(Integer difficulty)
    {
        this.difficulty = difficulty;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setLabel(String label)
    {
        this.label = label;
        this.name = label;
    }

    public void setTrust(Integer trust)
    {
        this.trust = trust;
    }

    public void setMattePainting(Boolean mattePainting)
    {
        this.mattePainting = mattePainting;
    }

    public void setLayout(Boolean layout)
    {
        this.layout = layout;
    }

    public void setAnimation(Boolean animation)
    {
        this.animation = animation;
    }

    public void setExport(Boolean export)
    {
        this.export = export;
    }

    public void setTracking(Boolean tracking)
    {
        this.tracking = tracking;
    }

    public void setDressing(Boolean dressing)
    {
        this.dressing = dressing;
    }

    public void setLighting(Boolean lighting)
    {
        this.lighting = lighting;
    }

    public void setRendering(Boolean rendering)
    {
        this.rendering = rendering;
    }

    public void setCompositing(Boolean compositing)
    {
        this.compositing = compositing;
    }

    public void setSequencesNames(String sequencesNames)
    {
        this.sequencesNames = sequencesNames;
    }

    public void setStartFrame(Integer startFrame)
    {
        this.startFrame = startFrame;
    }

    public void setEndFrame(Integer endFrame)
    {
        this.endFrame = endFrame;
    }

    public Integer getStartFrame()
    {
        return startFrame;
    }

    public Integer getEndFrame()
    {
        return endFrame;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + completion;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + difficulty;
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + trust;
        result = prime * result + startFrame;
        result = prime * result + endFrame;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LShot other = (LShot) obj;
        if (completion != other.completion)
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (difficulty != other.difficulty)
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (label == null)
        {
            if (other.label != null)
                return false;
        }
        else if (!label.equals(other.label))
            return false;
        if (sequence == null)
        {
            if (other.sequence != null)
                return false;
        }
        else if (!sequence.equals(other.sequence))
            return false;
        if (trust != other.trust)
            return false;
        if (mattePainting != other.mattePainting)
            return false;
        if (layout != other.layout)
            return false;
        if (animation != other.animation)
            return false;
        if (export != other.export)
            return false;
        if (tracking != other.tracking)
            return false;
        if (dressing != other.dressing)
            return false;
        if (lighting != other.lighting)
            return false;
        if (rendering != other.rendering)
            return false;
        if (compositing != other.compositing)
            return false;
        if (startFrame != other.startFrame)
            return false;
        if (endFrame != other.endFrame)
            return false;
        return true;
    }

    /**
     * @return the nBFrame
     */
    public Integer getNbFrame()
    {
        return nbFrame;
    }

    /**
     * @param frame
     *            the nBFrame to set
     */
    public void setNbFrame(Integer frame)
    {
        nbFrame = frame;
    }

}
