package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILDeviceType;


/**
 * Lightweight implementation for deviceType object.
 * 
 * @author HD3D
 */
public class LDeviceType extends LSimple implements ILDeviceType
{
    /**
     * Default Constructor.
     */
    public LDeviceType()
    {}

    public LDeviceType(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILDeviceType getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LDeviceType deviceType = new LDeviceType(id, version, tags, name);

        return deviceType;
    }

    public static ILDeviceType getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }
}
