package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILClassDynMetaDataType;


/**
 * @author Try LAM
 */
public class LClassDynMetaDataType extends LBase implements ILClassDynMetaDataType
{
    private String name;
    private Long dynMetaDataType;
    private String dynMetaDataTypeName;
    private String dynMetaDataTypeNature;
    private String predicate;
    private String className;
    private String extra;

    public void clear()
    {
        super.clear();

        name = null;
        dynMetaDataType = null;
        predicate = null;
        className = null;
        extra = null;
    }

    public LClassDynMetaDataType()
    {
        super();
    }

    public static ILClassDynMetaDataType getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags,
            String name, Long dynMetaDataType, String predicate, String className, String extra)
    {
        ILClassDynMetaDataType classDynMetaData = new LClassDynMetaDataType();
        classDynMetaData.setId(id);
        classDynMetaData.setVersion(version);
        classDynMetaData.setTags(tags);
        classDynMetaData.setName(name);
        classDynMetaData.setDynMetaDataType(dynMetaDataType);
        classDynMetaData.setPredicate(predicate);
        classDynMetaData.setClassName(className);
        classDynMetaData.setExtra(extra);
        return classDynMetaData;
    }

    public static ILClassDynMetaDataType getNewInstance(String name, Long dynMetaDataType, String predicate,
            String className, String extra)
    {
        return getNewInstance(null, null, null, name, dynMetaDataType, predicate, className, extra);
    }

    public String getName()
    {
        return name;
    }

    public Long getDynMetaDataType()
    {
        return dynMetaDataType;
    }

    public String getPredicate()
    {
        return predicate;
    }

    public String getClassName()
    {
        return className;
    }

    public String getExtra()
    {
        return extra;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDynMetaDataType(Long dynMetaDataType)
    {
        this.dynMetaDataType = dynMetaDataType;
    }

    public void setPredicate(String predicate)
    {
        this.predicate = predicate;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    /**
     * @return The dynMetaDataTypeName
     */
    public String getDynMetaDataTypeName()
    {
        return dynMetaDataTypeName;
    }

    /**
     * @param dynMetaDataTypeName
     *            The dynMetaDataTypeName to set
     */
    public void setDynMetaDataTypeName(String dynMetaDataTypeName)
    {
        this.dynMetaDataTypeName = dynMetaDataTypeName;
    }

    /**
     * @param The
     *            dynMetaDataType's nature
     */
    public void setDynMetaDataTypeNature(String nature)
    {
        this.dynMetaDataTypeNature = nature;
    }

    /**
     * @return The dynMetaDataType's nature
     */
    public String getDynMetaDataTypeNature()
    {
        return this.dynMetaDataTypeNature;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        result = prime * result + ((dynMetaDataType == null) ? 0 : dynMetaDataType.hashCode());
        result = prime * result + ((dynMetaDataTypeName == null) ? 0 : dynMetaDataTypeName.hashCode());
        result = prime * result + ((extra == null) ? 0 : extra.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((dynMetaDataTypeNature == null) ? 0 : dynMetaDataTypeNature.hashCode());
        result = prime * result + ((predicate == null) ? 0 : predicate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LClassDynMetaDataType other = (LClassDynMetaDataType) obj;
        if (className == null)
        {
            if (other.className != null)
                return false;
        }
        else if (!className.equals(other.className))
            return false;
        if (dynMetaDataType == null)
        {
            if (other.dynMetaDataType != null)
                return false;
        }
        else if (!dynMetaDataType.equals(other.dynMetaDataType))
            return false;
        if (dynMetaDataTypeName == null)
        {
            if (other.dynMetaDataTypeName != null)
                return false;
        }
        else if (!dynMetaDataTypeName.equals(other.dynMetaDataTypeName))
            return false;
        if (extra == null)
        {
            if (other.extra != null)
                return false;
        }
        else if (!extra.equals(other.extra))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (dynMetaDataTypeNature == null)
        {
            if (other.dynMetaDataTypeNature != null)
                return false;
        }
        else if (!dynMetaDataTypeNature.equals(other.dynMetaDataTypeNature))
            return false;
        if (predicate == null)
        {
            if (other.predicate != null)
                return false;
        }
        else if (!predicate.equals(other.predicate))
            return false;
        return true;
    }

}
