package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILCategory;


/**
 * @author Try LAM
 */
public class LCategory extends LSimple implements ILCategory
{

    private Long parent;// only CategoryId needed
    private Long project;// only projectId needed

    public void clear()
    {
        super.clear();
        parent = null;
        project = null;
    }

    public LCategory()
    {
        super();
    }

    public static ILCategory getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String name,
            Long parent, Long project)
    {
        final LCategory category = new LCategory();
        category.setId(id);
        category.setVersion(version);
        category.setTags(tags);
        category.setName(name);
        category.setParent(parent);
        category.setProject(project);
        return category;
    }

    public static ILCategory getNewInstance(String name, Long parent, Long project)
    {
        return getNewInstance(null, null, null, name, parent, project);
    }

    public Long getParent()
    {
        return parent;
    }

    public Long getProject()
    {
        return project;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LCategory other = (LCategory) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        return true;
    }

}
