package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;

import fr.hd3d.model.lightweight.ILMountPoint;


/**
 * Light class to manage mount points according the operating system.
 * 
 * @author HD3D
 * 
 */
public class LMountPoint extends LBase implements ILMountPoint
{
    private String storageName;
    private String operatingSystem;
    private String mountPointPath;

    public void clear()
    {

        storageName = null;
        operatingSystem = null;
        mountPointPath = null;
    }

    public LMountPoint()
    {}

    public LMountPoint(Long id, java.sql.Timestamp version, String storageName, String operatingSystem,
            String mountPointPath)
    {
        super();

        this.storageName = storageName;
        this.operatingSystem = operatingSystem;
        this.mountPointPath = mountPointPath;
    }

    public String getStorageName()
    {
        return storageName;
    }

    public String getOperatingSystem()
    {
        return operatingSystem;
    }

    public String getMountPointPath()
    {
        return mountPointPath;
    }

    public void setStorageName(String storageName)
    {
        this.storageName = storageName;
    }

    public void setOperatingSystem(String operatingSystem)
    {
        this.operatingSystem = operatingSystem;
    }

    public void setMountPointPath(String mountPointPath)
    {
        this.mountPointPath = mountPointPath;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = super.hashCode();
        result = prime * result + ((mountPointPath == null) ? 0 : mountPointPath.hashCode());
        result = prime * result + ((storageName == null) ? 0 : storageName.hashCode());
        result = prime * result + ((operatingSystem == null) ? 0 : operatingSystem.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LMountPoint other = (LMountPoint) obj;

        if (storageName == null)
        {
            if (other.storageName != null)
                return false;
        }
        else if (!storageName.equals(other.storageName))
            return false;
        if (operatingSystem == null)
        {
            if (other.operatingSystem != null)
                return false;
        }
        else if (!operatingSystem.equals(other.operatingSystem))
            return false;
        if (mountPointPath == null)
        {
            if (other.mountPointPath != null)
                return false;
        }
        else if (!mountPointPath.equals(other.mountPointPath))
            return false;

        return true;
    }

    public static ILMountPoint getNewInstance(Long id, Timestamp version, String storageName, String operatingSystem,
            String mountPointPath)
    {
        final LMountPoint mountPoint = new LMountPoint();
        mountPoint.setId(id);
        mountPoint.setVersion(version);
        mountPoint.setStorageName(storageName);
        mountPoint.setOperatingSystem(operatingSystem);
        mountPoint.setMountPointPath(mountPointPath);
        return mountPoint;
    }

}
