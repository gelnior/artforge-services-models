package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILPlayListRevision;


public class LPlayListRevision extends LBase implements ILPlayListRevision
{
    private Long project;
    private String frameRate;
    private String name;
    private Integer revision;
    private String hook;
    private String comment;
    private List<Long> playListEdits;
    private Long playListRevisionGroup;

    /*------------------
     * clear
     ------------------*/
    public void clear()
    {
        super.clear();
        project = null;
        frameRate = null;
        name = null;
        revision = null;
        hook = null;
        comment = null;
        if (playListEdits != null)
            playListEdits.clear();
        playListEdits = null;
        playListRevisionGroup = null;
    }

    /*-------------
     * Factory
     -------------*/
    public LPlayListRevision()
    {
        super();
    }

    public static ILPlayListRevision getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long project,
            String frameRate, String name, Integer revision, String hook, String comment, List<Long> playListEdits,
            Long playListRevisionGroup)
    {
        final LPlayListRevision playListRevision = new LPlayListRevision();
        playListRevision.setId(id);
        playListRevision.setVersion(version);
        playListRevision.setTags(tags);
        playListRevision.setProject(project);
        playListRevision.setFrameRate(frameRate);
        playListRevision.setName(name);
        playListRevision.setRevision(revision);
        playListRevision.setHook(hook);
        playListRevision.setComment(comment);
        playListRevision.setPlayListEdits(playListEdits);
        playListRevision.setPlayListRevisionGroup(playListRevisionGroup);
        return playListRevision;
    }

    public static ILPlayListRevision getNewInstance(Long project, String frameRate, String name, Integer revision,
            String hook, String comment, List<Long> playListEdits, Long playListRevisionGroup)
    {
        return getNewInstance(null, null, null, project, frameRate, name, revision, hook, comment, playListEdits,
                playListRevisionGroup);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    public Long getProject()
    {
        return project;
    }

    public String getFrameRate()
    {
        return frameRate;
    }

    public String getName()
    {
        return name;
    }

    public Integer getRevision()
    {
        return revision;
    }

    public String getHook()
    {
        return hook;
    }

    public String getComment()
    {
        return comment;
    }

    public List<Long> getPlayListEdits()
    {
        return playListEdits;
    }

    public void setProject(Long project)
    {
        this.project = project;
    }

    public void setFrameRate(String frameRate)
    {
        this.frameRate = frameRate;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setPlayListEdits(List<Long> playListEdits)
    {
        this.playListEdits = playListEdits;
    }

    public Long getPlayListRevisionGroup()
    {
        return playListRevisionGroup;
    }

    public void setPlayListRevisionGroup(Long playListRevisionGroup)
    {
        this.playListRevisionGroup = playListRevisionGroup;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((frameRate == null) ? 0 : frameRate.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((playListRevisionGroup == null) ? 0 : playListRevisionGroup.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LPlayListRevision other = (LPlayListRevision) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (frameRate == null)
        {
            if (other.frameRate != null)
                return false;
        }
        else if (!frameRate.equals(other.frameRate))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (playListRevisionGroup == null)
        {
            if (other.playListRevisionGroup != null)
                return false;
        }
        else if (!playListRevisionGroup.equals(other.playListRevisionGroup))
            return false;
        return true;
    }

}
