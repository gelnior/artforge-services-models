package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILQualification;


/**
 * Lightweight implementation for qualification object.
 * 
 * @author HD3D
 */
public class LQualification extends LSimple implements ILQualification
{
    private List<Long> skillIDs;
    private List<String> skillNames;

    public void clear()
    {
        super.clear();

        if (skillIDs != null)
            skillIDs.clear();
        skillIDs = null;

        if (skillNames != null)
            skillNames.clear();
        skillNames = null;
    }

    /**
     * Default Constructor.
     */
    public LQualification()
    {}

    public LQualification(Long id, Timestamp version, String name)
    {
        super(id, version, null, name);
    }

    public static ILQualification getNewInstance(Long id, Timestamp version, String name)
    {
        final LQualification qualification = new LQualification(id, version, name);

        return qualification;
    }

    public static ILQualification getNewInstance(String name)
    {
        return getNewInstance(null, null, name);
    }

    public List<Long> getSkillIDs()
    {
        return skillIDs;
    }

    public void addSkillID(Long id)
    {
        if (getSkillIDs() == null)
            setSkillIDs(new ArrayList<Long>());
        getSkillIDs().add(id);
    }

    public void setSkillIDs(List<Long> skillIDs)
    {
        this.skillIDs = skillIDs;
    }

    public List<String> getSkillNames()
    {
        return skillNames;
    }

    public void addSkillName(String name)
    {
        if (getSkillNames() == null)
            setSkillNames(new ArrayList<String>());
        getSkillNames().add(name);
    }

    public void setSkillNames(List<String> skillNames)
    {
        this.skillNames = skillNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((skillIDs == null) ? 0 : skillIDs.hashCode());
        result = prime * result + ((skillNames == null) ? 0 : skillNames.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LQualification other = (LQualification) obj;
        if (skillIDs == null)
        {
            if (other.skillIDs != null)
                return false;
        }
        else if (!skillIDs.equals(other.skillIDs))
            return false;
        if (skillNames == null)
        {
            if (other.skillNames != null)
                return false;
        }
        else if (!skillNames.equals(other.skillNames))
            return false;
        return true;
    }

}
