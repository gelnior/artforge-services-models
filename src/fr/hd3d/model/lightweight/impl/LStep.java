package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILStep;


public class LStep extends LBase implements ILStep
{
    private Long boundEntity;
    private String boundEntityName;
    private Long taskType;
    private String taskTypeName;
    private Boolean createTasknAsset;
    private Integer estimatedDuration;

    public void clear()
    {
        super.clear();
        boundEntity = null;
        boundEntityName = null;
        taskType = null;
        taskTypeName = null;
        createTasknAsset = null;
        estimatedDuration = null;
    }

    public LStep()
    {}

    public static ILStep getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, Long boundEntity,
            String boundEntityName, Long taskType, String taskTypeName, Boolean createTasknAsset,
            Integer estimatedDuration)
    {
        final ILStep step = new LStep();
        step.setId(id);
        step.setVersion(version);
        step.setTags(tags);
        step.setBoundEntity(boundEntity);
        step.setBoundEntityName(boundEntityName);
        step.setTaskType(taskType);
        step.setTaskTypeName(taskTypeName);
        step.setCreateTasknAsset(createTasknAsset);
        step.setEstimatedDuration(estimatedDuration);
        return step;
    }

    public static ILStep getNewInstance(Long boundEntity, String boundEntityName, Long taskType, String taskTypeName,
            Boolean createTasknAsset, Integer estimatedDuration)
    {
        return getNewInstance(null, null, null, boundEntity, boundEntityName, taskType, taskTypeName, createTasknAsset,
                estimatedDuration);
    }

    public Long getBoundEntity()
    {
        return boundEntity;
    }

    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    public Long getTaskType()
    {
        return taskType;
    }

    public String getTaskTypeName()
    {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.taskTypeName = taskTypeName;
    }

    public Boolean getCreateTasknAsset()
    {
        return createTasknAsset;
    }

    public void setCreateTasknAsset(Boolean createTasknAsset)
    {
        this.createTasknAsset = createTasknAsset;
    }

    public Integer getEstimatedDuration()
    {
        return estimatedDuration;
    }

    public void setEstimatedDuration(Integer estimatedDuration)
    {
        this.estimatedDuration = estimatedDuration;
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTaskType(Long task)
    {
        this.taskType = task;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        return result;
    }

    public String toString()
    {
        String duration = "0";
        if (this.estimatedDuration != null)
        {
            Double tmpDuration = new Double(this.estimatedDuration);
            tmpDuration = tmpDuration / (double) (8 * 3600);
            duration = tmpDuration.toString();
            if (duration.endsWith(".0"))
                duration = duration.substring(0, duration.length() - 2);
        }
        return duration;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LStep other = (LStep) obj;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        return true;
    }

}
