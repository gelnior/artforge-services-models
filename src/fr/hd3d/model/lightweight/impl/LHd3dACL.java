package fr.hd3d.model.lightweight.impl;

import java.util.Set;

import fr.hd3d.model.lightweight.ILHd3dACL;


/**
 * @author Try LAM
 */
public class LHd3dACL extends LBase implements ILHd3dACL
{
    private Set<Long> roles;
    private String entityName;
    private String readPermitted;
    private String readBans;
    private String createPermitted;
    private String updatePermitted;
    private String updateBans;
    private String deletePermitted;
    private String deleteBans;

    public LHd3dACL()
    {}

    public void clear()
    {
        super.clear();
        if (roles != null)
        {
            roles.clear();
            roles = null;
        }
        entityName = null;
        readPermitted = null;
        createPermitted = null;
        updatePermitted = null;
        deletePermitted = null;
        readBans = null;
        updateBans = null;
        deleteBans = null;
    }

    public Set<Long> getRoles()
    {
        return roles;
    }

    public void setRoles(Set<Long> roles)
    {
        this.roles = roles;
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public String getReadPermitted()
    {
        return readPermitted;
    }

    public void setReadPermitted(String read)
    {
        this.readPermitted = read;
    }

    public String getCreatePermitted()
    {
        return createPermitted;
    }

    public void setCreatePermitted(String create)
    {
        this.createPermitted = create;
    }

    public String getUpdatePermitted()
    {
        return updatePermitted;
    }

    public void setUpdatePermitted(String update)
    {
        this.updatePermitted = update;
    }

    public String getDeletePermitted()
    {
        return deletePermitted;
    }

    public void setDeletePermitted(String delete)
    {
        this.deletePermitted = delete;
    }

    public String getReadBans()
    {
        return readBans;
    }

    public void setReadBans(String readBans)
    {
        this.readBans = readBans;
    }

    public String getUpdateBans()
    {
        return updateBans;
    }

    public void setUpdateBans(String updateBans)
    {
        this.updateBans = updateBans;
    }

    public String getDeleteBans()
    {
        return deleteBans;
    }

    public void setDeleteBans(String deleteBans)
    {
        this.deleteBans = deleteBans;
    }

}
