package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.ILLicense;


/**
 * Lightweight implementation for license object.
 * 
 * @author HD3D
 */
public class LLicense extends LInventoryItem implements ILLicense
{
    private Integer number;
    private String type;
    private Long softwareId;
    private String softwareName;

    private List<Long> computerIDs;
    private List<String> computerNames;

    public void clear()
    {
        super.clear();
        number = null;
        type = null;
        softwareId = null;
        softwareName = null;

        if (computerIDs != null)
            computerIDs.clear();
        computerIDs = null;

        if (computerNames != null)
            computerNames.clear();
        computerNames = null;
    }

    public LLicense()
    {}

    public LLicense(Long id, Timestamp version, List<Long> tags, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Integer number, String type)
    {
        super(id, version, null, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus);

        this.name = name;
        this.number = number;
        this.type = type;
        // this.softwareID = new Long(0);
        // this.softwareName = new String();

        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());
    }

    public static ILLicense getNewInstance(Long id, Timestamp version, List<Long> tags, String name, String serial,
            String billingReference, Date purchaseDate, Date warrantyEnd, String inventoryStatus, Integer number,
            String type)
    {
        final ILLicense license = new LLicense(id, version, tags, name, serial, billingReference, purchaseDate,
                warrantyEnd, inventoryStatus, number, type);

        return license;
    }

    public static ILLicense getNewInstance(String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, String inventoryStatus, Integer number, String type)
    {
        final ILLicense license = new LLicense(null, null, null, name, serial, billingReference, purchaseDate,
                warrantyEnd, inventoryStatus, number, type);

        return license;
    }

    public Integer getNumber()
    {
        return number;
    }

    public String getType()
    {
        return type;
    }

    public Long getSoftwareId()
    {
        return softwareId;
    }

    public String getSoftwareName()
    {
        return softwareName;
    }

    public final List<Long> getComputerIDs()
    {
        return this.computerIDs;
    }

    public void addComputerID(Long id)
    {
        if (getComputerIDs() == null)
            setComputerIDs(new ArrayList<Long>());
        getComputerIDs().add(id);
    }

    public final List<String> getComputerNames()
    {
        return this.computerNames;
    }

    public void addComputerName(String name)
    {
        if (getComputerNames() == null)
            setComputerNames(new ArrayList<String>());
        getComputerNames().add(name);
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setNumber(Integer number)
    {
        this.number = number;
    }

    public void setSoftwareId(Long softwareId)
    {
        this.softwareId = softwareId;
    }

    public void setSoftwareName(String softwareName)
    {
        this.softwareName = softwareName;
    }

    public final void setComputerIDs(List<Long> computerIDs)
    {
        this.computerIDs = computerIDs;
    }

    public final void setComputerNames(List<String> computerNames)
    {
        this.computerNames = computerNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        result = prime * result + ((softwareId == null) ? 0 : softwareId.hashCode());
        result = prime * result + ((softwareName == null) ? 0 : softwareName.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LLicense other = (LLicense) obj;
        if (number == null)
        {
            if (other.number != null)
                return false;
        }
        else if (!number.equals(other.number))
            return false;
        if (softwareId == null)
        {
            if (other.softwareId != null)
                return false;
        }
        else if (!softwareId.equals(other.softwareId))
            return false;
        if (softwareName == null)
        {
            if (other.softwareName != null)
                return false;
        }
        else if (!softwareName.equals(other.softwareName))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

}
