package fr.hd3d.model.lightweight.impl;

import java.util.List;

import fr.hd3d.model.lightweight.ILListValues;


/**
 * @author Try LAM
 */
public class LListValues extends LBase implements ILListValues
{

    private String separator;
    private String values;

    public void clear()
    {
        super.clear();
        separator = null;
        values = null;
    }

    public LListValues()
    {}

    public static LListValues getNewInstance(Long id, java.sql.Timestamp version, List<Long> tags, String separator,
            String values)
    {
        LListValues listValues = new LListValues();

        listValues.setId(id);
        listValues.setVersion(version);
        listValues.setTags(tags);
        listValues.setSeparator(separator);
        listValues.setValues(values);

        return listValues;
    }

    public static LListValues getNewInstance(String separator, String values)
    {
        return getNewInstance(null, null, null, separator, values);
    }

    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public String getValues()
    {
        return values;
    }

    public void setValues(String values)
    {
        this.values = values;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((separator == null) ? 0 : separator.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LListValues other = (LListValues) obj;
        if (separator == null)
        {
            if (other.separator != null)
                return false;
        }
        else if (!separator.equals(other.separator))
            return false;
        if (values == null)
        {
            if (other.values != null)
                return false;
        }
        else if (!values.equals(other.values))
            return false;
        return true;
    }
}
