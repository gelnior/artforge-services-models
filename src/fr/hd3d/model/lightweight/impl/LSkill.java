package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.List;

import fr.hd3d.model.lightweight.ILSkill;


/**
 * Lightweight implementation for room object.
 * 
 * @author HD3D
 */
public class LSkill extends LSimple implements ILSkill
{
    Long softwareID;
    String softwareName;

    public void clear()
    {
        super.clear();
        softwareID = null;
        softwareName = null;
    }

    /**
     * Default Constructor.
     */
    public LSkill()
    {}

    public LSkill(Long id, Timestamp version, List<Long> tags, String name)
    {
        super(id, version, tags, name);
    }

    public static ILSkill getNewInstance(Long id, Timestamp version, String name)
    {
        final LSkill skill = new LSkill(id, version, null, name);

        return skill;
    }

    public static ILSkill getNewInstance(String name)
    {
        return getNewInstance(null, null, name);
    }

    public Long getSoftwareId()
    {
        return softwareID;
    }

    public void setSoftwareId(Long softwareId)
    {
        softwareID = softwareId;
    }

    public String getSoftwareName()
    {
        return softwareName;
    }

    public void setSoftwareName(String softwareName)
    {
        this.softwareName = softwareName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((softwareID == null) ? 0 : softwareID.hashCode());
        result = prime * result + ((softwareName == null) ? 0 : softwareName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LSkill other = (LSkill) obj;
        if (softwareID == null)
        {
            if (other.softwareID != null)
                return false;
        }
        else if (!softwareID.equals(other.softwareID))
            return false;
        if (softwareName == null)
        {
            if (other.softwareName != null)
                return false;
        }
        else if (!softwareName.equals(other.softwareName))
            return false;
        return true;
    }

}
