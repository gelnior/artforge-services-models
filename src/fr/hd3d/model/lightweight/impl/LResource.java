package fr.hd3d.model.lightweight.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.hd3d.model.lightweight.ILResource;


/**
 * Lightweight implementation for resource object.
 * 
 * @author HD3D
 */
public class LResource extends LBase implements ILResource
{
    private List<Long> resourceGroupIDs;
    private List<String> resourceGroupNames;
    private List<Long> projectIDs;
    private List<String> projectNames;

    public void clear()
    {
        super.clear();
        if (resourceGroupIDs != null)
            resourceGroupIDs.clear();
        resourceGroupIDs = null;
        if (resourceGroupNames != null)
            resourceGroupNames.clear();
        resourceGroupNames = null;
        if (projectIDs != null)
            projectIDs.clear();
        projectIDs = null;
    }

    /**
     * Default Constructor.
     */
    public LResource()
    {}

    public LResource(Long id, Timestamp version)
    {
        super(id, version);

        this.setResourceGroupIDs(new ArrayList<Long>());
        this.setResourceGroupNames(new ArrayList<String>());
    }

    public static ILResource getNewInstance(Long id, Timestamp version, List<Long> tags, String name)
    {
        final LResource LResource = new LResource(id, version);
        LResource.setTags(tags);
        return LResource;
    }

    public static ILResource getNewInstance(String name)
    {
        return getNewInstance(null, null, null, name);
    }

    public final List<Long> getResourceGroupIDs()
    {
        return this.resourceGroupIDs;
    }

    public void addResourceGroupID(Long id)
    {
        if (getResourceGroupIDs() == null)
            setResourceGroupIDs(new ArrayList<Long>());
        getResourceGroupIDs().add(id);
    }

    public final List<String> getResourceGroupNames()
    {
        return this.resourceGroupNames;
    }

    public void addResourceGroupName(String name)
    {
        if (getResourceGroupNames() == null)
            setResourceGroupNames(new ArrayList<String>());
        getResourceGroupNames().add(name);
    }

    public List<Long> getProjectIDs()
    {
        return projectIDs;
    }

    public void addProjectID(Long id)
    {
        if (getProjectIDs() == null)
            setProjectIDs(new ArrayList<Long>());
        getProjectIDs().add(id);
    }

    public List<String> getProjectNames()
    {
        return projectNames;
    }

    public void addProjectName(String name)
    {
        if (getProjectNames() == null)
            setProjectNames(new ArrayList<String>());
        getProjectNames().add(name);
    }

    public final void setResourceGroupIDs(List<Long> resourceGroupIds)
    {
        this.resourceGroupIDs = resourceGroupIds;
    }

    public final void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.resourceGroupNames = resourceGroupNames;
    }

    public void setProjectIDs(List<Long> projectIDs)
    {
        this.projectIDs = projectIDs;
    }

    public void setProjectNames(List<String> projectNames)
    {
        this.projectNames = projectNames;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((projectIDs == null) ? 0 : projectIDs.hashCode());
        result = prime * result + ((resourceGroupIDs == null) ? 0 : resourceGroupIDs.hashCode());
        result = prime * result + ((resourceGroupNames == null) ? 0 : resourceGroupNames.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LResource other = (LResource) obj;
        if (projectIDs == null)
        {
            if (other.projectIDs != null)
                return false;
        }
        else if (!projectIDs.equals(other.projectIDs))
            return false;
        if (resourceGroupIDs == null)
        {
            if (other.resourceGroupIDs != null)
                return false;
        }
        else if (!resourceGroupIDs.equals(other.resourceGroupIDs))
            return false;
        if (resourceGroupNames == null)
        {
            if (other.resourceGroupNames != null)
                return false;
        }
        else if (!resourceGroupNames.equals(other.resourceGroupNames))
            return false;
        return true;
    }

}
