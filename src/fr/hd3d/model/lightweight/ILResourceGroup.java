package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for resource group lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILResourceGroup extends ILSimple
{
    public Long getParent();

    public void setParent(Long parent);

    public Long getLeaderId();

    public void setLeaderId(Long leaderId);

    public String getLeaderName();

    public void setLeaderName(String leaderName);

    public List<Long> getResourceIDs();

    public void setResourceIDs(List<Long> resourceIds);

    public List<String> getResourceNames();

    public void setResourceNames(List<String> resourceNames);

    public List<Long> getProjectIDs();

    public List<String> getProjectNames();

    public void setProjectIDs(List<Long> resourceIds);

    public void setProjectNames(List<String> resourceNames);

    public List<Long> getOrganizationIDs();

    public List<String> getOrganizationNames();

    public void setOrganizationIDs(List<Long> resourceIds);

    public void setOrganizationNames(List<String> resourceNames);

    public void setRoleIDs(List<Long> rolesIDs);

    public List<Long> getRoleIDs();

    public List<String> getRoleNames();

    public void setRoleNames(List<String> rolesNames);

    public void addResourceID(Long id);

    public void addResourceName(String name);

    public void addProjectID(Long id);

    public void addProjectName(String name);

    public void addOrganizationID(Long id);

    public void addOrganizationName(String name);

    public void addRoleID(Long id);

    public void addRoleName(String name);

}
