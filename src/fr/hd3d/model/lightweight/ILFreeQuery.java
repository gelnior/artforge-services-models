package fr.hd3d.model.lightweight;

/**
 * Free Query
 * 
 * @author Try LAM
 * 
 */
public interface ILFreeQuery extends ILBase
{
    // nothing to do, just acts as a flag
}
