package fr.hd3d.model.lightweight;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ILTaskActivity extends ILActivity
{
    @ClientField
    public abstract ILTask getTask();

    @ClientField
    public abstract void setTask(ILTask task);

    @ClientField
    public void setTaskName(String taskName);

    @ClientField
    public Long getTaskTypeId();

    @ClientField
    public void setTaskTypeId(Long taskTypeId);

    @ClientField
    public Long getTaskDuration();

    @ClientField
    public void setTaskDuration(Long taskDuration);

    @ClientField
    public String getTaskTypeName();

    @ClientField
    public String getTaskTypeColor();

    @ServerImmutable
    public Long getTaskID();

    @ClientField
    public String getTaskStatus();

    @ClientField
    public void setTaskStatus(String taskStatus);

    @ClientField
    public Long getWorkObjectId();

    @ClientField
    public void setWorkObjectId(Long workObjectId);

    @ClientField
    public String getWorkObjectName();

    @ClientField
    public void setWorkObjectName(String workObjectName);

    public String getWorkObjectParentsNames();

    public void setWorkObjectParentsNames(String parentsNames);

    public String getWorkObjectEntity();

    public void setWorkObjectEntity(String entity);

}
