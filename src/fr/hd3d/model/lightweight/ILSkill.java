package fr.hd3d.model.lightweight;

/**
 * Interface for processor lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILSkill extends ILSimple
{
    public abstract Long getSoftwareId();

    public abstract void setSoftwareId(Long softwareId);

    public abstract String getSoftwareName();

    public abstract void setSoftwareName(String softwareName);
}
