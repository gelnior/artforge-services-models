package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for software lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILLicense extends ILInventoryItem
{
    public abstract Integer getNumber();

    public abstract String getType();

    public Long getSoftwareId();

    public String getSoftwareName();

    public List<Long> getComputerIDs();

    public List<String> getComputerNames();

    public abstract void setName(String name);

    public abstract void setNumber(Integer number);

    public abstract void setType(String type);

    public void setSoftwareId(Long softwareId);

    public void setSoftwareName(String softwareName);

    public void setComputerIDs(List<Long> computerIDs);

    public void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);
}
