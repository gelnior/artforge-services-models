package fr.hd3d.model.lightweight;

import java.util.List;


public interface ILAssetRevisionGroup extends ILBase
{

    public String getName();

    public List<Long> getAssetRevisions();

    public String getCriteria();

    public String getValue();

    public Long getProject();

    public void setName(String name);

    public void setAssetRevisions(List<Long> assetRevisions);

    public void setCriteria(String criteria);

    public void setValue(String value);

    public void setProject(Long project);

}
