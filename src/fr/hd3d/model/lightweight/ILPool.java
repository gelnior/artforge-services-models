package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for processor lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILPool extends ILSimple
{
    public abstract Boolean getIsDispatcherAllowed();

    public abstract void setIsDispatcherAllowed(Boolean isDispatcherAllowed);

    public List<Long> getComputerIDs();

    public List<String> getComputerNames();

    public void setComputerIDs(List<Long> computerIDs);

    public void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);

}
