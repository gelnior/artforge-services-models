package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.List;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author Guillaume CHATELET Implementation of the A34 set specifications
 *         https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 */
public interface ILTask extends ILTaskBase
{
    String getName();

    String getStatus();

    Byte getCompletion();

    Date getCompletionDate();

    @ClientField
    ILProject getProject();

    Long getProjectID();

    @ServerImmutable
    String getProjectName();

    @ServerImmutable
    String getColor();

    @ServerImmutable
    String getTaskTypeName();

    @ServerImmutable
    String getProjectColor();

    @ClientField
    ILPerson getCreator();

    Long getCreatorID();

    @ServerImmutable
    String getCreatorName();

    @ClientField
    ILPerson getWorker();

    Long getWorkerID();

    @ServerImmutable
    String getWorkerName();

    Long getBoundEntityTaskLink();

    String getCommentForApprovalNote();

    void setCommentForApprovalNote(String commentForApprovalNote);

    List<Long> getFileRevisionsForApprovalNote();

    void setFileRevisionsForApprovalNote(List<Long> fileRevisions);

    void setBoundEntityTaskLink(Long boundEntityTaskLink);

    void setName(String name);

    void setStatus(String status);

    void setCompletion(Byte completion);

    void setCompletionDate(Date completionDate);

    @ClientField
    void setProject(ILProject project);

    void setProjectID(Long projectID);

    @ClientField
    void setCreator(ILPerson creator);

    void setCreatorID(Long creatorID);

    @ClientField
    void setWorker(ILPerson worker);

    void setWorkerID(Long workerID);

    Date getDeadLine();

    void setDeadLine(Date deadLineDate);

    Date getActualStartDate();

    void setActualStartDate(Date actualStartDate);

    Date getActualEndDate();

    void setActualEndDate(Date actualEndDate);

    Boolean getStartable();

    void setStartable(Boolean startable);

    Boolean getConfirmed();

    void setConfirmed(Boolean confirmed);

    Boolean getStartup();

    void setStartup(Boolean startup);

    Long getTaskTypeID();

    ILTaskType getTaskType();

    void setTaskTypeID(Long taskTypeID);

    void setTaskType(ILTaskType taskType);

    public String getProjectTypeName();

    public void setProjectTypeName(String projectTypeName);

    public String getBoundEntityName();

    public void setBoundEntityName(String boundEntityName);

    public String getWorkObjectName();

    public void setWorkObjectName(String workObjectName);

    public String getWorkObjectParentsNames();

    public void setWorkObjectParentsNames(String parentsNames);

    public Long getWorkObjectId();

    public void setWorkObjectId(Long workObjectId);

    public Long getTotalActivitiesDuration();

    public void setTotalActivitiesDuration(Long totalActivitiesDuration);
}
