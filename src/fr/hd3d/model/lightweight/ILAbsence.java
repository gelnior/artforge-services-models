/**
 * 
 */
package fr.hd3d.model.lightweight;

/**
 * @author thomas-eskenazi
 * 
 */
public interface ILAbsence extends ILDurable, ILBase
{
    public ILPerson getWorker();

    public void setWorker(ILPerson worker);

    public Long getWorkerID();

    public void setWorkerID(Long workerID);

    public String getComment();

    public void setComment(String comment);

    public String getType();

    public void setType(String type);
}
