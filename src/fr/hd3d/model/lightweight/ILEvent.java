/**
 * 
 */
package fr.hd3d.model.lightweight;

/**
 * @author thomas-eskenazi
 * 
 */
public interface ILEvent extends ILBase, ILDurable
{
    public ILPlanning getPlanning();

    public Long getPlanningID();

    public String getTitle();

    public String getDescription();

    public void setPlanning(ILPlanning planning);

    public void setPlanningID(Long planningID);

    public void setTitle(String title);

    public void setDescription(String description);
}
