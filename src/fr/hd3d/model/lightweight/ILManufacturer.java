package fr.hd3d.model.lightweight;



/**
 * Interface for software lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILManufacturer extends ILBase
{
    public abstract String getName();

    public abstract void setName(String name);
}
