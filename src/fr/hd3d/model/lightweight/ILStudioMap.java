package fr.hd3d.model.lightweight;

import java.util.List;


public interface ILStudioMap extends ILBase
{
    public abstract String getName();

    public abstract void setName(String name);

    public abstract String getImagePath();

    public abstract void setImagePath(String imagePath);

    public abstract Integer getSizeX();

    public abstract void setSizeX(Integer sizeX);

    public abstract Integer getSizeY();

    public abstract void setSizeY(Integer sizeY);

    public abstract List<Long> getComputerIDs();

    public abstract void setComputerIDs(List<Long> computerIDs);

    public abstract List<String> getComputerNames();

    public abstract void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);
}
