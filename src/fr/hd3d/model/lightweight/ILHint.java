package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILHint extends ILBase
{

    public String getName();

    public void setName(String name);

    public String getDescription();

    public void setDescription(String description);
}
