package fr.hd3d.model.lightweight;

import java.util.Date;
import java.util.Set;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


public interface ILAssetAbstract extends ILBase
{
    @ServerImmutable
    public String getKey();

    public void setKey(String key);

    @ServerImmutable
    public String getVariation();

    public void setVariation(String variation);

    @ServerImmutable
    public String getName();

    public void setName(String name);

    @ServerImmutable
    public Long getTaskType();

    public void setTaskType(Long type);

    public String getTaskTypeName();

    public void setTaskTypeName(String taskTypeName);

    @ServerImmutable
    @ClientField
    public Set<ILAssetRevision> getRevisionObjects();

    @ServerImmutable
    public Set<Integer> getRevisions();

    public void setRevisions(Set<Integer> revisions);

    @ServerImmutable
    public Integer getLastRevision();

    public void setLastRevision(Integer revision);

    @ServerImmutable
    public Date getCreationDate();

    public void setCreationDate(Date creationDate);

    @ServerImmutable
    public Long getCreatorId();

    public void setCreatorId(Long creatorId);

    @ServerImmutable
    public Long getProjectId();

    public void setProjectId(Long projectId);

}
