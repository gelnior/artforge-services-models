package fr.hd3d.model.lightweight;

public interface ILDynamicPath extends ILSimple
{

    Long getProjectId();

    Long getWorkObjectId();

    String getWorkObjectName();

    String getBoundEntityName();

    Long getTaskTypeId();

    String getTaskTypeName();

    String getWipDynamicPath();

    String getPublishDynamicPath();

    String getOldDynamicPath();

    Long getParent();

    String getType();

    void setType(String type);

    void setParent(Long parent);

    void setProjectId(final Long projectId);

    void setWorkObjectId(final Long workObjectId);

    void setWorkObjectName(final String workObjectName);

    void setBoundEntityName(final String boundEntityName);

    void setTaskTypeId(final Long taskTypeId);

    void setTaskTypeName(final String taskTypeName);

    void setWipDynamicPath(final String wipDynamicPath);

    void setPublishDynamicPath(final String publishDynamicPath);

    void setOldDynamicPath(final String oldDynamicPath);

    void setTaskTypeColor(String color);

}
