package fr.hd3d.model.lightweight;

import java.util.Date;

import fr.hd3d.model.lightweight.annotation.ClientField;


public interface ILTaskBase extends ILBase
{

    public abstract Date getStartDate();

    public abstract void setStartDate(Date startDate);

    public abstract Date getEndDate();

    public abstract void setEndDate(Date date);

    public abstract Long getDuration();

    public abstract void setDuration(Long duration);

    @ClientField
    public abstract ILTaskGroup getTaskGroup();

    public abstract Long getTaskGroupID();

    public abstract void setTaskGroupID(Long taskGroup);

    @ClientField
    public abstract ILExtraLine getExtraLine();

    public abstract Long getExtraLineID();

    public abstract void setExtraLineID(Long extraLine);

}
