package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ILItemGroup extends ILBase, Hook
{

    public String getName();

    public List<Long> getItems();

    public Long getSheet();

    public void setName(String name);

    public void setItems(List<Long> items);

    public void setSheet(Long sheet);

}
