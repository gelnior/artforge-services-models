package fr.hd3d.model.lightweight;

public interface ILScript extends ILBase
{
    public String getTrigger();

    public String getName();

    public String getDescription();

    public String getLocation();

    public Integer getExitCode();

    public java.util.Date getLastRun();

    public Integer getLastExitCode();

    public String getLastRunMessage();

    public void setTrigger(String trigger);

    public void setName(String name);

    public void setDescription(String description);

    public void setLocation(String location);

    public void setExitCode(Integer exitCode);

    public void setLastRun(java.util.Date lastRun);

    public void setLastExitCode(Integer lastExitCode);

    public void setLastRunMessage(String lastRunMessage);
}
