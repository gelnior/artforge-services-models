package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for software lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILSoftware extends ILSimple
{
    public List<Long> getLicenseIDs();

    public List<String> getLicenseNames();

    public List<Long> getComputerIDs();

    public List<String> getComputerNames();

    public void setComputerIDs(List<Long> computerIDs);

    public void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);

    public void addLicenseID(Long id);

    public void addLicenseName(String name);
}
