package fr.hd3d.model.lightweight;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * @author michael.guiral
 * 
 */
public interface ILTaskChanges extends ILTaskBase
{
    @ClientField
    public ILTask getTask();

    public Long getTaskID();

    public void setTaskID(Long task);

    @ServerImmutable
    public String getTaskName();

    @ClientField
    public ILPlanning getPlanning();

    public Long getPlanningID();

    public void setPlanningID(Long planning);

    @ClientField
    public ILPerson getWorker();

    public Long getWorkerID();

    @ServerImmutable
    public String getPersonName();

    public void setWorkerID(Long person);

    public String getWorkObjectName();

    public void setWorkObjectName(String workObjectName);

}
