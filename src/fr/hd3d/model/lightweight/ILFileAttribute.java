package fr.hd3d.model.lightweight;



public interface ILFileAttribute extends ILBase
{

    public Long getFile();

    public String getName();

    public String getValue();

    public void setFile(Long file);

    public void setName(String name);

    public void setValue(String value);

}
