package fr.hd3d.model.lightweight;

/**
 * a sequence has one and only one parent.
 * 
 * @author Try LAM
 */
public interface ILSequence extends ILBase, Hook
{

    // getters
    public String getName();

    public String getTitle();

    public Long getParent();

    public Long getProject();

    // setters
    public void setName(String name);

    public void setTitle(String title);

    public void setParent(Long parent);

    public void setProject(Long project);

}
