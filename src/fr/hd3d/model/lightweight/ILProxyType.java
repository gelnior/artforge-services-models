package fr.hd3d.model.lightweight;

public interface ILProxyType extends ILSimple
{

    String getOpenType();

    String getDescription();

    void setOpenType(final String openType);

    void setDescription(final String description);

}
