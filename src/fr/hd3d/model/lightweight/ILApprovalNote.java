package fr.hd3d.model.lightweight;

import java.util.Collection;
import java.util.Date;
import java.util.List;


public interface ILApprovalNote extends ILBase
{
    public Long getBoundEntity();

    public String getBoundEntityName();

    public Date getApprovalDate();

    public String getStatus();

    public String getComment();

    public Long getApprover();

    public List<Long> getFilerevisions();

    public String getType();

    public Long getApprovalNoteType();

    public String getMedia();

    public String getApproverName();

    public String getApprovalNoteTypeName();

    public Long getTaskTypeId();

    public void setTaskTypeId(Long taskTypeId);

    public String getTaskTypeName();

    public Collection<Long> getProxies();

    public Collection<Long> getGraphicalAnnotationIDs();

    public void setTaskTypeName(String taskTypeName);

    public void setBoundEntity(Long boundEntity);

    public void setBoundEntityName(String boundEntityName);

    public void setApprovalDate(Date approvalDate);

    public void setStatus(String status);

    public void setComment(String comment);

    public void setApprover(Long approver);

    public void setFilerevisions(List<Long> filerevision);

    public void setType(String type);

    public void setApprovalNoteType(Long approvalNoteType);

    public void setMedia(String media);

    public void setApproverName(String approverName);

    public void setApprovalNoteTypeName(String approvalNoteTypeName);

    public void setProxies(Collection<Long> ids);

    public void setGraphicalAnnotationIDs(Collection<Long> graphicalAnnotations);
}
