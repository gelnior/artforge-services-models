package fr.hd3d.model.lightweight;

/**
 * Interface for object with name lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILModel extends ILSimple
{
    public abstract Long getManufacturerId();

    public abstract void setManufacturerId(Long manufacturerId);

    public abstract String getManufacturerName();

    public abstract void setManufacturerName(String manufacturerName);
}
