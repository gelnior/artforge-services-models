package fr.hd3d.model.lightweight;

/**
 * Interface for screen model lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILScreenModel extends ILModel
{
}
