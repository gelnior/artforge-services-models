package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILDynMetaDataType extends ILBase
{
    public String getName();

    public String getNature();

    public String getType();

    public String getDefaultValue();

    public String getStructName();

    public Long getStructId();

    public void setName(String name);

    public void setNature(String nature);

    public void setType(String type);

    public void setDefaultValue(String defaultValue);

    public void setStructName(String structName);

    public void setStructId(Long structId);
}
