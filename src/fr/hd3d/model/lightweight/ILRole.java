package fr.hd3d.model.lightweight;

import java.util.LinkedHashSet;
import java.util.List;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;


/**
 * 
 * @author florent-della-valle
 * 
 */
public interface ILRole extends ILSimple
{
    @ClientField
    public List<String> getPermissions();

    @ClientField
    public void setPermissions(List<String> permissions);

    @ClientField
    public List<String> getBans();

    @ClientField
    public void setBans(List<String> bans);

    @ClientField
    public List<ILResourceGroup> getResourceGroups();

    @ClientField
    public void setResourceGroups(List<ILResourceGroup> groups);

    @ServerImmutable
    public List<String> getResourceGroupNames();

    public LinkedHashSet<Long> getResourceGroupIDs();

    public void setResourceGroupIDs(LinkedHashSet<Long> groupsIDs);

    public void addResourceGroupID(Long id);

    public void addResourceGroupName(String name);

    public void addPermissions(List<String> permissions);

    public void addBans(List<String> bans);

}
