package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILDynMetaDataValue extends ILBase
{
    public Long getClassDynMetaDataType();

    public String getClassDynMetaDataTypeName();

    public Long getParent();

    public String getParentType();

    public Object getValue();

    public String getClassDynMetaDataType_Type();

    public void setClassDynMetaDataType(Long classDynMetaDataType);

    public void setClassDynMetaDataTypeName(String classDynMetaDataTypeName);

    public void setParent(Long parent);

    public void setValue(Object value);

    public void setParentType(String parentType);
}
