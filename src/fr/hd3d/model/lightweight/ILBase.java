package fr.hd3d.model.lightweight;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.impl.ILPersistent;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ILBase extends ILPersistent
{
    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public void clear();

    public String getInternalUUID();

    public void setInternalUUID(String internalUUID);

    public List<Long> getTags();

    public void setTags(List<Long> tags);

    public List<String> getTagNames();

    public void setTagNames(List<String> tagNames);

    public void addTag(Long tagId);

    public Collection<ILDynMetaDataValue> getDynMetaDataValues();

    public void setDynMetaDataValues(Collection<ILDynMetaDataValue> dynMetaDataValues);

    public Set<ILEntityTaskLink> getEntityTaskLinks();

    public void setEntityTaskLinks(Set<ILEntityTaskLink> entityTaskLinks);

    public Set<ILApprovalNote> getApprovalNotes();

    public void setApprovalNotes(Set<ILApprovalNote> approvalNotes);

    public Set<ILTask> getBoundTasks();

    public void setBoundTasks(Set<ILTask> boundTasks);

    public int hashCode();

    public boolean equals(Object obj);

    @ClientField
    public boolean getUserCanUpdate();

    public void setUserCanUpdate(boolean canUpdate);

    @ClientField
    public boolean getUserCanDelete();

    public void setUserCanDelete(boolean canDelete);

    @ClientField
    public String getDefaultPath();

    public void setDefaultPath(String path);
}
