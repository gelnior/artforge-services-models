package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * @author Try LAM
 */
public interface ILSheet extends ILBase
{

    // getters
    public String getName();

    public String getDescription();

    public Long getProject();

    public String getBoundClassName();

    public List<Long> getItemGroups();

    public String getType();

    // setters
    public void setName(String name);

    public void setDescription(String description);

    public void setProject(Long project);

    public void setBoundClassName(String boundclassname);

    public void setItemGroups(List<Long> itemgroups);

    public void setType(String type);

}
