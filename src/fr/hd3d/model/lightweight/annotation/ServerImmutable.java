package fr.hd3d.model.lightweight.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ServerImmutable
{
}
