package fr.hd3d.model.lightweight;

public interface ILStep extends ILBase
{
    public Long getBoundEntity();

    public String getBoundEntityName();

    public Long getTaskType();

    public String getTaskTypeName();

    public Boolean getCreateTasknAsset();

    public Integer getEstimatedDuration();

    public void setCreateTasknAsset(Boolean createTasknAsset);

    public void setTaskTypeName(String taskTypeName);

    public void setEstimatedDuration(Integer estimatedDuration);

    public void setBoundEntity(Long boundEntity);

    public void setBoundEntityName(String boundEntityName);

    public void setTaskType(Long task);
}
