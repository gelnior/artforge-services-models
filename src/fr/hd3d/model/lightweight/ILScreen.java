package fr.hd3d.model.lightweight;

import java.util.List;


/**
 * Interface for screen lightweight implementation.
 * 
 * @author HD3D
 */
public interface ILScreen extends ILInventoryItem
{
    public abstract String getQualification();

    public abstract String getType();

    public abstract void setQualification(String qualification);

    public Long getScreenModelId();

    public void setScreenModelId(Long screenModelId);

    public String getScreenModelName();

    public void setScreenModelName(String screenModelName);

    public abstract Float getSize();

    public abstract List<Long> getComputerIDs();

    public abstract List<String> getComputerNames();

    public abstract void setSize(Float size);

    public abstract void setComputerIDs(List<Long> computerIDs);

    public abstract void setComputerNames(List<String> computerNames);

    public void addComputerID(Long id);

    public void addComputerName(String name);
}
