package fr.hd3d.model.lightweight;

import java.util.List;


public interface ILPlayListRevision extends ILBase, Hook
{
    public Long getProject();

    public String getFrameRate();

    public String getName();

    public Integer getRevision();

    public String getComment();

    public List<Long> getPlayListEdits();

    public void setProject(Long project);

    public void setFrameRate(String frameRate);

    public void setName(String name);

    public void setRevision(Integer revision);

    public void setComment(String comment);

    public void setPlayListEdits(List<Long> playListEdits);

    public Long getPlayListRevisionGroup();

    public void setPlayListRevisionGroup(Long playListRevisionGroup);
}
