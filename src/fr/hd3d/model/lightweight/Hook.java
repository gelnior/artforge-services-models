package fr.hd3d.model.lightweight;

public interface Hook
{
    String getHook();

    void setHook(final String hook);
}
