package fr.hd3d.model.lightweight;

/**
 * a category has one and only one parent.
 * 
 * @author Try LAM
 */
public interface ILComposition extends ILBase
{

    // getters
    public String getName();

    public String getDescription();

    public Long getConstituent();

    public Long getShot();

    public Long getSequence();

    public void setSequence(Long sequence);

    // public Long getInput();
    //
    // public Long getOutput();

    public Integer getNbOccurence();

    // setters
    public void setName(String name);

    public void setDescription(String description);

    public void setConstituent(Long constituent);

    public void setConstituentName(String constituentName);

    public String getConstituentName();

    public void setShot(Long shot);

    // public void setInput(Long input);
    //
    // public void setOutput(Long output);

    public void setNbOccurence(Integer nbOccurence);

    public void setCategoryId(Long categoryId);

}
