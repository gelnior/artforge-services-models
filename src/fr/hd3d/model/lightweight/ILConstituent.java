package fr.hd3d.model.lightweight;

/**
 * @author Try LAM
 */
public interface ILConstituent extends ILBase, Hook
{

    public Long getCategory();

    public String getCategoryName();

    public String getLabel();

    public String getDescription();

    public Integer getDifficulty();

    public Integer getTrust();

    public Boolean getDesign();

    public Boolean getModeling();

    public Boolean getSetup();

    public Boolean getTexturing();

    public Boolean getShading();

    public Boolean getHairs();

    public Integer getCompletion();

    public String getCategoriesNames();

    // setters
    public void setCategory(Long category);

    public void setCategoryName(String name);

    public void setLabel(String label);

    public void setDescription(String desc);

    public void setDifficulty(Integer difficulty);

    public void setTrust(Integer trust);

    public void setCompletion(Integer completion);

    public void setDesign(Boolean design);

    public void setModeling(Boolean modeling);

    public void setSetup(Boolean setup);

    public void setTexturing(Boolean texturing);

    public void setShading(Boolean shading);

    public void setHairs(Boolean hairs);

    public void setCategoriesNames(String categoriesNames);

}
