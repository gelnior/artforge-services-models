package fr.hd3d.model.lightweight;

public interface ILPlanning extends ILBase, ILDurable
{
    public void setName(String name);

    public String getName();

    public void setProject(Long project);

    public Long getProject();

    public Boolean getMaster();

    public void setMaster(Boolean master);
}
